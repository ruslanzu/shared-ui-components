import { Component } from '@angular/core';
import { ASH_TABLE_ACTION_ICON } from '../../../shared/models/svg-icon-constants';
import { CellActionDropdownItem } from '../../models/cell-action-dropdown-renderer/cell-action-dropdown-item';
import { CellActionDropdownParams } from '../../models/cell-action-dropdown-renderer/cell-action-dropdown-params';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'ash-cell-action-dropdown-renderer',
  templateUrl: './cell-action-dropdown-renderer.component.html',
  styleUrls: ['./cell-action-dropdown-renderer.component.scss']
})
export class CellActionDropdownRendererComponent implements ICellRendererAngularComp {
  public items: CellActionDropdownItem[];
  public actionIcon: string = ASH_TABLE_ACTION_ICON;
  public rowData: any;

  constructor() {}

  public agInit(params: CellActionDropdownParams): void {
    this.items = params.cellActionDropdownItem.filter(item => item.showItem(params.data));
    this.rowData = params.data;
  }

  public refresh(params: any): boolean {
    return false;
  }
}
