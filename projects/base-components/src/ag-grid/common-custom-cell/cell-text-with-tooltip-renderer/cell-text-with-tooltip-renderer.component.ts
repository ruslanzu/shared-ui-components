import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ICellRendererParams } from '@ag-grid-community/core';
@Component({
  selector: 'acf-cell-text-with-tooltip-renderer',
  templateUrl: './cell-text-with-tooltip-renderer.component.html',
  styleUrls: ['./cell-text-with-tooltip-renderer.component.scss']
})
export class CellTextWithTooltipRendererComponent implements ICellRendererAngularComp {
  public cellText: string;
  public agInit(params: ICellRendererParams): void {
    this.cellText = params.value;
  }

  public refresh(): boolean {
    return false;
  }
}
