import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ShortFullName } from '@models/short-full-name';

@Component({
  selector: 'acf-cell-multi-items-text-links-renderer',
  templateUrl: './cell-multi-items-text-links-renderer.component.html',
  styleUrls: ['./cell-multi-items-text-links-renderer.component.scss']
})
export class CellMultiItemsTextLinksRendererComponent implements ICellRendererAngularComp {
  public items: ShortFullName[];
  public shouldDisplayFullName: boolean;
  public linkClicked: Function;
  public getVendor: Function;
  public href: string;

  public agInit(params: any): void {
    this.items = params.value;
    this.shouldDisplayFullName = !!params.fullName;
    this.linkClicked = params.linkClicked;
    this.href = params.href;
    this.getVendor = params.getVendor;
  }

  public refresh(): boolean {
    return false;
  }

  public getName(item: ShortFullName): string {
    return this.shouldDisplayFullName ? item.fullName : item.shortName;
  }
}
