import { ICellRendererParams } from '@ag-grid-community/core';

export interface TextWithLinkCellRendererParams extends ICellRendererParams {
  routePath: string;
  linkText: string;
  data: any;
}
