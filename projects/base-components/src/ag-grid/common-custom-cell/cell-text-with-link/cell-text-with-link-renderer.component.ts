import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { TextWithLinkCellRendererParams } from '@shared/ag-grid/common-custom-cell/cell-text-with-link/params';

@Component({
  selector: 'acf-cell-text-with-link-renderer',
  templateUrl: './cell-text-with-link-renderer.component.html',
  styleUrls: ['./cell-text-with-link-renderer.component.scss']
})
export class CellTextWithLinkRendererComponent implements ICellRendererAngularComp {
  public routePath: string;
  public text: string;
  public agInit(params: TextWithLinkCellRendererParams): void {
    this.routePath = params.routePath;
    this.text = params.linkText;
  }

  public refresh(): boolean {
    return false;
  }
}
