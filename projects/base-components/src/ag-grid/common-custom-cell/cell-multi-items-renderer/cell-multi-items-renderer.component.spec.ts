import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellMultiItemsRendererComponent } from './cell-multi-items-renderer.component';

describe('CellMultiItemsRendererComponent', () => {
  let component: CellMultiItemsRendererComponent;
  let fixture: ComponentFixture<CellMultiItemsRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CellMultiItemsRendererComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(CellMultiItemsRendererComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
