import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ShortFullName } from '@app/models/short-full-name';

@Component({
  selector: 'acf-cell-multi-items-renderer',
  templateUrl: './cell-multi-items-renderer.component.html',
  styleUrls: ['./cell-multi-items-renderer.component.scss']
})
export class CellMultiItemsRendererComponent implements ICellRendererAngularComp {
  public items: ShortFullName[];
  public shouldDisplayFullName: boolean;
  public agInit(params: any): void {
    this.items = params.value;
    this.shouldDisplayFullName = !!params.fullName;
  }

  public refresh(): boolean {
    return false;
  }
}
