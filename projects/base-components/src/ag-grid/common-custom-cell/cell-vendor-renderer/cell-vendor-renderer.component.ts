import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ICellRendererParams, IAfterGuiAttachedParams } from '@ag-grid-community/core';
import { Component } from '@angular/core';

@Component({
  selector: 'acf-cell-vendor-renderer',
  templateUrl: './cell-vendor-renderer.component.html',
  styleUrls: ['./cell-vendor-renderer.component.scss']
})
export class CellVendorRendererComponent implements ICellRendererAngularComp {
  public vendorName: any;

  public agInit(params: ICellRendererParams): void {
    this.vendorName = params.value;
  }
  public afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
  }
  public refresh(): boolean {
    return false;
  }
}
