import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'acf-cell-group-expand-render',
  templateUrl: './cell-group-expand-render.component.html',
  styleUrls: ['./cell-group-expand-render.component.scss']
})
export class CellGroupExpandRenderComponent implements ICellRendererAngularComp {
  constructor() {}

  public agInit(params): void {
  }

  public refresh(): boolean {
    return false;
  }
}
