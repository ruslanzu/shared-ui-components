import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ICellRendererParams } from '@ag-grid-community/core';
import { StringUtils } from '@utils/string-utils';

@Component({
  selector: 'acf-cell-unused-error-renderer',
  templateUrl: './cell-unused-error-renderer.component.html',
  styleUrls: ['./cell-unused-error-renderer.component.scss']
})
export class CellUnunsedErrorRendererComponent implements ICellRendererAngularComp {
  public text: string;
  public error: string;

  public agInit(params: ICellRendererParams): void {
    this.text = params.valueFormatted;
    this.error = StringUtils.wrap(params.data.errorMessage, 60);
  }

  public refresh(): boolean {
    return false;
  }
}
