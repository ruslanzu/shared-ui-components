import { CellActionDropdownItem } from './cell-action-dropdown-item';
import { ICellRendererParams } from '@ag-grid-community/core';

export interface CellActionDropdownParams extends ICellRendererParams {
  cellActionDropdownItem: CellActionDropdownItem[];
}
