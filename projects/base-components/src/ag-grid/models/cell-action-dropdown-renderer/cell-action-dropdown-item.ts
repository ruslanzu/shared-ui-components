export interface CellActionDropdownItem {
  actionText: string;
  actionFunction: (rowData: any) => void;
  showItem: (rowData: any) => boolean;
  itemClass: string;
}
