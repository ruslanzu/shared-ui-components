import { Observable } from 'rxjs';

export interface GridActions {
  getRowData: (startRow: number, endRow: number, sortModel: any, filterModel: any) => Observable<RowData>;
}

export interface RowData {
  rowsThisBlock: any[];
  lastRow?: number;
}
