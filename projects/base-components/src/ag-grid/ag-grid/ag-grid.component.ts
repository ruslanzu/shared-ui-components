import { AgGridAngular } from '@ag-grid-community/angular';
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
import { ColDef, DetailGridInfo, GridOptions, IDatasource, IGetRowsParams, Module } from '@ag-grid-community/core';
import { InfiniteRowModelModule } from '@ag-grid-community/infinite-row-model';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { actionTypes } from '../models/actions';
import { rowModelType } from '../models/ag-grid-types';
import { GridActions } from '../models/grid-actions';
@Component({
  selector: 'ash-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: ['./ag-grid.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AgGridComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() public columnDefs: ColDef[];
  @Input() public gridOptions: GridOptions;
  @Input() public actions: GridActions;
  @Input() public reSetDatasource: boolean;
  @Input() public isHoverGrid: boolean;
  @Input() public rowData: any;
  @Input() public initialSelectedRow: number;
  @Input() public emptyTableMessage: string = 'No Records Found';

  @Output() public gridAction = new EventEmitter<string>(true);

  @ViewChild('grid', { static: true }) public grid: AgGridAngular;
  public modules: Module[] = [ClientSideRowModelModule, InfiniteRowModelModule];

  constructor() {}

  public ngOnInit() {}

  public ngOnChanges(changes: any) {
    if (this.reSetDatasource) {
      if (this.grid.api) {
        this.grid.api.setDatasource(this.getDatasource());
      }
      this.gridAction.emit(actionTypes.FINISH_RESET_DATA_SOURCE);
    }
  }

  public ngAfterViewInit(): void {
    if (this.initialSelectedRow !== undefined) {
      this.grid.api.getRenderedNodes()[this.initialSelectedRow].setSelected(true, true);
    }

    // setTimeout(() => {
    //   this.gridOptions.api.setQuickFilter('yh');
    // }, 5000)
  }

  public onGridReady(params: DetailGridInfo) {
    if (this.gridOptions.rowModelType === rowModelType.INFINITE) {
      params.api.setDatasource(this.getDatasource());
    }
    params.api.sizeColumnsToFit();
  }

  private getDatasource(): IDatasource {
    const datasource: IDatasource = {
      getRows: (rowParams: IGetRowsParams) => {
        this.actions.getRowData(rowParams.startRow, rowParams.endRow, rowParams.sortModel, rowParams.filterModel).subscribe(rowData => {
          const isEmptyTable: boolean = rowData.rowsThisBlock.length === 0;
          if (isEmptyTable) {
            this.gridOptions.api.showNoRowsOverlay();
          } else {
            this.gridOptions.api.hideOverlay();
          }
          rowParams.successCallback(rowData.rowsThisBlock, rowData.lastRow);
        });
      }
    };
    return datasource;
  }
}
