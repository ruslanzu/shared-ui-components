import { Component, Input, Output, EventEmitter, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { QUESTION_ICON } from '@models/svg-icon-constants';
import { IHeaderAngularComp } from '@ag-grid-community/angular';
import { IHeaderParams, IAfterGuiAttachedParams } from '@ag-grid-community/core';

@Component({
  templateUrl: './grid-header-with-help.component.html',
  styleUrls: ['./grid-header-with-help.component.scss']
})
export class GridHeaderWithHelpComponent implements IHeaderAngularComp {
  public readonly helpIcon = QUESTION_ICON;
  public hasUrl: string;
  public helpUrl: string;
  public helpTooltipText: string;
  public ascSort: string;
  public descSort: string;
  public noSort: string;
  public params: any;
  private defaultSort: string;
  private currentSor: string;
  public agInit(params: any): void {
    this.params = params;
    this.helpTooltipText = params.helpTooltipText;
    this.helpUrl = params.helpUrl;
    this.hasUrl = this.helpUrl && this.helpUrl.length > 0 ? 'hasUrl' : 'noUrl';
    this.defaultSort = params.defaultSort ? params.defaultSort : 'asc';
    params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
    this.onSortChanged();
  }
  public onSortRequested(event: KeyboardEvent) {
    if (!this.params.suppressSorting) {
      this.currentSor = this.currentSor === 'asc' ? 'desc' : this.currentSor === 'desc' ? 'asc' : this.defaultSort;
      this.params.setSort(this.currentSor);
    }
  }
  protected onSortChanged(): void {
    this.ascSort = this.descSort = this.noSort = 'grid-header-help-inactive';
    if (this.params.column.isSortAscending()) {
      this.ascSort = 'grid-header-help-active';
    } else if (this.params.column.isSortDescending()) {
      this.descSort = 'grid-header-help-active';
    } else {
      this.noSort = 'grid-header-help-active';
    }
  }
  public afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
  }
  public refresh(params: IHeaderParams): boolean {
    return false;
  }

}
