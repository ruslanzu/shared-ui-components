import { NgModule } from '@angular/core';
import { AgGridModule } from '@ag-grid-community/angular';
import { AgGridComponent } from './ag-grid/ag-grid.component';

@NgModule({
  imports: [AgGridModule.withComponents([])],
  exports: [AgGridComponent, AgGridModule], // TODO remove AgGridModule export
  declarations: [AgGridComponent]
})
export class AngularAgGridModule {}
