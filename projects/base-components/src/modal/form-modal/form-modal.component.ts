import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DialogService } from '../../shared/services/dialog.service';

@Component({
  selector: 'ash-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss']
})
export class FormModalComponent {
  @Input() public title: string = '';
  @Input() public confirmButtonName: string = 'Done';
  @Input() public cancelButtonName: string = 'Cancel';
  @Input() public closeOnCancel: boolean = true;
  @Input() public modalInfoText: string;

  @Output() public close = new EventEmitter<string>();

  constructor(private dialogService: DialogService) {}

  public onClose(button: string) {
    if (this.closeOnCancel) {
      this.dialogService.closeDialog();
    }
    this.close.emit(button);
  }
}
