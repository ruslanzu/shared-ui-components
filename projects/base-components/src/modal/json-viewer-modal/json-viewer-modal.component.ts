import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { COLLAPSE_ALL_ICON, EXPAND_ALL_ICON } from '../../shared/models/svg-icon-constants';
import { CopyButtonType } from '../../shared/copy-to-clipboard/copy-button-type.enum';

@Component({
  templateUrl: './json-viewer-modal.component.html',
  styleUrls: ['./json-viewer-modal.component.scss']
})
export class JsonViewerModalComponent implements OnInit {
  @Input() public elementName: string;
  @Input() public data: any;
  @Input() public isLoading: boolean;
  public isExpanded: boolean;
  public readonly expandIcon = EXPAND_ALL_ICON;
  public readonly collapseIcon = COLLAPSE_ALL_ICON;
  public copyButtonType = CopyButtonType.BUTTON;

  constructor(public activeModal: NgbActiveModal) {}

  public ngOnInit() {
    this.isExpanded = false;
  }

  public onExpandClick() {
    this.isExpanded = true;
  }

  public onCollapseClick() {
    this.isExpanded = false;
  }

  get disableTooltip() {
    const divWidth = 90;
    return this.elementName.length < divWidth;
  }
}
