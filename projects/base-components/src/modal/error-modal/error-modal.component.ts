import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WARNING } from '../../shared/models/svg-icon-constants';

@Component({
  selector: 'ash-error-modal',
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.scss']
})
export class ErrorModalComponent implements OnInit {
  @Input() public title: string;
  @Input() public error: string;

  public readonly warningIcon = WARNING;

  constructor(public activeModal: NgbActiveModal) {}

  public ngOnInit() {}
}
