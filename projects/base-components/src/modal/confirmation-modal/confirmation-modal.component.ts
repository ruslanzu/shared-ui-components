import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { actionTypes } from './actions';

@Component({
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent implements OnInit {
  public readonly yes: string = actionTypes.YES;
  public readonly no: string = actionTypes.NO;

  @Input() public title: string;
  @Input() public body: string;
  @Input() public approveButtonText: string = 'Yes';
  @Input() public declineButtonText: string = 'No';

  constructor(public activeModal: NgbActiveModal) {}

  public ngOnInit() {}
}
