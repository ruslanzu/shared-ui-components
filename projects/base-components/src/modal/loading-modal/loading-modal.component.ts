import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { CONNECTING } from '../../shared/models/svg-icon-constants';

@Component({
  selector: 'ash-loading-modal',
  templateUrl: './loading-modal.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./loading-modal.component.scss']
})
export class LoadingModalComponent implements OnInit {
  @Input() public message: string;

  public readonly connectingIcon = CONNECTING;

  constructor() {}

  public ngOnInit() {}
}
