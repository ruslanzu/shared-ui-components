import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ash-label-with-info',
  templateUrl: './label-with-info.component.html',
  styleUrls: ['./label-with-info.component.scss']
})
export class LabelWithInfoComponent {
  @Input() public id = '';
  @Input() public label = '';
  @Input() public infoText = '';
}
