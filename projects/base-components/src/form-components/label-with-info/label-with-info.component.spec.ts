import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { LabelWithInfoComponent } from './label-with-info.component';

describe('LabelWithInfoComponent', () => {
  let fixture: ComponentFixture<LabelWithInfoComponent>;

  const LABEL = 'test';
  const INFO_TEXT = 'info';

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LabelWithInfoComponent],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(LabelWithInfoComponent);
  });

  it('should display correct label', () => {
    fixture.componentInstance.label = LABEL;

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('.ash-input-label').textContent).toEqual(LABEL);
  });

  it('should render info element', () => {
    fixture.componentInstance.infoText = INFO_TEXT;

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('ash-info')).toBeTruthy();
  });

  it('should not render info element', () => {
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('ash-info')).toBeNull();
  });
});
