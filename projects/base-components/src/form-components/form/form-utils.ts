export class FormUtils {
  public static stringToNumber(value: string): number {
    return +value;
  }

  public static stringWithCommaToStringArray(value: string | string[]): any {
    if (value && typeof value === 'string') {
      return value.split(',');
    }
    return value;
  }
}
