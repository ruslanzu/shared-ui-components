import { FormGroupTyped } from './form-group-type';
import { FormBuilder } from '@angular/forms';

export abstract class AbstractFormComponent<T> {
  public form: FormGroupTyped<T>;
  private formInitialValue: any;
  private formSubmitAttempt = false;
  constructor(private converter?: Partial<{ [key in keyof T]: any }>, private onSubmitMarkAllAsTouched: boolean = true) {}

  public resetForm() {
    this.formSubmitAttempt = false;
    this.form.reset();
  }

  public onSubmit() {
    if (this.onSubmitMarkAllAsTouched) {
      this.form.markAllAsTouched();
    }
    this.formSubmitAttempt = true;
    if (!this.form.valid) {
      return;
    }

    if (this.converter) {
      Object.keys(this.converter).map(key => {
        this.form.value[key] = this.converter[key](this.form.value[key]);
      });
    }
    this.submit();
  }

  public isFieldInvalid(field: Extract<keyof T, string>) {
    return (
      this.isInvalid(field) &&
      (this.form.get(field).touched || this.form.get(field).dirty || (this.form.get(field).untouched && this.formSubmitAttempt))
    );
  }

  public saveInitialValue() {
    this.formInitialValue = Object.freeze(this.form.value); // TODO - make it deep freeze
  }

  public resetFormToInitialValue() {
    this.form.reset(this.formInitialValue);
  }

  protected abstract submit(): void;

  private isInvalid(field) {
    if (!this.form.errors) {
      return this.form.get(field).invalid;
    }
    return this.form.get(field).invalid || this.form.errors.error.value === field;
  }
}
