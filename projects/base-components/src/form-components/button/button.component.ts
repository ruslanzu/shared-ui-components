import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ash-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @Input() public name = '';

  @Input() public disabled = false;

  @Input() public type = 'button';

  @Input() public formId = null;

  @Input() public color = '';

  @Input() public buttonClass = 'btn-default';

  @Input() public svgLeftIconName: string;

  @Input() public svgRightIconName: string;

  @Input() public svgLeftIconClass = 'left-icon-default';

  @Input() public svgRightIconClass = 'right-icon-default';

  @Input() public tooltip: string;

  @Output() public action = new EventEmitter<void>();

  public onClick() {
    this.action.emit();
  }

  public getFormId() {
    return '';
  }

  public getButtonClass(): string[] {
    const buttonClasses = ['btn-' + this.color, this.buttonClass];
    if (this.disabled) {
      buttonClasses.push('ash-button-disabled');
      buttonClasses.push('ash-cursor-not-allowed');
    }
    return buttonClasses;
  }
}
