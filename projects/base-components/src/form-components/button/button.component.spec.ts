import { ButtonComponent } from './button.component';

describe('a new button', () => {
  const button = new ButtonComponent();
  it('should init params correctly', () => {
    expect(button.name).toBe('', 'empty name at first');
    expect(button.disabled).toBe(false, 'inited as enabled');

    button.name = 'Moshe';
    expect(button.name).toBe('Moshe', 'name was filled');
  });
});

describe('a modified button', () => {
  const button = new ButtonComponent();
  it('should construct button class', () => {
    button.color = 'red';
    expect(button.getButtonClass()).toEqual(['btn-red', 'btn-default'], 'button');

    button.disabled = true;
    expect(button.getButtonClass()).toEqual(['btn-red', 'btn-default', 'ash-button-disabled', 'ash-cursor-not-allowed'], 'button');
  });

  it('should emit event when click', () => {
    spyOn(button.action, 'emit');

    button.onClick();
    expect(button.action.emit).toHaveBeenCalled();
  });
});
