import { InputComponent } from './input.component';

describe('initing a general input component', () => {
  const inputComponent = new InputComponent();
  beforeEach(() => {
    spyOn(inputComponent, 'onChange');
  });

  inputComponent.val = 'some val';
  it('should init params correctly', () => {
    expect(inputComponent.value).toBe('some val', 'get value');
  });

  inputComponent.setDisabledState(true);
  it('should be disabled', () => {
    expect(inputComponent.disabled).toBe(true, 'disabled');
  });

  it('callback should not call', () => {
    expect(inputComponent.onChange).not.toHaveBeenCalled();
  });
});

describe('a general input component', () => {
  const inputComponent = new InputComponent();
  beforeEach(() => {
    spyOn(inputComponent, 'onChange');
  });

  it('callback should call', () => {
    inputComponent.value = 'moshe';
    expect(inputComponent.onChange).toHaveBeenCalled();
  });
});
