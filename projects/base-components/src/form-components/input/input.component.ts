import { Component, Input, forwardRef, ViewChild, OnChanges, SimpleChanges } from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ash-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})
export class InputComponent implements ControlValueAccessor, OnChanges {
  // tslint:disable-next-line:no-input-rename
  @Input('value') public val: string;

  @Input() public inputClass = 'input-default';

  @Input() public id = '';

  @Input() public min = null;

  @Input() public max = null;

  @Input() public label = '';

  @Input() public disabled = false;

  @Input() public required = false;

  @Input() public type = 'text';

  @Input() public placeholder = '';

  @Input() public infoText = '';

  @Input() public autocomplete = 'off';

  @Input() public errorMsg = '';

  @Input() public displayError = false;

  @Input() public tooltipWidth = '';

  @Input() public ngbModalAutofocus: boolean;

  @ViewChild('ngbTooltip', { static: true }) public ngbTooltipRef: any;

  public ngOnChanges(changes: SimpleChanges) {
    this.setErrorToolTip(changes);
  }

  get value() {
    return this.val;
  }

  set value(val) {
    this.val = val;
    this.onChange(val);
    this.onTouched();
  }

  // We implement this method to keep a reference to the onChange
  // callback function passed by the forms API
  public registerOnChange(fn) {
    this.onChange = fn;
  }

  // We implement this method to keep a reference to the onTouched
  // callback function passed by the forms API
  public registerOnTouched(fn) {
    this.onTouched = fn;
  }

  // This is a basic setter that the forms API is going to use
  public writeValue(value) {
    if (value) {
      this.value = value;
    } else if (value === null) {
      this.value = '';
    }
  }

  /**
   * This function is called by the forms API when
   * the control status changes to or from "DISABLED".
   */
  public setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  public onChange: any = () => {};
  public onTouched: any = () => {};

  private setErrorToolTip(changes: SimpleChanges) {
    if (this.displayError) {
      this.openErrorToolTipIfNeeded(changes);
    } else {
      this.closeErrorToolTipIfNeeded(changes);
    }
  }

  private openErrorToolTipIfNeeded(changes: SimpleChanges) {
    if (changes.displayError && changes.displayError.currentValue) {
      this.openErrorToolTip();
    } else {
      this.openErrorToolTipIfTextWasUpdated(changes);
    }
  }

  private closeErrorToolTipIfNeeded(changes: SimpleChanges) {
    if (changes.displayError && !changes.displayError.currentValue) {
      this.closeErrorToolTip();
    }
  }

  private openErrorToolTipIfTextWasUpdated(changes: SimpleChanges) {
    if (changes.errorMsg) {
      this.closeErrorToolTip();
      this.openErrorToolTip();
    }
  }

  private openErrorToolTip() {
    // delay in 100 ms since the ngTooltip has the old message.
    setTimeout(() => {
      this.ngbTooltipRef.open();
    }, 100);
  }

  private closeErrorToolTip() {
    this.ngbTooltipRef.close();
  }
}
