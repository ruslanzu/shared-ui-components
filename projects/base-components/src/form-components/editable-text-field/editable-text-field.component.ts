import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { actionTypes } from './actions';
import { icon } from './icon-state';
import { CORRECT, EDIT, ERROR } from '../../shared/models/svg-icon-constants';

@Component({
  selector: 'ash-editable-text-field',
  templateUrl: './editable-text-field.component.html',
  styleUrls: ['./editable-text-field.component.scss']
})
export class EditableTextFieldComponent implements OnInit, OnChanges {
  public readonly editIcon = EDIT;
  public readonly correctIcon = CORRECT;
  public readonly closeIcon = ERROR;

  @Input() public value = '';
  @Input() public inputClass = 'input-default';
  @Input() public textClass = '';
  @Input() public editFinished = true;
  @Input() public autocomplete = 'off';
  @Input() public editFieldError = '';
  @Input() public displayIcon = false;
  @Input() public editAllowed = true;
  @Input() public showTooltip = false;
  @Input() public editMaxLength: number = null;

  @Output() public save = new EventEmitter<string>();
  @Output() public editAction = new EventEmitter<string>();
  // tslint:disable-next-line: no-output-native
  @Output() public close = new EventEmitter<void>();
  @Output() public editStart = new EventEmitter<void>();

  @Input() public editMode = false;

  public iconName: string;

  private originalValue: string;

  constructor() {}

  public ngOnInit() {
    this.iconName = icon.EDIT;
    this.editMode = false;
  }

  public ngOnChanges(changes: any) {
    if (this.editMode) {
      if (this.editFinished) {
        this.editMode = false;
        this.iconName = icon.EDIT;
      }

      if (this.editFieldError) {
        this.iconName = icon.ERROR;
      }
    }
  }

  public handleIconClick() {
    if (this.editAllowed) {
      if (this.iconName === icon.EDIT) {
        this.iconName = icon.CORRECT;
        this.editMode = true;
        this.originalValue = this.value;
        this.editAction.emit(actionTypes.EDIT_START);
      } else if (this.iconName === icon.CORRECT) {
        this.save.emit(this.value);
      } else {
        this.value = this.originalValue;
        this.iconName = icon.EDIT;
        this.editMode = false;
        this.editAction.emit(actionTypes.CLOSE);
      }
    }
  }

  public handleInputChange() {
    if (this.iconName === icon.ERROR) {
      this.editAction.emit(actionTypes.RE_EDIT);
      this.iconName = icon.CORRECT;
    }
  }

  public getTooltipText(): string {
    return this.showTooltip ? this.value : '';
  }
}
