import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { EditableTextFieldComponent } from './editable-text-field.component';

describe('initing an editable text field component', () => {
  let editableTextField;
  let fixture: ComponentFixture<EditableTextFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({declarations: [EditableTextFieldComponent]}).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableTextFieldComponent);
    editableTextField = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    spyOn(editableTextField.save, 'emit');
  });

  it('should init params correctly', () => {
    expect(editableTextField.editMode).toBe(false, 'not in edit mode at start');
  });

  it('should enter edit mode', () => {
    editableTextField.handleIconClick();
    editableTextField.value = 'moshe';
    expect(editableTextField.editMode).toBe(true, 'enter edit mode after clicking edit icon');

    editableTextField.handleIconClick();
    expect(editableTextField.save.emit).toHaveBeenCalled();

    editableTextField.showTooltip = true;
    expect(editableTextField.getTooltipText()).toBe('moshe', 'tooltip should show value');
  });
});
