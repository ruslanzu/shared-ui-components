import { EditableTextComponent } from './editable-text.component';

describe('an editable text component', () => {
  const editableText = new EditableTextComponent();
  beforeEach(() => {
    spyOn(editableText, 'onChange');
  });

  editableText.setDisabledState(true);
  it('should be disabled', () => {
    expect(editableText.disabled).toBe(true, 'disabled');
  });

  it('callback should not call', () => {
    expect(editableText.onChange).not.toHaveBeenCalled();
  });


  it('callback should call', () => {
    editableText.value = 'moshe';
    expect(editableText.onChange).toHaveBeenCalled();
  });
});
