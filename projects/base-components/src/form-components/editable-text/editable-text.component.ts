import { Component, Input, forwardRef, ChangeDetectionStrategy } from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ash-editable-text',
  templateUrl: './editable-text.component.html',
  styleUrls: ['./editable-text.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EditableTextComponent),
      multi: true
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableTextComponent implements ControlValueAccessor {
  // tslint:disable-next-line:no-input-rename
  @Input('value') public val: string;

  @Input() public inputClass = 'input-default';

  @Input() public id = '';

  @Input() public label = '';

  @Input() public disabled = false;

  @Input() public required = false;

  @Input() public type = 'text';

  @Input() public placeholder = '';

  @Input() public autocomplete = 'off';

  @Input() public errorMsg = '';

  @Input() public editMode = false;

  @Input() public displayError = false;

  get value() {
    return this.val;
  }

  set value(val) {
    this.val = val;
    this.onChange(val);
    this.onTouched();
  }

  // We implement this method to keep a reference to the onChange
  // callback function passed by the forms API
  public registerOnChange(fn) {
    this.onChange = fn;
  }

  // We implement this method to keep a reference to the onTouched
  // callback function passed by the forms API
  public registerOnTouched(fn) {
    this.onTouched = fn;
  }

  // This is a basic setter that the forms API is going to use
  public writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  /**
   * This function is called by the forms API when
   * the control status changes to or from "DISABLED".
   */
  public setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  public onChange: any = () => {};
  public onTouched: any = () => {};
}
