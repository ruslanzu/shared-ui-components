import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbModalModule, NgbTooltipModule, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { ClipboardModule } from 'ngx-clipboard';
import { AngularMaterialModule } from './angular-material.module';
import { ConfirmationModalComponent } from './modal/confirmation-modal/confirmation-modal.component';
import { JsonViewerModalComponent } from './modal/json-viewer-modal/json-viewer-modal.component';
import { LoadingModalComponent } from './modal/loading-modal/loading-modal.component';
import { ErrorModalComponent } from './modal/error-modal/error-modal.component';
import { FormModalComponent } from './modal/form-modal/form-modal.component';
import { ButtonComponent } from './form-components/button/button.component';
import { InputComponent } from './form-components/input/input.component';
import { EditableTextFieldComponent } from './form-components/editable-text-field/editable-text-field.component';
import { EditableTextComponent } from './form-components/editable-text/editable-text.component';
import { LabelWithInfoComponent } from './form-components/label-with-info/label-with-info.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { DividerComponent } from './shared/divider/divider.component';
import { CopyToClipboardComponent } from './shared/copy-to-clipboard/copy-to-clipboard.component';
import { SvgIconComponent } from './shared/svg-icon/svg-icon.component';
import { InfoComponent } from './shared/info/info.component';
import { SearchComponent } from './shared/search/search.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularAgGridModule } from './ag-grid/angular-ag-grid.module';
import { CellActionDropdownRendererComponent } from './ag-grid/common-custom-cell/cell-action-dropdown-renderer/cell-action-dropdown-renderer.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [
    ConfirmationModalComponent,
    JsonViewerModalComponent,
    LoadingModalComponent,
    ErrorModalComponent,
    FormModalComponent,
    ButtonComponent,
    InputComponent,
    EditableTextFieldComponent,
    EditableTextComponent,
    LabelWithInfoComponent,
    LoadingComponent,
    DividerComponent,
    CopyToClipboardComponent,
    SvgIconComponent,
    InfoComponent,
    SearchComponent,
    CellActionDropdownRendererComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbTooltipModule,
    AngularMaterialModule,
    NgxJsonViewerModule,
    ClipboardModule,
    HttpClientModule,
    AngularAgGridModule,
    StoreModule.forFeature('sharedComponents', {}, {}),
    EffectsModule.forFeature([])
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbTooltipModule,
    AngularMaterialModule,
    ConfirmationModalComponent,
    JsonViewerModalComponent,
    LoadingModalComponent,
    ErrorModalComponent,
    FormModalComponent,
    ButtonComponent,
    InputComponent,
    EditableTextFieldComponent,
    EditableTextComponent,
    LabelWithInfoComponent,
    LoadingComponent,
    DividerComponent,
    CopyToClipboardComponent,
    NgxJsonViewerModule,
    ClipboardModule,
    SvgIconComponent,
    InfoComponent,
    HttpClientModule,
    AngularAgGridModule,
    SearchComponent,
    CellActionDropdownRendererComponent
  ],
  entryComponents: [
    CellActionDropdownRendererComponent
  ]
})
export class BaseComponentsModule { }
