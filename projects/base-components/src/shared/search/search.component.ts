import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

import { SEARCH_ICON } from '../../shared/models/svg-icon-constants';

@Component({
  selector: 'ash-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Input() public setValue: Observable<string> = new Observable();
  @Input() public reset: Observable<void> = new Observable();

  @Output() public search = new EventEmitter<string>();

  public searchValue: string;
  public readonly svgIconName = SEARCH_ICON;

  public ngOnInit() {
    this.reset.subscribe(() => {
      this.searchValue = '';
    });

    this.setValue.subscribe((value: string) => {
      this.searchValue = value;
    });
  }

  public onSearch(searchString: string) {
    this.search.emit(searchString);
  }
}
