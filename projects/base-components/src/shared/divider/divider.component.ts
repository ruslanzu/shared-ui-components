import { Component, Input } from '@angular/core';

@Component({
  selector: 'ash-divider',
  templateUrl: './divider.component.html',
  styleUrls: ['./divider.component.scss']
})
export class DividerComponent {
  @Input() public vertical = false;
}
