import { DividerComponent } from './divider.component';

describe('a divider component', () => {
  const divider = new DividerComponent();
  it('should init params correctly', () => {
    expect(divider.vertical).toBe(false, 'horizontal on init');

    divider.vertical = true;
    expect(divider.vertical).toBe(true, 'set as vertival');
  });
});
