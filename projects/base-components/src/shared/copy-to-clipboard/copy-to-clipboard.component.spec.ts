import { CopyToClipboardComponent } from './copy-to-clipboard.component';
import { CopyButtonType } from './copy-button-type.enum';

describe('a copy to clipboard component initialization', () => {
  const copyToClipboard = new CopyToClipboardComponent();
  copyToClipboard.textToCopy = 'moshe';
  it('should init params correctly', () => {
    expect(copyToClipboard.textToCopy).toBe('moshe', 'inited');
    expect(copyToClipboard.isIconType()).toBe(true, 'inited');
  });
});

describe('a copy to clipboard component type', () => {
  const copyToClipboard = new CopyToClipboardComponent();

  it('Changes to button type correctly', () => {
    copyToClipboard.copyButtonType = CopyButtonType.BUTTON;
    expect(copyToClipboard.isIconType()).toBe(false, 'verify not button type');
  });
});
