import { Component, Input } from '@angular/core';
import { COPY_TO_CLIPBOARD_ICON } from '../../shared/models/svg-icon-constants';
import { CopyButtonType } from './copy-button-type.enum';

@Component({
  selector: 'ash-copy-to-clipboard',
  templateUrl: './copy-to-clipboard.component.html',
  styleUrls: ['./copy-to-clipboard.component.scss']
})
export class CopyToClipboardComponent {
  @Input() public textToCopy: string;
  @Input() public target: HTMLInputElement;
  @Input() public copyButtonType: CopyButtonType = CopyButtonType.ICON;

  public copyToClipboardIcon: string = COPY_TO_CLIPBOARD_ICON;

  public isIconType(): boolean {
    return this.copyButtonType === CopyButtonType.ICON;
  }
}
