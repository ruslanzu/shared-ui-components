export enum Position {
  RELATIVE = 'relative',
  ABSOLUTE = 'absolute',
  DEFAULT = ''
}

export enum Size {
  SMALL = 'small',
  XS = 'xs',
  DEFAULT = ''
}
