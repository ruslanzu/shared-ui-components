import { Component, Input } from '@angular/core';
import { Size, Position } from './loading.enum';

@Component({
  selector: 'ash-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {
  @Input() public size: Size;
  @Input() public position: Position;
}
