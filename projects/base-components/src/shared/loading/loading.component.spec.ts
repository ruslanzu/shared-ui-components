import { LoadingComponent } from './loading.component';
import { Size, Position } from './loading.enum';

describe('loading component initialization', () => {
  const loadingComponent = new LoadingComponent();
  loadingComponent.size = Size.XS;
  loadingComponent.position = Position.ABSOLUTE;
  it('should init params correctly', () => {
    expect(loadingComponent.size).toBe(Size.XS, 'inited');
    expect(loadingComponent.position).toBe(Position.ABSOLUTE, 'inited');
  });
});
