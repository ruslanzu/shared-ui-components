/*******************
 * Icons
 * This is the name of the icons - the icon can be found at "src/assets/svg/icon-name.svg"
 * here you can fins the 'icon-name'
 ******************/

export const ALGOSEC_MAIN_ICON: string = 'ic-algosec';
export const ALGOSEC_CLOUD_LOGO_ICON: string = 'ic-algoseccloud';

export const HOME_ICON: string = 'ic-menu-home';
export const NETWORK_POLICIES_ICON: string = 'ic-menu-network-policy';
export const RISKS_ICON: string = 'ic-menu-risks';
export const INVENTORY_ICON: string = 'ic-menu-inventory';

export const SIDEBAR_ARROW_RIGHT_ICON: string = 'caret-left-solid';
export const SIDEBAR_ARROW_LEFT_ICON: string = 'caret-right-solid';

export const NAVBAR_OPEN_TREE_ICON: string = 'ic-tree-open';
export const NAVBAR_CLOSE_TREE_ICON: string = 'ic-tree-close';
export const NAVBAR_USER_MANAGEMENT_ICON: string = 'ic-arrow';
export const SETTINGS_ICON: string = 'cog-solid';
export const SETTINGS_USERS_MANAGEMENT_ICON: string = 'users-cog-light';
export const SETTINGS_ACCOUNT_MANAGEMENT_ICON: string = 'clouds-light';
export const SETTINGS_ASMS_INTEGRATION_ICON: string = 'ash-plug-light';

export const SEARCH_ICON: string = 'ic-search';
export const TREE_PINNED_ICON: string = 'ic-push-pin';
export const TREE_OPEN_FULL_SIZE_ICON: string = 'ic-full-menu';

export const TREE_OPEN_ASSET_ELEMENT: string = 'angle-up-solid';
export const TREE_CLOSE_ASSET_ELEMENT: string = 'angle-down-solid';

export const NETWORK_POLICY_POLICY_ICON: string = 'np_ic_blue';

export const LONG_ARROW_POINT_DOWN: string = 'ic-long-arrow-point-down';
export const ROUND_ARROW_POINT_RIGHT: string = 'ic-round-arrow-point-right';
export const LONG_ARROW_POINT_RIGHT: string = 'ic-long-arrow-point-right';
export const CONNECTING: string = 'ic-connecting';
export const WARNING: string = 'ic-warning';
export const EDIT: string = 'ic-edit';
export const CORRECT: string = 'ic-correct';
export const ERROR: string = 'ic-error';
export const DELETE: string = 'ic-delete';
export const TIMES_ICON: string = 'ic-times-light';
export const PASSWORD: string = 'ic-password';
export const AZURE_ICON: string = 'ic-azure';

export const QUESTION_ICON: string = 'ic-question';
export const INFO_CIRCLE_ICON: string = 'ic-info-circle';
export const COMMENT_ICON: string = 'ic-comment';

export const AZURE_ACCOUNT: string = 'azure-account';
export const AZURE_VNET_PEERING: string = 'VNetPeering';
export const PLUS_ICON: string = 'plus-solid';
export const AZURE_VNET: string = 'azure-vnet';
export const AZURE_NSG_ICON: string = 'azure-nsg';
export const AZURE_PUBLIC_IP: string = 'azure-public-ip';
export const AZURE_NETWORK_INTERFACE: string = 'azure-network-interface';
export const AZURE_VM: string = 'azure-vm';
export const AZURE_LOAD_BALANCER: string = 'azure-load-balancer';
export const AZURE_SUBNET: string = 'azure-subnet';
export const AZURE_FIREWALL_ICON: string = 'azure-firewall';
export const AZURE_STORAGE_ACCOUNT: string = 'azure-storage-account';
export const AZURE_CONTAINER_BLOB: string = 'azure-container-blob';
export const ALGOSEC_CLOUD_FLOW_ICON: string = 'algosec-cloud-flow';
export const APPROVE_ICON: string = 'ic_approve';
export const APPROVE_ICON_GRAY: string = 'ic_approve_gray';
export const SUPPRESS_ICON_GRAY: string = 'ic_suppress';
export const ERASE_ICON: string = 'ic_erase';
export const EDITED_ICON: string = 'ic_edited_rule';
export const FAILED_ICON: string = 'ic_failed';
export const PENDING_ICON: string = 'pending';
export const EXTERNAL_LINK_ICON: string = 'external-link-solid';
export const COPY_TO_CLIPBOARD_ICON: string = 'copy-solid';
export const EXPAND_ICON: string = 'chevron-double-right-regular';
export const COLLAPSE_ICON: string = 'chevron-double-up-regular';
export const EXPAND_BOX_ICON: string = 'chevron-down-light';
export const COLLAPSE_BOX_ICON: string = 'chevron-up-light';

export const AWS_ICON: string = 'ic-aws';
export const AWS_ACCOUNT_ICON: string = 'aws-account';
export const AWS_VPC_ICON: string = 'aws-vpc';
export const AWS_SUBNET_ICON: string = 'aws-subnet';
export const AWS_INSTANCE_ICON: string = 'aws-instance';
export const AWS_SG_ICON: string = 'aws-sg';
export const AWS_NETWORK_INTERFACE_ICON: string = 'aws-network-interface';
export const AWS_S3_ICON: string = 'aws-s3';
export const NOT_FOUND_ICON: string = 'ic-404';

export const EXPAND_ALL_ICON: string = 'ic-expand';
export const COLLAPSE_ALL_ICON: string = 'ic-collapse';

export const DISSOLVE_ICON: string = 'ic-dissolve';
export const DOWNLOAD_ICON: string = 'download';
export const TSQ_ICON: string = 'TSQ';
export const TSQ_CONNECTIVITY_ICON: string = 'connectivity';
export const TSQ_INTEGRATION_ICON: string = 'integration';

export const ASH_TABLE_ACTION_ICON: string = 'ellipsis-v-light';
