import { InfoComponent } from './info.component';

describe('a info component initialization', () => {
  const infoComponent = new InfoComponent();
  infoComponent.infoMessage = 'moshe';
  it('should init params correctly', () => {
    expect(infoComponent.infoMessage).toBe('moshe', 'inited');
  });
});
