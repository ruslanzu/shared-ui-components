import { Component, Input } from '@angular/core';
import { INFO_CIRCLE_ICON } from '../models/svg-icon-constants';

@Component({
  selector: 'ash-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent {
  public readonly infoIcon = INFO_CIRCLE_ICON;

  @Input() public infoMessage: string;
}
