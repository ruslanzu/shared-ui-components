import { Component, HostListener, OnInit, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { SessionService } from '@algosec/auth';
import { environment } from '../environments/environment';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

  constructor(private sessionService: SessionService, private router: Router) {
    sessionService.setProductType('cloud_flow');
  }

  public title = 'Lib Tester';
  public isTreeDisplayed = false;
  public isTreeOpen = false;

  @HostListener('document:keyup', ['$event'])
  @HostListener('document:click', ['$event'])

  public updateUserActivity() {
   this.sessionService.updateUserActivity(false);
  }

  public ngOnInit() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe(() => {
        switch (this.router.url) {
          case '/':
            this.title = 'Home';
            this.isTreeDisplayed = false;
            break;
            case '/under-construction':
              case '/settings/under-construction':
            this.title = 'Under Construction';
            this.isTreeDisplayed = false;
            break;
          case '/auth-tester':
            this.title = 'Auth Tester';
            this.isTreeDisplayed = false;
            break;
          case '/settings/asms-integration':
            this.title = 'Settings';
            this.isTreeDisplayed = false;
            break;
          default:
            this.title = 'Lib Tester';
            this.isTreeDisplayed = true;
            break;
        }
      });
  }
}
