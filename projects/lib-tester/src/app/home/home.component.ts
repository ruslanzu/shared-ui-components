import { Component, OnInit } from '@angular/core';
import { ALGOSEC_CLOUD_FLOW_ICON, ALGOSEC_MAIN_ICON } from 'projects/base-components/src/shared/models/svg-icon-constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public readonly genericFlowIcon = ALGOSEC_CLOUD_FLOW_ICON;
  public readonly algosecIcon = ALGOSEC_MAIN_ICON;
  logoSubtitleText = 'Build your product with the shareable-ui libraries';
  constructor() { }

  ngOnInit(): void {
  }

}
