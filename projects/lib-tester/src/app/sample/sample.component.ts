import { AuthorizationApiService, AuthorizationComponent, AuthorizationState } from '@algosec/authorization';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { SessionService } from '@algosec/auth';
import { EditableTextFieldComponent } from '@algosec/base-components';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent extends AuthorizationComponent implements AfterViewInit {
  @ViewChild(EditableTextFieldComponent, {static: false}) ashEditableTextFieldSample: EditableTextFieldComponent;
  constructor(protected store: Store<AuthorizationState>,
              protected route: ActivatedRoute,
              protected authorizationApiService: AuthorizationApiService,
              protected sessionService: SessionService) {
               super(store, route, authorizationApiService);
  }

  ngAfterViewInit(): void {
    this.ashEditableTextFieldSample.editMode = this.canUserEdit(['ManageResources','AdminSettings']);
  }

  public onClick() {
    alert('clicked');
  }

  public getUsername(): string {
    return this.sessionService.getUsername();
  }

  public getFormattedOps() {
    const ops = this.getOperations();
    if (!ops) {
      return ('(None)');
    }
    let formatted = '';
    for (const op of ops) {
      formatted += ' ' + op + ',';
    }
    return formatted;
  }
}
