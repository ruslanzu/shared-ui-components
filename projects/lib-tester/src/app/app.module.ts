import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BaseComponentsModule } from '@algosec/base-components';
import { AccessManagementModule } from '@algosec/access-management';
import { HttpClientModule } from '@angular/common/http';
import { ShellModule } from '@algosec/shell';
import { AuthErrorInterceptor,  AuthModule,  TokenInterceptor } from '@algosec/auth';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthTesterComponent } from './auth-tester/auth-tester.component';
import { HomeComponent } from './home/home.component';
import { UnderConstructionComponent } from './under-construction/under-construction.component';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { SampleComponent } from './sample/sample.component';
import { AuthorizationModule } from '@algosec/authorization';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthErrorInterceptor, multi: true }
];

@NgModule({
  declarations: [
    AppComponent,
    AuthTesterComponent,
    HomeComponent,
    UnderConstructionComponent,
    SampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BaseComponentsModule,
    AccessManagementModule,
    HttpClientModule,
    ShellModule,
    AuthModule,
    AuthorizationModule,
    StoreModule.forRoot({}, {}),
    EffectsModule.forRoot([])
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
