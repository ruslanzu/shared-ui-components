import { AccessManagementComponent } from '@algosec/access-management';
import { AsmsIntegrationComponent, EnvironmentPreset } from '@algosec/asms-integration';
import { AuthGuard } from '@algosec/auth';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from '../environments/environment';
import { AuthTesterComponent } from './auth-tester/auth-tester.component';
import { HomeComponent } from './home/home.component';
import { SampleComponent } from './sample/sample.component';
import { UnderConstructionComponent } from './under-construction/under-construction.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'sample',
    component: SampleComponent,
    data: {
      envPreset: environment.dev ? EnvironmentPreset.Dev : (
        environment.stage ? EnvironmentPreset.Stage : (
          environment.production ? EnvironmentPreset.Prod :
          EnvironmentPreset.Dev
        )
      )
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'under-construction',
    component: UnderConstructionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'settings/under-construction',
    component: UnderConstructionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'settings/asms-integration',
    component: AsmsIntegrationComponent,
    data: {
      envPreset: environment.dev ? EnvironmentPreset.Dev : (
        environment.stage ? EnvironmentPreset.Stage : (
          environment.production ? EnvironmentPreset.Prod :
          EnvironmentPreset.Dev
        )
      )
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'settings/access-management',
    component: AccessManagementComponent,
    data: {
      envPreset: environment.dev ? EnvironmentPreset.Dev : (
        environment.stage ? EnvironmentPreset.Stage : (
          environment.production ? EnvironmentPreset.Prod :
          EnvironmentPreset.Dev
        )
      )
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'auth-tester',
    component: AuthTesterComponent,
    data: {
      envPreset: environment.dev ? EnvironmentPreset.Dev : (
        environment.stage ? EnvironmentPreset.Stage : (
          environment.production ? EnvironmentPreset.Prod :
          EnvironmentPreset.Dev
        )
      )
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
