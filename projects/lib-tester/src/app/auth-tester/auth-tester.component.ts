import { Component, OnInit } from '@angular/core';
import { AuthService } from '@algosec/auth';
import { SessionService } from '@algosec/auth';

@Component({
  selector: 'app-auth-tester',
  templateUrl: './auth-tester.component.html',
  styleUrls: ['./auth-tester.component.scss']
})
export class AuthTesterComponent implements OnInit {

  constructor(private sessionService: SessionService, private authService: AuthService) { }

  ngOnInit(): void {
  }

  public testLogout() {
    this.sessionService.userLogout();
  }

  public async currentSession() {
    this.authService.getCurrentSession();
  }

  public async getToken() {
    console.log(await this.authService.getToken());
  }
}
