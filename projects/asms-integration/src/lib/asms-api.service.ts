import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { from } from 'rxjs';
import {
  ASMS_SERVICE_PATH,
  ASMS_SERVICE_VERSION
} from './micro-services-paths';
import { Environment } from '../shared/models/environment';

@Injectable({ providedIn: 'root' })
export class AsmsApiService {
  private static readonly asmsBaseApi = `${ASMS_SERVICE_PATH}${ASMS_SERVICE_VERSION}/asms`;

  constructor(private apiService: ApiService) {}

  public getLastConnectionTime(env: Environment): Observable<number> {
    this.apiService.environment = env;
    return this.apiService.get<number>(
      `${AsmsApiService.asmsBaseApi}/last-connection`
    );
  }

  public getDownloadCertificate(env: Environment): Observable<Blob> {
    this.apiService.environment = env;
    return this.apiService.download(
      `${AsmsApiService.asmsBaseApi}/download-certificate`
    );
  }
}
