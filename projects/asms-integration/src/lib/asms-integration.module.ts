import { NgModule } from '@angular/core';
import { AsmsIntegrationComponent } from './asms-integration.component';
import { BaseComponentsModule } from '@algosec/base-components';

@NgModule({
  declarations: [AsmsIntegrationComponent],
  imports: [BaseComponentsModule]
})
export class AsmsIntegrationModule {
}
