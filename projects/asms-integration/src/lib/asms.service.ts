import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DialogService } from '@algosec/base-components';
import { AsmsApiService } from './asms-api.service';
import { Environment } from '../shared/models/environment';

@Injectable({ providedIn: 'root' })
export class AsmsService {

  public env: Environment;

  constructor(
    private asmsApiService: AsmsApiService,
    private dialogService: DialogService
  ) { }

  public getLastConnectionTime(): Observable<Date> {
    return this.asmsApiService.getLastConnectionTime(this.env).pipe(
      map(
        lastConnectionTimeStamp => {
          return lastConnectionTimeStamp['lastUpdate'] ? new Date(lastConnectionTimeStamp['lastUpdate']) : null;
        },
        errors => {
          this.handleError(errors);
        }
      )
    );
  }

  public getDownloadCertificate(): Observable<any> {
    return this.asmsApiService.getDownloadCertificate(this.env);
  }

  private handleError(errors: any): void {
    console.log(errors);
    this.dialogService.openErrorModal('Operation has failed', errors.message);
  }
}
