import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { TSQ_CONNECTIVITY_ICON, TSQ_INTEGRATION_ICON, DOWNLOAD_ICON } from '../shared/models/svg-icon-constants';
import { AsmsService } from './asms.service';
import { SessionService } from '@algosec/auth';
import { ASMS_INTEGRATION_HELP_CENTER_LINK } from '../shared/models/constants';
import { DialogService } from '@algosec/base-components';
import { EnvironmentPreset, environments } from '../shared/models/environment';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ash-asms-integration',
  templateUrl: './asms-integration.component.html',
  styleUrls: ['./asms-integration.component.scss']
})
export class AsmsIntegrationComponent implements OnInit, OnDestroy {
  public readonly tsqConnectivityIconName = TSQ_CONNECTIVITY_ICON;
  public readonly tsqIntegrationIconName = TSQ_INTEGRATION_ICON;
  public readonly downloadIconName = DOWNLOAD_ICON;
  public readonly asmsIntegrationHelpCenterLink = ASMS_INTEGRATION_HELP_CENTER_LINK;

  @Input() public envPreset: EnvironmentPreset;
  private subscription: Subscription;

  public isLoading = true;

  private lastConnectionTime;

  constructor(private asmsService: AsmsService, private sessionService: SessionService, private dialogService: DialogService, private route: ActivatedRoute) {
  }

  public ngOnInit() {
    this.subscription = this.route.data.subscribe(data => {
      this.asmsService.env = environments[data.envPreset];

      this.asmsService.getLastConnectionTime().subscribe(lastConnectionTime => {
        this.lastConnectionTime = lastConnectionTime;
        this.isLoading = false;
      });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public isIntegrationSet(): boolean {
    return !!this.lastConnectionTime;
  }

  public onDownloadConfigurationFile(): void {
    this.asmsService.getDownloadCertificate().subscribe(
      data => {
        this.downloadBlob(new Blob([data], { type: 'application/zip' }), `AlgoSec_Cloud_trust_establish_data-${this.sessionService.getTenantId()}.zip`);
      },
      error => {
        const reader = new FileReader();
        reader.addEventListener('loadend', (e) => {
          const errorAsText = e.srcElement['result'];
          console.log(errorAsText);
          const errorAsObject = JSON.parse(errorAsText);
          const errorModal = this.dialogService.openErrorModal(undefined, errorAsObject.reason.message);
        });
        reader.readAsText(error);
      }
    );
  }

  private downloadBlob(data: Blob, filename: string) {
    const downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(data);
    downloadLink.download = filename;
    downloadLink.click();
  }
}
