import { TestBed } from '@angular/core/testing';

import { AsmsIntegrationService } from './asms-integration.service';

describe('AsmsIntegrationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsmsIntegrationService = TestBed.get(AsmsIntegrationService);
    expect(service).toBeTruthy();
  });
});
