/****************************
 * General constants
 ****************************/
export const CLOUDFLOW_HELP_CENTER_BASE_COMMON_LINK: string = 'https://docs.algosec.com/en/cloudflow/content/cloud-common';
export const ASMS_INTEGRATION_HELP_CENTER_LINK: string = `${CLOUDFLOW_HELP_CENTER_BASE_COMMON_LINK}/asms-integration.htm`;

