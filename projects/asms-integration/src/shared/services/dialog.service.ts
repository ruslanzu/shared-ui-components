import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoadingModalComponent } from '../../modal/loading-modal/loading-modal.component';
import { ErrorModalComponent } from '../../modal/error-modal/error-modal.component';
import { JsonViewerModalComponent } from '../../modal/json-viewer-modal/json-viewer-modal.component';

@Injectable({ providedIn: 'root' })
export class DialogService {
  private stack: NgbModalRef[] = [];

  constructor(private modalService: NgbModal) {}

  public openDialog(content: any, options?: NgbModalOptions): NgbModalRef {
    options.backdrop = options.backdrop || 'static';
    const dialog = this.modalService.open(content, options);

    this.stack.push(dialog);

    return dialog;
  }

  public openJsonViewerModal(): NgbModalRef {
    const dialog = this.modalService.open(JsonViewerModalComponent, { centered: true, size: 'lg', windowClass: 'json-viewer-modal' });
    this.stack.push(dialog);
    return dialog;
  }

  public closeDialog(): void {
    this.stack.forEach(dialogRef => dialogRef.close());
    this.stack = [];
  }

  public dismissDialog(): void {
    this.stack.forEach(dialogRef => dialogRef.dismiss());
    this.stack = [];
  }

  public openLoadingModal(modalMessage: string) {
    const loadingModal = this.openDialog(LoadingModalComponent, { centered: true, windowClass: 'loading-modal' });
    loadingModal.componentInstance.message = modalMessage;
    return loadingModal;
  }

  public openErrorModal(title: string, errorMessage: string): NgbModalRef {
    const errorModal = this.openDialog(ErrorModalComponent, { centered: true });
    errorModal.componentInstance.title = title;
    errorModal.componentInstance.error = errorMessage;
    return errorModal;
  }
}
