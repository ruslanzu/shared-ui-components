import { Component, Input, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ash-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent implements OnInit {
  private static readonly svgIconPrefix: string = 'assets/svg/';

  private static readonly svgIconSuffix: string = '.svg';

  @Input() public svgIconName: string;

  @Input() public inline = true;


  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {}

  public ngOnInit() {
    this.matIconRegistry.addSvgIcon(
      this.svgIconName,
      this.domSanitizer.bypassSecurityTrustResourceUrl(SvgIconComponent.svgIconPrefix + this.svgIconName + SvgIconComponent.svgIconSuffix)
    );
  }
}
