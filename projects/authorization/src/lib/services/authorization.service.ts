import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { USER_OPS_KEY } from '../models/constants/constants';
import { AuthorizationRootState } from '../store/authorization-root-state';
import { AuthorizationApiService } from './api/authorization-api.service';
import { StorageService } from './storage.service';

@Injectable({ providedIn: 'root' })
export class AuthorizationService {
  constructor(
  private storageService: StorageService,
  private authorizationApiService: AuthorizationApiService,
  private store: Store<AuthorizationRootState>) {}

  public loadUserOperations(tenantId: string, username: string, includeStorage: boolean): Observable<string[]> {
    if (includeStorage) {
      const userOpsList = this.storageService.getItem(USER_OPS_KEY);
      if (userOpsList) {
        const userOpsArr: string[] = userOpsList.split(',');
        if (userOpsArr.length > 0) {
          return of (userOpsArr);
        }
      }
    }
    return this.authorizationApiService.loadUserOperations(tenantId, username);
  }
}
