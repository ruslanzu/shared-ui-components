import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Environment } from '../../models/environment';
import { AlgoSaaSApiService } from './api.service';
import { AUTHZ_SERVICE_PATH, AUTHZ_SERVICE_VERSION } from './micro-services-paths';

@Injectable({ providedIn: 'root' })
export class AuthorizationApiService {
  private static readonly authorizationBaseApi: string = `${AUTHZ_SERVICE_PATH}${AUTHZ_SERVICE_VERSION}`;
  public env: Environment;

  constructor(private algoSaaSApiService: AlgoSaaSApiService) {}

  public loadUserOperations(tenantId: string, username: string): Observable<string[]> {
    const params = new HttpParams().set('tenant', tenantId).set('user', username.toLowerCase());
    this.algoSaaSApiService.environment = this.env;
    return this.algoSaaSApiService.get<string[]>(`${AuthorizationApiService.authorizationBaseApi}/user/operations`, params);
  }
}
