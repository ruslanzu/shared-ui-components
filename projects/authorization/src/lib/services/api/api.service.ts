import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Environment } from '../../models/environment';

@Injectable({ providedIn: 'root' })
export class ApiService {
  public environment: Environment;

  constructor(private http: HttpClient) {}

  public get<T>(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.get<T>(`${this.environment.cloudflow_api}${path}`, { params, headers });
  }

  public download(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<Blob> {
    return this.http.get<Blob>(`${this.environment.cloudflow_api}${path}`, { params, headers, responseType: 'blob' as 'json'});
  }

  public put<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.put<T>(`${this.environment.cloudflow_api}${path}`, body);
  }

  public patch<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.patch<T>(`${this.environment.cloudflow_api}${path}`, body);
  }

  public post<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.post<T>(`${this.environment.cloudflow_api}${path}`, body);
  }

  public delete<T>(path, headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.delete<T>(`${this.environment.cloudflow_api}${path}`, {headers});
  }
}

@Injectable({ providedIn: 'root' })
export class AlgoSaaSApiService {
  public environment: Environment;

  constructor(private http: HttpClient) {}

  public get<T>(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.get<T>(`${this.environment.algosass_api}${path}`, { params, headers });
  }

  public download(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<Blob> {
    return this.http.get<Blob>(`${this.environment.algosass_api}${path}`, { params, headers, responseType: 'blob' as 'json'});
  }

  public put<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.put<T>(`${this.environment.algosass_api}${path}`, body);
  }

  public patch<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.patch<T>(`${this.environment.algosass_api}${path}`, body);
  }

  public post<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.post<T>(`${this.environment.algosass_api}${path}`, body);
  }

  public delete<T>(path, headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.delete<T>(`${this.environment.algosass_api}${path}`, {headers});
  }
}
