import { NgModule } from '@angular/core';
import { AuthorizationStoreModule } from './store/authorization-store.module';

@NgModule({
  declarations: [
  ],
  imports: [AuthorizationStoreModule]
  ,
  exports: [
  ]
})
export class AuthorizationModule { }
