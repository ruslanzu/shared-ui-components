import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Environment, EnvironmentPreset, environments } from './models/environment';
import { AuthorizationApiService } from './services/api/authorization-api.service';
import { AuthorizationState } from './store/authorization-state';
import { AuthorizationAction } from './store/authorization.actions';
import { selectAuthorization } from './store/authorizaton.selectors';
@Component({
  selector: 'lib-operations',
  template: `
  `,
  styles: [
  ]
})
export abstract class AuthorizationComponent implements OnInit {
  private subscription: Subscription;
  private userOperations: string[] = [];
  private userOperationsSubscription: Subscription;

  constructor(
    protected store: Store<AuthorizationState>,
    protected route: ActivatedRoute,
    protected authorizationApiService: AuthorizationApiService) {
    this.subscription = this.route.data.subscribe(data => {
      this.authorizationApiService.env = environments[data.envPreset];
      this.setOperations();
    });
    this.store.dispatch(AuthorizationAction.create(true));
   }

  ngOnInit(): void {
  }

  public getOperations(): string[] {
    return this.userOperations;
  }

  private setOperations(): void {
    this.userOperationsSubscription = this.store.select(selectAuthorization).subscribe((operations: string[]) => {
      this.userOperations = operations;
    });
  }

  public canUserEdit(allowedOperations: string[]): boolean {
    if (!allowedOperations) {
      return false;
    }
    if (allowedOperations.length === 0) {
      return false;
    }
    return this.userOperations.some(operation => allowedOperations.indexOf(operation) !== -1);
  }
}
