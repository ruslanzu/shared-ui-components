export interface Environment {
  algosass_api: string;
  login_api: string;
  cloudflow_api: string;
}

export enum EnvironmentPreset {
  Prod,
  Stage,
  Dev
}

export type Environments = {[key in keyof typeof EnvironmentPreset]?: Environment};

export const environments: Environments = {
  [EnvironmentPreset.Prod]: {
    algosass_api: 'https://app.algosec.com/api',
    cloudflow_api: 'https://prod.cloudflow.algosec.com/api',
    login_api: 'https://app.algosec.com/login'
  },
  [EnvironmentPreset.Stage]: {
    algosass_api: 'https://stage.app.algosec.com/api',
    cloudflow_api: 'https://atest1.stage.cloudflow.algosec.com/api',
    login_api: 'https://stage.app.algosec.com/login'
  },
  [EnvironmentPreset.Dev]: {
    algosass_api: 'https://dev.app.algosec.com/api',
    cloudflow_api: 'https://api-feature-cs-89898.dev.cloudflow.algosec.com/api', // This is a cloud flow dev env that should be fixed for ObjectFlow
    login_api: 'https://dev.app.algosec.com/login'
  }
};
