import { AuthorizationState } from './authorization-state';

export interface AuthorizationRootState {
    authorizationState: AuthorizationState;
  }
