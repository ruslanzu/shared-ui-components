import { Action } from '@ngrx/store';

export enum AuthorizationActionTypes {
    AUTHORIZATION = '[Authorization] Authorization',
    AUTHORIZATION_SUCCESS = '[Authorization] Authorization retrieved successfully',
    AUTHORIZATION_FAILURE = '[Authorization] Authorization retrieval failed',
  }

export class AuthorizationAction implements Action {
    public readonly type = AuthorizationActionTypes.AUTHORIZATION;
    constructor(public payload: {includeStorage: boolean}) {}

    public static create(includeStorage: boolean): AuthorizationAction {
      return new AuthorizationAction({includeStorage});
    }
  }

export class AuthorizationSuccessAction implements Action {
    public readonly type = AuthorizationActionTypes.AUTHORIZATION_SUCCESS;

    constructor(public payload: { authorization: string[] }) {
    }

    public static create(operations: string[]): AuthorizationSuccessAction {
      return new AuthorizationSuccessAction({ authorization: operations });
    }
  }

export class AuthorizationFailedAction implements Action {
    public readonly type = AuthorizationActionTypes.AUTHORIZATION_FAILURE;
    constructor(public payload: { authorizationError: string }) {
    }

    public static create(error?: string): AuthorizationFailedAction {
      return new AuthorizationFailedAction({authorizationError: error});
    }
  }

export type AuthorizationActions =
  | AuthorizationAction
  | AuthorizationSuccessAction
  | AuthorizationFailedAction;

