
export interface AuthorizationState {
    isLoading?: boolean;
    authorization: string[];
    authorizationError: string;
  }

export const initialAuthorizationState: AuthorizationState = {
    isLoading: false,
    authorization: [],
    authorizationError: ''
  };
