import { initialAuthorizationState, AuthorizationState } from './authorization-state';
import { AuthorizationActions, AuthorizationActionTypes } from './authorization.actions';

export function reducer(state = initialAuthorizationState, action: AuthorizationActions): AuthorizationState {
    switch (action.type) {
      case AuthorizationActionTypes.AUTHORIZATION_SUCCESS:
        return {
          ...state,
          authorization: action.payload.authorization
        };

        case AuthorizationActionTypes.AUTHORIZATION_FAILURE:
        return {
          ...state,
          authorizationError: action.payload.authorizationError
        };

      default:
        return state;
    }
  }
