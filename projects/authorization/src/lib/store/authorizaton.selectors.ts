import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { AuthorizationState } from './authorization-state';

export const selectAuthorizationState: MemoizedSelector<object, AuthorizationState> = createFeatureSelector<AuthorizationState>('authorization');

export const selectAuthorization: MemoizedSelector<object, string[]> = createSelector(
    selectAuthorizationState,
    (state: AuthorizationState) => state.authorization
  );
