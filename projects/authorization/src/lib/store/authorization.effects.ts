import { SessionService } from '@algosec/auth';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { USER_OPS_KEY } from '../models/constants/constants';
import { AuthorizationService } from '../services/authorization.service';
import { StorageService } from '../services/storage.service';
import { AuthorizationActions, AuthorizationActionTypes, AuthorizationAction, AuthorizationSuccessAction, AuthorizationFailedAction } from './authorization.actions';

@Injectable()
export class AuthorizationsEffects {
  @Effect()
  public authorization$ = this.actions$.pipe(
    ofType(AuthorizationActionTypes.AUTHORIZATION),
    mergeMap((action: AuthorizationAction) => {
      const tenantId = this.sessionService.getTenantId();
      const userName = this.sessionService.getUsername();
      if (!userName) {
        return of( AuthorizationSuccessAction.create([]));
      }
      try {
        return this.authorizationService.loadUserOperations(tenantId, userName, action.payload.includeStorage)
        .pipe(map((authorization => AuthorizationSuccessAction.create(authorization))));
      }
      catch (error) {
        return of (AuthorizationFailedAction.create('Error getting authorization. Details: ' + JSON.stringify(error)));
      }
    })
  );

  @Effect({ dispatch: false })
  public authorizationSuccess$ = this.actions$.pipe(
    ofType(AuthorizationActionTypes.AUTHORIZATION_SUCCESS),
    tap((action: AuthorizationSuccessAction) => {
      this.storageService.setItem(USER_OPS_KEY, action.payload.authorization.join());
    })
  );

  @Effect({ dispatch: false })
  public authorizationFailed$ = this.actions$.pipe(
    ofType(AuthorizationActionTypes.AUTHORIZATION_FAILURE),
    tap((action: AuthorizationFailedAction) => {
      this.sessionService.userLogout();
    })
  );

  constructor(private actions$: Actions<AuthorizationActions>,
              private authorizationService: AuthorizationService,
              private storageService: StorageService,
              private sessionService: SessionService) {}
}
