import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import * as authorization from './authorization.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AuthorizationsEffects } from './authorization.effects';

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature('authorization', authorization.reducer),
    EffectsModule.forFeature([AuthorizationsEffects])
  ]
})
export class AuthorizationStoreModule {}
