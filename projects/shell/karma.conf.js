// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-safari-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../../coverage/base-components'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome', 'Safari', 'ChromeHeadless', 'ChromeHeadlessNoSandbox'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },
    singleRun: false,
    restartOnFileChange: true,
    proxies: {
       // The definitions below get recognized, withour a 404 error. However they generate a
       // 431 error (header too large). Also, I need to individually list the icons.
      '/assets/svg/ic-algosec.svg': 'http://localhost:9876/assets/svg/ic-algosec.svg',
      '/assets/svg/ic-menu-home.svg': 'http://localhost:9876/assets/svg/ic-menu-home.svg',
      '/assets/svg/ic-menu-risks.svg': 'http://localhost:9876/assets/svg/ic-menu-risks.svg',
      '/assets/svg/ic-menu-network-policy.svg': 'http://localhost:9876/assets/svg/ic-menu-network-policy.svg',
      '/assets/svg/ic-menu-inventory.svg': 'http://localhost:9876/assets/svg/ic-menu-inventory.svg',
      '/assets/svg/clouds-light.svg': 'http://localhost:9876/assets/svg/clouds-light.svg',
      '/assets/svg/ash-plug-light.svg': 'http://localhost:9876/assets/svg/ash-plug-light.svg',
      '/assets/svg/users-cog-light.svg': 'http://localhost:9876/assets/svg/users-cog-light.svg',
      '/assets/svg/cog-solid.svg': 'http://localhost:9876/assets/svg/cog-solid.svg',
    
      '/assets/svg/angle-up-solid.svg': 'http://localhost:9876/assets/svg/angle-up-solid.svg',
      '/assets/svg/angle-down-solid.svg': 'http://localhost:9876/assets/svg/angle-down-solid.svg',
      '/assets/svg/ic-algoseccloud.svg': 'http://localhost:9876/assets/svg/ic-algoseccloud.svg'
      }
  });
};
