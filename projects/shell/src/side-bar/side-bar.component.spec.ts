import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';
import { DividerComponent } from 'projects/base-components/src/shared/divider/divider.component';
import { SvgIconComponent } from 'projects/base-components/src/shared/svg-icon/svg-icon.component';
import { SideBarComponent } from './side-bar.component';

describe('side-bar component tests', () => {
  let sideBarComponent;
  let fixture: ComponentFixture<SideBarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatIconModule, MatDividerModule, RouterTestingModule, HttpClientModule],
      declarations: [SideBarComponent, SvgIconComponent, DividerComponent]}).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarComponent);
    sideBarComponent = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  // beforeEach(() => {
  //  spyOn(editableTextField.save, 'emit');
  // });

  it('should populate correctly', () => {
    sideBarComponent.sidebarAlgosecIcon = 'ic-algosec';
    sideBarComponent.sidebarTopButtons = [
        {
            buttonLabel: 'HOME',
            buttonLink: '',
            buttonIconName: 'ic-menu-home'
        }
    ];

    sideBarComponent.sidebarBottomButtons = [
        {
            buttonLabel: 'RISKS',
            buttonLink: 'risks',
            buttonIconName: 'ic-menu-risks'
        },
        {
            buttonLabel: 'INVENTORY',
            buttonLink: 'inventory',
            buttonIconName: 'ic-menu-inventory'
        },
        {
            buttonLabel: 'NETWORK POLICIES',
            buttonLink: 'network-policy',
            buttonIconName: 'ic-menu-network-policy'
        }
    ];

    sideBarComponent.sidebarSettingsButtons = [
        {
            buttonLabel: 'ACCOUNTS',
            buttonLink: 'settings/account',
            buttonIconName: 'clouds-light'
        },
        {
            buttonLabel: 'ASMS INTEGRATION',
            buttonLink: 'settings/asms-integration',
            buttonIconName: 'ash-plug-light'
        },
        {
            buttonLabel: 'ACCESS MANAGEMENT',
            buttonLink: 'settings/access-management',
            buttonIconName: 'users-cog-light'
        }
    ];
    // sideBarComponent.settingsIcon = 'cog-solid';

    fixture.detectChanges();
    expect(sideBarComponent.forcefullyCollapseSidebar).toBe(false, 'forcefully collapsing at start');
    expect(sideBarComponent.sidebarAlgosecIcon).toBe('ic-algosec', 'algosec icon not set correctly');
    expect(sideBarComponent.sidebarTopButtons).toEqual([
      {
          buttonLabel: 'HOME',
          buttonLink: '',
          buttonIconName: 'ic-menu-home'
      }], 'top buttons not set correctly');

    expect(sideBarComponent.sidebarSettingsButtons).toEqual( [
        {
            buttonLabel: 'ACCOUNTS',
            buttonLink: 'settings/account',
            buttonIconName: 'clouds-light'
        },
        {
            buttonLabel: 'ASMS INTEGRATION',
            buttonLink: 'settings/asms-integration',
            buttonIconName: 'ash-plug-light'
        },
        {
            buttonLabel: 'ACCESS MANAGEMENT',
            buttonLink: 'settings/access-management',
            buttonIconName: 'users-cog-light'
        }
    ], 'settings buttons not set correctly');

    expect(sideBarComponent.sidebarBottomButtons).toEqual( [
    {
        buttonLabel: 'RISKS',
        buttonLink: 'risks',
        buttonIconName: 'ic-menu-risks'
    },
    {
        buttonLabel: 'INVENTORY',
        buttonLink: 'inventory',
        buttonIconName: 'ic-menu-inventory'
    },
    {
        buttonLabel: 'NETWORK POLICIES',
        buttonLink: 'network-policy',
        buttonIconName: 'ic-menu-network-policy'
    }
    ], 'bottom buttons not set correctly');
  });

  it('should collapse sidebar on clicking it', () => {
    sideBarComponent.onSidebarClicked(null);
    fixture.detectChanges();
    expect(sideBarComponent.forcefullyCollapseSidebar).toBe(true, 'not forcefully collapsing on sidebar click');
  });
  it('should not collapse sidebar on mouse enter', () => {
    sideBarComponent.onMouseEnter(null);
    fixture.detectChanges();
    expect(sideBarComponent.forcefullyCollapseSidebar).toBe(false, 'forcefully collapsing on mouse enter');
  });
  it('should not collapse sidebar on mouse leave', () => {
    sideBarComponent.onMouseLeave(null);
    fixture.detectChanges();
    expect(sideBarComponent.forcefullyCollapseSidebar).toBe(false, 'forcefully collapsing on mouse leave');
  });

  
});
