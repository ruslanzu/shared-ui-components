export interface SidebarButtonParams {
  buttonLabel: string;
  buttonLink: string;
  buttonIconName: string;
}
