import { Component, Input, OnInit } from '@angular/core';
import { SidebarButtonParams } from './sidebar-button-params';

@Component({
  selector: 'ash-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  @Input() public sidebarTopButtons: SidebarButtonParams[] = [];
  @Input() public sidebarBottomButtons: SidebarButtonParams[] = [];
  @Input() public sidebarSettingsButtons: SidebarButtonParams[] = [];
  @Input() public sidebarAlgosecIcon = 'ic-algosec';
  @Input() public settingsIcon = '';
  public forcefullyCollapseSidebar = false;
  public isPinned = false;
  public pinnedIcon = 'caret-left-solid';

  constructor() {}

  public ngOnInit() {
  }

  public onSidebarClicked(event: any) {
    this.forcefullyCollapseSidebar = true;
  }

  public onPinClicked() {
    this.isPinned = !this.isPinned;
  }

  public onMouseLeave() {
    this.forcefullyCollapseSidebar = false;
  }

  public onMouseEnter() {
    this.forcefullyCollapseSidebar = false;
  }

  public getPinnedIcon(): string {
    return this.isPinned ? 'caret-left-solid' : 'caret-right-solid';
  }
  // temp feature flag - allow risk option only for specific tenants
  get shouldDisableRiskFeature(): boolean {
    // const tenant = this.sessionService.getTenantId(); // @TODO
    const tenant = '';
    return ['1069018161957230'].includes(tenant);
  }
}
