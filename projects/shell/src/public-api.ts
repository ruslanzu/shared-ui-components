/*
 * Public API Surface of shell
 */

export * from './shell.module';
export * from './side-bar/side-bar.component';
export * from './nav-bar-user-management/nav-bar-user-management.component';
export * from './nav-bar/nav-bar.component';
