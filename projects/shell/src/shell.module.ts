import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideBarComponent } from './side-bar/side-bar.component';
import { RouterModule } from '@angular/router';
import { BaseComponentsModule } from '@algosec/base-components';
import { HttpClientModule } from '@angular/common/http';
import { NavBarUserManagementComponent } from './nav-bar-user-management/nav-bar-user-management.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    SideBarComponent,
    NavBarUserManagementComponent,
    NavBarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    BaseComponentsModule,
    HttpClientModule
  ],
  exports: [
    CommonModule,
    SideBarComponent,
    NavBarComponent,
    HttpClientModule
  ]
})
export class ShellModule { }
