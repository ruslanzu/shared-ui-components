import { Component, EventEmitter, Input, Output } from '@angular/core';
@Component({
  selector: 'ash-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {
  @Input() public username = '';
  @Input() public title = '';
  @Input() public isTreeDisplayed = false;
  @Input() public isTreeOpen = false;
  @Input() public icon = '';
  @Input() public isBetaPage = false;

  @Output() public toggleTree = new EventEmitter<void>();
  @Output() public logout = new EventEmitter<void>();

  @Input() public navbarLogo = 'ic-algoseccloud';
  @Input() public navbarCloseTreeIcon = 'angle-down-solid';
  @Input() public navbarOpenTreeIcon =  'angle-up-solid';

  constructor() {}

  public toggle(): void {
    this.toggleTree.emit();
  }

  public userLogout(): void {
    this.logout.emit();
  }
}
