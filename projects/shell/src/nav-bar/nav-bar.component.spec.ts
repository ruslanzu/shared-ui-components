import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';
import { DividerComponent } from 'projects/base-components/src/shared/divider/divider.component';
import { SvgIconComponent } from 'projects/base-components/src/shared/svg-icon/svg-icon.component';
import { NavBarUserManagementComponent } from '../nav-bar-user-management/nav-bar-user-management.component';
import { NavBarComponent } from '../nav-bar/nav-bar.component';

describe('nav-bar component tests', () => {
  let navBarComponent;
  let fixture: ComponentFixture<NavBarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatIconModule, MatDividerModule, RouterTestingModule, HttpClientModule],
      declarations: [NavBarComponent, NavBarUserManagementComponent, SvgIconComponent, DividerComponent]}).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarComponent);
    navBarComponent = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
   spyOn(navBarComponent.toggleTree, 'emit');
   spyOn(navBarComponent.logout, 'emit');
  });

  it('should populate correctly', () => {
    navBarComponent.title = 'All Inventory';
    navBarComponent.username = 'Someone';

    fixture.detectChanges();
    expect(navBarComponent.title).toBe('All Inventory', 'Title incorrect');
    expect(navBarComponent.username).toBe('Someone', 'Username incorrect');
    expect(navBarComponent.isTreeDisplayed).toBe(false, 'isTreeDisplayed is not initially false');
    expect(navBarComponent.isTreeOpen).toBe(false, 'isTreeOpen is not initially false');
    expect(navBarComponent.icon).toBe('', 'left-side icon is not initially empty');
    expect(navBarComponent.navbarLogo).toBe('ic-algoseccloud', 'Algosec logo is not initially set');
    expect(navBarComponent.navbarOpenTreeIcon).toBe('angle-up-solid', 'Open tree icon is not initially set');
    expect(navBarComponent.navbarCloseTreeIcon).toBe('angle-down-solid', 'Close tree icon is not initially set');
});

  it('should set correct icons', () => {
    navBarComponent.navbarOpenTreeIcon = 'angle-up-solid';
    navBarComponent.navbarCloseTreeIcon = 'angle-down-solid';
    navBarComponent.navbarLogo = 'ic-algoseccloud';
    fixture.detectChanges();
    expect(navBarComponent.navbarOpenTreeIcon).toBe('angle-up-solid', 'Open tree icon not set');
    expect(navBarComponent.navbarCloseTreeIcon).toBe('angle-down-solid', 'Close tree icon not set');
    expect(navBarComponent.navbarLogo).toBe('ic-algoseccloud', 'Algosec logo icon not set');
    });

  it('should toggle correctly', () => {
        navBarComponent.toggle();
        expect(navBarComponent.toggleTree.emit).toHaveBeenCalled();
    });

  it('should handle logout correctly', () => {
        navBarComponent.userLogout();
        expect(navBarComponent.logout.emit).toHaveBeenCalled();
    });
});


