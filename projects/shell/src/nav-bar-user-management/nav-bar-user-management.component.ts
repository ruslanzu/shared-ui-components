import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ash-nav-bar-user-management',
  templateUrl: './nav-bar-user-management.component.html',
  styleUrls: ['./nav-bar-user-management.component.scss']
})
export class NavBarUserManagementComponent {
  @Input() public username: string;
  @Output() public logout = new EventEmitter<void>();
  @Input()  public eulaPdfLink: string;
  @Input()  public cloudFlowHelpCenter: string;
  constructor() {}

  public userLogout(): void {
    this.logout.emit();
  }
}
