import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private sessionService: SessionService) {}

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.sessionService
      .isValidSession()
      .then((isUserLoggedIn: boolean) => {
        if (!isUserLoggedIn) {
          return this.userLoggedOut(state.url);
        }
        return true;
      })
      .catch(() => {
        return this.userLoggedOut(state.url);
      });
  }

  private userLoggedOut(url: string): boolean {
    this.sessionService.userLogout();
    return false;
  }
}
