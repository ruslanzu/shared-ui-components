import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { AUTHORIZATION_HEADER_KEY, HEADERS_BASE_CONFIG, SKIP_TOKEN_INTERCEPTOR, TOKEN_PREFIX } from '../models/constants';
import { SessionService } from '../services/session.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private readonly headersConfig = { ...HEADERS_BASE_CONFIG };

  constructor(private authService: AuthService,
              private sessionService: SessionService) {}

  public  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (TokenInterceptor.skipTokenHeader(req)) {
      req.headers.delete(SKIP_TOKEN_INTERCEPTOR);
      return next.handle(req);
    } else {
      if (from(this.validateSession())) {
        return this.returnRequestWithFullHeaders(req, next);
      } else {
        return next.handle(req);
      }
    }
  }

  private async validateSession(): Promise<boolean> {
    const isValidSession: boolean = await this.sessionService.isValidSession();
    if (!isValidSession) {
      console.log('Invalid session, will login');
      this.sessionService.userLogin();
      return false;
    }
    return true;
  }
  private returnRequestWithFullHeaders(req: HttpRequest<any>, next: HttpHandler) {
    return from(this.authService.getToken()).pipe(
      switchMap((token: string) => {
        this.headersConfig[AUTHORIZATION_HEADER_KEY] = TOKEN_PREFIX + token;
        const reqWithToken = req.clone({ setHeaders: this.headersConfig });
        return next.handle(reqWithToken);
      })
    );
  }

  private static skipTokenHeader(req: HttpRequest<any>): boolean {
    return req.headers.has(SKIP_TOKEN_INTERCEPTOR);
  }
}
