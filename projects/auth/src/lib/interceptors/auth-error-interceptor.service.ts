import { SessionService } from '../services/session.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpStatusCode } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router, private sessionService: SessionService) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(err => {
        if (err.status === HttpStatusCode.Unauthorized) {
          // Yoni todo, redirect to login
          const error = err.error || err.statusText;
          console.error('Error in Interceptor: ' + error);
          this.sessionService.userLogout();
        }
        return throwError(err);
      })
    );
  }
}
