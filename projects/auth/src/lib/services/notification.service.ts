import { Injectable } from '@angular/core';
import * as socketIo from 'socket.io-client';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { AUTHORIZATION_HEADER_KEY, HEADERS_BASE_CONFIG, TOKEN_PREFIX } from '@models/constants/constants';
import { NOTIFY_SERVICE_PATH, NOTIFY_SERVICE_VERSION } from './api/micro-services-paths';
import { NotificationType, NotificationTypesToData } from '@shared-DTOs/notifications-manager/notifications-client.types';


@Injectable({ providedIn: 'root' })
export class NotificationService {
  private socket;

  constructor() {}

  public initConnection(token: string): void {
    if (!this.socket || this.socket.disconnected) {
      const extraHeaders = {
        ...HEADERS_BASE_CONFIG,
        [AUTHORIZATION_HEADER_KEY]: TOKEN_PREFIX + token
      };
      this.socket = socketIo(environment.api_url, {
        path: `${NOTIFY_SERVICE_PATH}${NOTIFY_SERVICE_VERSION}/socket.io`,
        transports: ['websocket', 'polling'],
        reconnectionAttempts: 5,
        transportOptions: {
          polling: {
            extraHeaders
          }
        }
      });
    }
  }

  public closeConnection(): void {
    if (this.socket && this.socket.connected) {
      this.socket.close();
    }
  }

  public onMessageReceived(event: NotificationType): Observable<NotificationTypesToData[NotificationType]> {
    return new Observable<NotificationTypesToData[NotificationType]>(observer => {
      this.socket.on(event, (data: any) => observer.next(data));
    });
  }
}
