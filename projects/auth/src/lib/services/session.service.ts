import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription, timer } from 'rxjs';
import { AuthService } from '../auth.service';
import { SESSION_TIMEOUT_MILLI_SEC, TENANT_ID_KEY, USERNAME_KEY, USER_ACTIVITY_KEY } from '../models/constants';
import { StorageService } from './storage.service';

@Injectable({ providedIn: 'root' })
export class SessionService {
  private sessionTimeout$: Observable<number>;
  private timeoutSubscription: Subscription;
  private productType = '';
  constructor(
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  public setProductType(setProductType: string): void {
    this.productType = setProductType;
  }

  public userLogin(): void {
    if (this.productType === '') {
      throw new Error('Product type has not been set');
    }
    let domain = window.location.origin;
    domain = domain.endsWith('/') ? domain : domain + '/';
    const href = domain + 'algosaas/index.html?product=' + this.productType + '&action=login&source=' + window.location.href;
    this.document.location.href = href;
  }

  public userLogout(): void {
    if (this.productType === '') {
      throw new Error('Product type has not been set');
    }
    this.authService.logout().finally(() => {
      this.clearSessionParamsFromStorage();
      let domain = window.location.origin;
      domain = domain.endsWith('/') ? domain : domain + '/';
      const href = domain + 'algosaas/index.html?product=' + this.productType + '&action=logout&source=' + window.location.href;
      this.document.location.href =  href;
    });
  }

  public getTenantId(): string {
    return this.storageService.getItem(TENANT_ID_KEY);
  }
  public getUsername(): string {
    return this.storageService.getItem(USERNAME_KEY);
  }

  public async isValidSession(): Promise<boolean> {
    try {
      const isValidSession: boolean = await this.authService.isValidSession();
      const isActiveSession: boolean = this.isActiveSession();
      return isValidSession && isActiveSession;
    } catch (error) {
      console.log ('isValidSession error');
      return false;
    }
  }

  public updateUserActivity(isFirstLogin?: boolean): void {
    if (isFirstLogin || this.isActiveSession()) {
      const timeout: number = new Date().getTime() + SESSION_TIMEOUT_MILLI_SEC;
      this.sessionTimeout$ = timer(SESSION_TIMEOUT_MILLI_SEC);
      this.subscribeTimeout();
      this.storageService.setItem(USER_ACTIVITY_KEY, timeout + '');
    }
  }

  private sessionTimeoutTriggered(): void {
    if (this.isActiveSession()) {
      this.syncDuplicateTabsWithStorageTimeout();
    } else {
      this.userLogout();
    }
  }

  private subscribeTimeout() {
    this.unsubscribePreviousTimeout();
    this.timeoutSubscription = this.sessionTimeout$.subscribe(() => {
      this.sessionTimeoutTriggered();
    });
  }

  private unsubscribePreviousTimeout() {
    if (this.timeoutSubscription) {
      this.timeoutSubscription.unsubscribe();
    }
  }

  /**
   * This function should handle a situation of multiple tabs open.
   * The user activity is update in the storage by another tab activity
   * but the suspended tab is not sync with the storage
   * */
  private syncDuplicateTabsWithStorageTimeout(): void {
    const now: number = new Date().getTime();
    const timeoutLeft: number = Number(this.storageService.getItem(USER_ACTIVITY_KEY)) - now;
    this.sessionTimeout$ = timer(timeoutLeft);
    this.subscribeTimeout();
  }

  private storeTenantId(tenantId: string) {
    this.storageService.setItem(TENANT_ID_KEY, tenantId);
  }

  private clearSessionParamsFromStorage(): void {
    const tenantId: string = this.getTenantId();
    this.storageService.clearStorage();
    this.storeTenantId(tenantId);
  }

  private isActiveSession(): boolean {
    const currentTimeMilliSec: number = new Date().getTime();
    const sessionTimeoutMilliSec: number = Number(this.storageService.getItem(USER_ACTIVITY_KEY));
    return currentTimeMilliSec < sessionTimeoutMilliSec;
  }
}
