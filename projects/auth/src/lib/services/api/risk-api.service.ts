import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { RISK_SERVICE_PATH, RISK_SERVICE_VERSION } from './micro-services-paths';
import { Risk, RiskDetails, TriggerStatusTypeValues } from '@common-ts/micro-service-clients/risk-service-client/risk-service-client.types';
import { RisksFilterBody } from '@common-ts/filters/filtering.types';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RiskApiService {
  private static readonly resource = `${RISK_SERVICE_PATH}${RISK_SERVICE_VERSION}/results/detected-risks`;
  private static readonly riskStatusResource = `${RISK_SERVICE_PATH}${RISK_SERVICE_VERSION}/results/risks/status`;
  private static readonly triggerStatusResource = `${RISK_SERVICE_PATH}${RISK_SERVICE_VERSION}/results/trigger/status`;
  private static readonly possibleValuesResource = `${RISK_SERVICE_PATH}${RISK_SERVICE_VERSION}/filters/possible-values`;
  private static readonly RISK_MOCK_DATA_KEY: string = 'mock-risk-data';

  // temp feature flag
  private readonly params: HttpParams;

  constructor(private apiService: ApiService) {
    // temp feature flag
    this.params = new HttpParams().set('real', RiskApiService.isRealRiskData());
  }

  public getRisks(filters: RisksFilterBody): Observable<Risk[]> {
    return this.apiService.post<Risk[]>(RiskApiService.resource, filters);
  }

  public getRisk(id: string, activeFilters: any): Observable<RiskDetails> {
    return this.apiService.post<RiskDetails>(`${RiskApiService.resource}/${encodeURIComponent(id)}`, activeFilters);
  }

  public suppressRisk(riskId: string, comment: string): Observable<string> {
    return this.apiService.patch<string>(`${RiskApiService.riskStatusResource}/${riskId}`, {
      comment,
      status: TriggerStatusTypeValues.suppressed
    });
  }

  public activateRisk(riskId: string): Observable<string> {
    return this.apiService.patch<string>(`${RiskApiService.riskStatusResource}/${riskId}`, {
      status: TriggerStatusTypeValues.active
    });
  }

  public suppressTrigger(triggerId: string, riskId: string, global: boolean, comment: string): Observable<string> {
    return this.apiService.patch<string>(`${RiskApiService.triggerStatusResource}`, {
      comment,
      status: TriggerStatusTypeValues.suppressed,
      applyToAll: global.toString(),
      triggers: [{ 'evidenceId': triggerId, 'riskCode': riskId }]
    });
  }

  public activateTrigger(triggerId: string, riskId: string): Observable<string> {
    return this.apiService.patch<string>(`${RiskApiService.triggerStatusResource}`, {
      status: TriggerStatusTypeValues.active,
      triggers: [{ 'evidenceId': triggerId, 'riskCode': riskId }]
    });
  }

  public getFilterPossibleValues(): Observable<RisksFilterBody> {
    return this.apiService.post<RisksFilterBody>(`${RiskApiService.possibleValuesResource}`, {});
  }

  private static isRealRiskData(): string {
    const mockRiskParam: string = localStorage.getItem(RiskApiService.RISK_MOCK_DATA_KEY);
    return mockRiskParam === 'true' ? 'false' : 'true';
  }
}
