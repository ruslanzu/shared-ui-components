import { Injectable } from '@angular/core';

import { NetworkPoliciesTypes } from '@network-policy/models/network-policies-types.enum';
import { SecurityControl } from '@network-policy/models/security-control';
import { PolicyForm } from '@network-policy/models/policy-form/policy-form';
import { ApiService } from '@services/api/api.service';
import { Observable } from 'rxjs';
import {
  PolicyTypeToCountData,
  PolicyCommitData,
  PolicyDiscardData,
  PolicyFilterId,
  PolicyOperationRequest,
  PolicyMemberAlteration,
  RiskyRuleData,
  GenericPolicyState,
  TrafficSimulationQueryResults
} from '@shared-DTOs/policies-manager-client/policies-client.types';
import { PolicyDetails } from '@shared-DTOs/policies-manager-client/policies-client.types';
import {
  AzfwNetworkCollectionFilter,
  AzfwNetworkRuleFilter,
  AzfwApplicationRuleFilter
} from '@shared-DTOs/policies-manager-client/policy-managers/policies-azfw-client.types';
import {
  NETWORK_POLICY_SERVICE_PATH,
  NETWORK_POLICY_SERVICE_VERSION,
  CEDA_SERVICE_PATH,
  CEDA_SERVICE_VERSION
} from '@services/api/micro-services-paths';
import { HttpParams } from '@angular/common/http';
import {
  NsgRuleFilter,
  AzureApplicationSecurityGroupsOfResourceGroup,
  AzureServiceTagTreeNode
} from '@common-ts/micro-service-clients/policies-manager-client/policy-managers/policies-nsg-client.types';
import {
  AwsSgRuleFilter,
  AwsIcmpTypeNode,
  AwsSecurityGroup
} from '@shared-DTOs/policies-manager-client/policy-managers/policies-aws-client.types';
import { AccountVendor } from '@account/models/account-vendor.enum';
import { BaseRuleForm, BaseSecurityControlRuleForm } from '@network-policy/models/network-policy-rules-grid/rule-form/base-rule-form';
import { NetworkPolicyGridTabs } from '@network-policy/models/network-policy-rules-grid/network-policy-grid-tabs.enum';
import { IAzureFirewallRuleFormBase } from '@network-policy/models/network-policy-rules-grid/rule-form/azure-firewall-rule/azure-firewall-rule-type';

@Injectable({ providedIn: 'root' })
export class NetworkPolicyApiService {
  private static networkTypes = {
    [NetworkPoliciesTypes.AZURE_FIREWALL]: 'azfws',
    [NetworkPoliciesTypes.AZURE_NSG]: 'nsgs',
    [NetworkPoliciesTypes.AWS_SGS]: 'sgs'
  };

  private static ruleDirectionRoute = {
    [NetworkPolicyGridTabs.INBOUND]: 'inbound-rules',
    [NetworkPolicyGridTabs.OUTBOUND]: 'outbound-rules',
    [NetworkPolicyGridTabs.NETWORK]: 'network-collections',
    [NetworkPolicyGridTabs.APPLICATION]: 'application-collections'
  };

  private static readonly networkPolicyBaseApi = `${NETWORK_POLICY_SERVICE_PATH}${NETWORK_POLICY_SERVICE_VERSION}/policies`;

  constructor(private apiService: ApiService) {}

  /****************************
   * Policy
   ****************************/

  public getPolicyTypes(): Observable<PolicyTypeToCountData[]> {
    return this.apiService.get<PolicyTypeToCountData[]>(`${NetworkPolicyApiService.networkPolicyBaseApi}/types`);
  }

  public getSecurityControls(policyType: NetworkPoliciesTypes, key: string): Observable<SecurityControl[]> {
    const params = new HttpParams().set('descriptionContaining', key).set('maxSuggestions', '10');

    return this.apiService.get<SecurityControl[]>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policyType]}/candidate-members`,
      params
    );
  }

  public getPolicyData(policyType: NetworkPoliciesTypes, policyId: string): Observable<GenericPolicyState> {
    return this.apiService.get<GenericPolicyState>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policyType]}/${policyId}`
    );
  }

  public getPoliciesFullDetails(policyType: NetworkPoliciesTypes): Observable<PolicyDetails[]> {
    return this.apiService.get<PolicyDetails[]>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/` + `${NetworkPolicyApiService.networkTypes[policyType]}/details`
    );
  }

  public createNewPolicy(policy: PolicyForm, autoLock: boolean): Observable<string> {
    return this.apiService.post<string>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policy.policyType]}`,
      {
        autoLock,
        name: policy.name,
        description: policy.description,
        members: policy.members
      }
    );
  }

  public mergePolicy(policy: PolicyForm): Observable<string> {
    return this.apiService.post<string>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policy.policyType]}/merge`,
      {
        name: policy.name,
        description: policy.description,
        policies: policy.members
      }
    );
  }

  public editPolicy(policy: PolicyForm, policyMemberAlteration: PolicyMemberAlteration[]): Observable<void> {
    return this.apiService.patch(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policy.policyType]}/${policy.id}`,
      {
        name: policy.name,
        description: policy.description,
        members: policyMemberAlteration
      }
    );
  }

  public alterPolicyMembers(
    id: string,
    policyType: NetworkPoliciesTypes,
    policyMemberAlteration: PolicyMemberAlteration[]
  ): Observable<void> {
    return this.apiService.patch(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policyType]}/${id}/members`,
      {
        members: policyMemberAlteration
      }
    );
  }

  public deletePolicy(policyId: string, networkPolicyType: NetworkPoliciesTypes): Observable<void> {
    return this.apiService.delete(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[networkPolicyType]}/${policyId}`
    );
  }

  public lockPolicy(policyId: string, networkPolicyType: NetworkPoliciesTypes): Observable<void> {
    return this.apiService.post(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[networkPolicyType]}/${policyId}/lock`
    );
  }

  public commitPolicy(policyId: string, networkPolicyType: NetworkPoliciesTypes): Observable<PolicyCommitData> {
    return this.apiService.post(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[networkPolicyType]}/${policyId}/commit`
    );
  }

  public discardPolicy(policyId: string, networkPolicyType: NetworkPoliciesTypes): Observable<PolicyDiscardData> {
    return this.apiService.post(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[networkPolicyType]}/${policyId}/discard`
    );
  }

  /****************************
   * TSQ
   ****************************/
  public issueTrafficSimulationQuery(policyId: string, networkPolicyType: NetworkPoliciesTypes, ruleId: string, memberCfId: string, queryId: string): Observable<void> {
    let requestBody = {
         ruleId,
         queryId
    };
    if (memberCfId) {
      requestBody['memberCfid'] = memberCfId;
    }

    return this.apiService.post(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[networkPolicyType]}/${policyId}/traffic-simulation-query`, requestBody);
  }

  public statusTrafficSimulationQuery(): Observable<TrafficSimulationQueryResults> {
    return this.apiService.get<TrafficSimulationQueryResults>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/traffic-simulation-query`);
  }

  /****************************
   * General Function Rules
   ****************************/

  public createRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: BaseRuleForm,
    ruleFilter: any
  ): Observable<PolicyFilterId[]> {
    switch (policyType) {
      case NetworkPoliciesTypes.AWS_SGS:
      case NetworkPoliciesTypes.AZURE_NSG:
        return this.createSecurityControlRule(policyType, policyId, <BaseSecurityControlRuleForm>formData, ruleFilter);

      case NetworkPoliciesTypes.AZURE_FIREWALL:
        return this.createFirewallRule(policyType, policyId, <IAzureFirewallRuleFormBase>formData, ruleFilter);
    }
  }

  public updateRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: BaseRuleForm,
    ruleFilter: any
  ): Observable<PolicyFilterId[]> {
    switch (policyType) {
      case NetworkPoliciesTypes.AWS_SGS:
      case NetworkPoliciesTypes.AZURE_NSG:
        return this.updateSecurityControlRule(policyType, policyId, <BaseSecurityControlRuleForm>formData, ruleFilter);

      case NetworkPoliciesTypes.AZURE_FIREWALL:
        return this.updateFirewallRule(policyType, policyId, <IAzureFirewallRuleFormBase>formData, ruleFilter);
    }
  }

  public updateInstallMembersForRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: BaseRuleForm,
    operations: PolicyOperationRequest[]
  ): Observable<void> {
    switch (policyType) {
      case NetworkPoliciesTypes.AWS_SGS:
      case NetworkPoliciesTypes.AZURE_NSG:
        return this.updateSecurityControlInstallMembersForRule(policyType, policyId, <BaseSecurityControlRuleForm>formData, operations);

      case NetworkPoliciesTypes.AZURE_FIREWALL:
        return this.updateFirewallInstallMembersForRule(policyType, policyId, <IAzureFirewallRuleFormBase>formData, operations);
    }
  }

  public deleteRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    ruleDirection: NetworkPolicyGridTabs,
    ruleId: string,
    collectionId?: string
  ): Observable<void> {
    switch (policyType) {
      case NetworkPoliciesTypes.AWS_SGS:
      case NetworkPoliciesTypes.AZURE_NSG:
        return this.deleteSecurityControlRule(policyType, policyId, ruleDirection, ruleId);

      case NetworkPoliciesTypes.AZURE_FIREWALL:
        return this.deleteFirewallRule(policyType, policyId, ruleDirection, ruleId, collectionId);
    }
  }

  public getAzureServiceTagsByType(policyType: NetworkPoliciesTypes): Observable<AzureServiceTagTreeNode[]> {
    return this.apiService.get<AzureServiceTagTreeNode[]>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policyType]}/service-tags`
    );
  }

  public createFirewallCollection(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    ruleDirection: NetworkPolicyGridTabs,
    collectionFilter: AzfwNetworkCollectionFilter
  ): Observable<PolicyFilterId> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, ruleDirection);
    return this.apiService.post<PolicyFilterId>(requestRoute, { collectionFilter });
  }

  /****************************
   * Azure Firewalls
   ****************************/

  public getAzfwFqdnTags(): Observable<string[]> {
    return this.apiService.get<string[]>(`${NetworkPolicyApiService.networkPolicyBaseApi}/azfws/fqdn-tags`);
  }

  public acknowledgeOperations(policyId: string, networkPolicyType: NetworkPoliciesTypes): Observable<number> {
    return this.apiService.post(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/` +
        `${NetworkPolicyApiService.networkTypes[networkPolicyType]}/` +
        `${policyId}/acknowledge-operations`
    );
  }

  /****************************
   * Azure NSG General
   ****************************/

  public getAzureApplicationSecurityGroup(): Observable<AzureApplicationSecurityGroupsOfResourceGroup[]> {
    return this.apiService.get<AzureApplicationSecurityGroupsOfResourceGroup[]>(
      `${NetworkPolicyApiService.networkPolicyBaseApi}/nsgs/application-security-groups`
    );
  }

  /****************************
   * AWS SG General
   ****************************/

  public getAwsSecurityGroup(): Observable<AwsSecurityGroup[]> {
    return this.apiService.get<AwsSecurityGroup[]>(`${NetworkPolicyApiService.networkPolicyBaseApi}/sgs/security-groups`);
  }

  public getIcmpTypes(): Observable<AwsIcmpTypeNode[]> {
    return this.apiService.get<any[]>(`${NetworkPolicyApiService.networkPolicyBaseApi}/sgs/icmp-types`);
  }

  public discoverPolicyIdByRuleAndSg(securityControlCfid: string, ruleCfid: string, vendor: AccountVendor): Observable<RiskyRuleData> {
    const params = new HttpParams()
      .set('securityControlCfid', securityControlCfid)
      .set('ruleCfid', ruleCfid)
      .set('vendor', vendor.toString().toLowerCase());
    return this.apiService.get(`${NetworkPolicyApiService.networkPolicyBaseApi}/risky-rule-data`, params);
  }

  /*********************************
   * General privet Function Rules
   *********************************/
  private createSecurityControlRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: BaseSecurityControlRuleForm,
    ruleFilter: NsgRuleFilter | AwsSgRuleFilter
  ): Observable<PolicyFilterId[]> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, formData.ruleDirection);
    return this.apiService.post(requestRoute, {
      ruleFilter,
      comment: formData.description,
      installOnMemberCfids: formData.installedOn
    });
  }

  private createFirewallRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: IAzureFirewallRuleFormBase,
    ruleFilter: AzfwNetworkRuleFilter | AzfwApplicationRuleFilter
  ): Observable<PolicyFilterId[]> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, formData.ruleDirection);
    return this.apiService.post(`${requestRoute}/${formData.collection}/rules`, {
      ruleFilter,
      installOnMemberCfids: formData.installedOn
    });
  }

  private updateSecurityControlRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: BaseSecurityControlRuleForm,
    ruleFilter: NsgRuleFilter | AwsSgRuleFilter
  ): Observable<PolicyFilterId[]> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, formData.ruleDirection);
    return this.apiService.patch(`${requestRoute}/${formData.ruleId}`, {
      newRuleFilterOrId: ruleFilter,
      comment: formData.description
    });
  }

  private updateFirewallRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: IAzureFirewallRuleFormBase,
    ruleFilter: AzfwNetworkRuleFilter | AzfwApplicationRuleFilter
  ): Observable<PolicyFilterId[]> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, formData.ruleDirection);
    return this.apiService.patch(`${requestRoute}/${formData.collection}/rules/${formData.ruleId}`, {
      newCollectionId: formData.collection,
      newRuleFilterOrId: ruleFilter
    });
  }

  private updateSecurityControlInstallMembersForRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: BaseSecurityControlRuleForm,
    operations: PolicyOperationRequest[]
  ): Observable<void> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, formData.ruleDirection);
    return this.apiService.post(`${requestRoute}/${formData.ruleId}/operations`, {
      operations
    });
  }

  private updateFirewallInstallMembersForRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    formData: IAzureFirewallRuleFormBase,
    operations: PolicyOperationRequest[]
  ): Observable<void> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, formData.ruleDirection);
    return this.apiService.post(`${requestRoute}/${formData.collection}/rules/${formData.ruleId}/operations`, {
      operations
    });
  }

  private deleteSecurityControlRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    ruleDirection: NetworkPolicyGridTabs,
    ruleId: string
  ): Observable<void> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, ruleDirection);
    return this.apiService.delete(`${requestRoute}/${ruleId}`);
  }

  private deleteFirewallRule(
    policyType: NetworkPoliciesTypes,
    policyId: string,
    ruleDirection: NetworkPolicyGridTabs,
    ruleId: string,
    collectionId: string
  ): Observable<void> {
    const requestRoute: string = NetworkPolicyApiService.getRulesEndpointRoute(policyType, policyId, ruleDirection);
    return this.apiService.delete(`${requestRoute}/${collectionId}/rules/${ruleId}`);
  }

  private static getRulesEndpointRoute(policyType: NetworkPoliciesTypes, policyId: string, ruleDirection: NetworkPolicyGridTabs): string {
    return (
      `${NetworkPolicyApiService.networkPolicyBaseApi}/${NetworkPolicyApiService.networkTypes[policyType]}/` +
      `${policyId}/${NetworkPolicyApiService.ruleDirectionRoute[ruleDirection]}`
    );
  }
}
