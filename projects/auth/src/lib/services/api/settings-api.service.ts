import { Injectable } from '@angular/core';

import { NetworkPoliciesTypes } from '@network-policy/models/network-policies-types.enum';
import { ApiService } from '@services/api/api.service';
import { Observable } from 'rxjs';
import {
  CEDA_SERVICE_PATH,
  CEDA_SERVICE_VERSION
} from '@services/api/micro-services-paths';
import { HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class SettingsApiService {
  private static networkTypesToInactivePeriodRoute = {
    [NetworkPoliciesTypes.AZURE_FIREWALL]: 'INACTIVE_RULE_PERIOD_HOURS_AZURE',
    [NetworkPoliciesTypes.AZURE_NSG]: 'INACTIVE_RULE_PERIOD_HOURS_AZURE',
    [NetworkPoliciesTypes.AWS_SGS]: 'INACTIVE_RULE_PERIOD_HOURS_AWS'
  };

  private static readonly settingsBaseApi = `${CEDA_SERVICE_PATH}${CEDA_SERVICE_VERSION}/settings`;

  constructor(private apiService: ApiService) {}

  public getUnusedThresholdDays(policyType: NetworkPoliciesTypes): Observable<string> {
    return this.apiService.get<string>(
      `${SettingsApiService.settingsBaseApi}/generic/${SettingsApiService.networkTypesToInactivePeriodRoute[policyType]}`
    );
  }

  public setUnusedThresholdDays(policyType: NetworkPoliciesTypes, hours: string): Observable<void> {
    return this.apiService.post(
      `${SettingsApiService.settingsBaseApi}/generic/${SettingsApiService.networkTypesToInactivePeriodRoute[policyType]}`,
      {
        value: hours
      }
    );
  }
}
