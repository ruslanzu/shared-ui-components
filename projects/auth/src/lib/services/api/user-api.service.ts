import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HEADERS_BASE_CONFIG, SKIP_TOKEN_INTERCEPTOR, TENANT_ID_KEY } from '@models/constants/constants';
import { UserPoolResponse } from '@models/login/cognito/user-pool-response';
import { ClientUserForm } from '@models/user/clientUserForm';
import { AlgoSaaSApiService, ApiService } from '@services/api/api.service';
import {
  /*USERS_ADMIN_SERVICE_PATH,*/ ADMIN_SERVICE_PATH, ADMIN_SERVICE_VERSION,
  /*USERS_ADMIN_SERVICE_VERSION,*/ AUTH_SERVICE_PATH, TENANT_SERVICE_PATH
} from '@services/api/micro-services-paths';
import { User } from '@shared-DTOs/administration-client/administration-user-client.types';
import { Observable } from 'rxjs';
import { StorageService } from '@services/storage.service';
import { QrCodeResponse } from '@models/login/mfa/qr-code-response';

@Injectable({ providedIn: 'root' })
export class UserApiService {
  private static readonly usersBaseApi: string = `${ADMIN_SERVICE_PATH}${ADMIN_SERVICE_VERSION}`;
  // private static readonly usersBaseApi: string = `${USERS_ADMIN_SERVICE_PATH}`;

  constructor(private algoSaaSApiService: AlgoSaaSApiService, private apiService: ApiService, private storageService: StorageService) {}

  public getUserPollParams(tenantId: string): Observable<UserPoolResponse> {
    const headersConfig = { ...HEADERS_BASE_CONFIG };
    headersConfig[SKIP_TOKEN_INTERCEPTOR] = SKIP_TOKEN_INTERCEPTOR;

    return this.algoSaaSApiService.get<UserPoolResponse>(
      `${AUTH_SERVICE_PATH}/idp/${tenantId}`,
      null,
      new HttpHeaders(headersConfig)
    );
  }

  public validateUserSession(): Observable<void> {
    return this.algoSaaSApiService.get<void>(`${TENANT_SERVICE_PATH}/${this.storageService.getItem(TENANT_ID_KEY)}/eula/`);
  }

  public signEula(): Observable<void> {
    return this.algoSaaSApiService.put<void>(`${TENANT_SERVICE_PATH}/${this.storageService.getItem(TENANT_ID_KEY)}/eula/`, {});
  }

  public getUsers(): Observable<ClientUserForm[]> {
    return this.apiService.get<ClientUserForm[]>(`${UserApiService.usersBaseApi}/user`);
    // return this.algoSaaSApiService.get<ClientUserForm[]>(`${UserApiService.usersBaseApi}/user`);
  }

  public createUser(user: User): Observable<void> {
    return this.apiService.post<void>(`${UserApiService.usersBaseApi}/user`, { user });
    // return this.algoSaaSApiService.post<void>(`${UserApiService.usersBaseApi}/user`, { user });
  }

  public updateUser(user: User): Observable<void> {
    return this.apiService.patch<void>(`${UserApiService.usersBaseApi}/user`, { user });
    // return this.algoSaaSApiService.patch<void>(`${UserApiService.usersBaseApi}/user`, { user });
  }

  public deleteUser(usernameToDelete: string): Observable<void> {
    return this.apiService.delete<void>(`${UserApiService.usersBaseApi}/user/${encodeURIComponent(usernameToDelete)}`);
    // return this.algoSaaSApiService.delete<void>(`${UserApiService.usersBaseApi}/user/${encodeURIComponent(usernameToDelete)}`);
  }

  public resendEmailInvitation(username: string): Observable<void> {
    return this.apiService.post<void>(`${UserApiService.usersBaseApi}/user/email-invite`, { username });
    // return this.algoSaaSApiService.post<void>(`${UserApiService.usersBaseApi}/user/email-invite`, { username });
  }

  public getQRCode(accessToken: string): Observable<QrCodeResponse> {
    return this.apiService.post<{qrUrl: string; secretCode: string}>(`${UserApiService.usersBaseApi}/user/mfa/qrcode`, {accessToken});
  }

  public validateMfa(accessToken: string, userCode: string): Observable<void> {
    return this.apiService.post<void>(`${UserApiService.usersBaseApi}/user/mfa/validate`, {accessToken, userCode});
  }

  public resetMfa(username: string): Observable<void> {
    return this.apiService.delete<void>(`${UserApiService.usersBaseApi}/user/${encodeURIComponent(username)}/mfa`);
  }
}
