import { Injectable } from '@angular/core';

import { ApiService } from '@services/api/api.service';
import { Observable } from 'rxjs';
import {
  ASMS_SERVICE_PATH,
  ASMS_SERVICE_VERSION
} from '@services/api/micro-services-paths';

@Injectable({ providedIn: 'root' })
export class AsmsApiService {
  private static readonly asmsBaseApi = `${ASMS_SERVICE_PATH}${ASMS_SERVICE_VERSION}/asms`;

  constructor(private apiService: ApiService) {}

  public getLastConnectionTime(): Observable<number> {
    return this.apiService.get<number>(
      `${AsmsApiService.asmsBaseApi}/last-connection`
    );
  }

  public getDownloadCertificate(): Observable<Blob> {
    return this.apiService.download(
      `${AsmsApiService.asmsBaseApi}/download-certificate`
    );
  }
}
