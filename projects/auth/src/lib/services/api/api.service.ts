import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class ApiService {
  constructor(private http: HttpClient) {}

  public get<T>(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.get<T>(`${environment.api_url}${path}`, { params, headers });
  }

  public download(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<Blob> {
    return this.http.get<Blob>(`${environment.api_url}${path}`, { params, headers, responseType: 'blob' as 'json'});
  }

  public put<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.put<T>(`${environment.api_url}${path}`, body);
  }

  public patch<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.patch<T>(`${environment.api_url}${path}`, body);
  }

  public post<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.post<T>(`${environment.api_url}${path}`, body);
  }

  public delete<T>(path, headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.delete<T>(`${environment.api_url}${path}`, {headers});
  }
}

@Injectable({ providedIn: 'root' })
export class AlgoSaaSApiService {
  constructor(private http: HttpClient) {}

  public get<T>(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.get<T>(`${environment.algosaas_api_url}${path}`, { params, headers });
  }

  public download(path: string, params: HttpParams = new HttpParams(), headers: HttpHeaders = new HttpHeaders()): Observable<Blob> {
    return this.http.get<Blob>(`${environment.algosaas_api_url}${path}`, { params, headers, responseType: 'blob' as 'json'});
  }

  public put<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.put<T>(`${environment.algosaas_api_url}${path}`, body);
  }

  public patch<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.patch<T>(`${environment.algosaas_api_url}${path}`, body);
  }

  public post<T>(path: string, body: Object = {}): Observable<T> {
    return this.http.post<T>(`${environment.algosaas_api_url}${path}`, body);
  }

  public delete<T>(path, headers: HttpHeaders = new HttpHeaders()): Observable<T> {
    return this.http.delete<T>(`${environment.algosaas_api_url}${path}`, {headers});
  }
}
