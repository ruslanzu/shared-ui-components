import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { HttpParams } from '@angular/common/http';
import { InventoryTreeNode, InventoryAssetData, InventoryAssetCategory } from '@shared-DTOs/ceda-client/ceda-inventory-client.types';
import { PaginationRequest, PaginationResult } from '@shared-DTOs/-micro-service.types';
import { CEDA_SERVICE_PATH, CEDA_SERVICE_VERSION } from '@services/api/micro-services-paths';
import { JsonObject } from '@common-ts/utils/json.types';

@Injectable({
  providedIn: 'root'
})
export class InventoryApiService {
  private static readonly resource = `${CEDA_SERVICE_PATH}${CEDA_SERVICE_VERSION}/inventory`;

  private static readonly treeResource = `${InventoryApiService.resource}/root/tree`;

  private static readonly dunoResource = `${CEDA_SERVICE_PATH}${CEDA_SERVICE_VERSION}/duno`;
  private static readonly nativeResource = `${CEDA_SERVICE_PATH}${CEDA_SERVICE_VERSION}/native`;

  constructor(private apiService: ApiService) {}

  public getInventoryTree(): Observable<InventoryTreeNode[]> {
    return this.apiService.get<InventoryTreeNode[]>(InventoryApiService.treeResource);
  }

  public getDashboardData(id: string): Observable<any> {
    return this.apiService.get<any>(`${InventoryApiService.resource}/${id}/stats`);
  }

  public getAssets(
    inventoryAssetCategory: InventoryAssetCategory,
    id: string,
    startRow: number,
    endRow: number,
    sortModel: any
  ): Observable<PaginationResult<InventoryAssetData>> {
    const pageSize = endRow - startRow;
    const paginationRequest: PaginationRequest<any> = {
      pageSize,
      pageIndex: endRow / pageSize - 1,
      sort: sortModel
    };

    const params = new HttpParams()
      .set('category', inventoryAssetCategory)
      .set('pageSize', paginationRequest.pageSize.toString())
      .set('pageIndex', paginationRequest.pageIndex.toString())
      .set('sort', JSON.stringify(paginationRequest.sort));
    return this.apiService.get<any>(`${InventoryApiService.resource}/${id}/assets`, params);
  }

  public getDunoByCfid(cfId: string): Observable<JsonObject> {
    return this.apiService.get<JsonObject>(`${InventoryApiService.dunoResource}/${cfId}`);
  }

  public getNativeObjectByCfid(cfId: string): Observable<JsonObject> {
    return this.apiService.get<JsonObject>(`${InventoryApiService.nativeResource}/${cfId}`);
  }
}
