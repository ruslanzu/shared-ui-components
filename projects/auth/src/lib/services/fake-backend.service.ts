/*
import { InMemoryDbService, RequestInfo, ResponseOptions } from 'angular-in-memory-web-api';
import { getStatusText, STATUS } from 'angular-in-memory-web-api/http-status-codes';
import { Observable } from 'rxjs';
export class FakeBackendService implements InMemoryDbService {

  private accounts = [
    {
      accountName: 'Azure account 1', accountType: 'AzureAccount', lastCollectionTime: new Date().toTimeString(),
      status: 'Active', accountId: 1
    },
    {
      accountName: 'Azure account 2', accountType: 'AzureAccount', lastCollectionTime: new Date().toTimeString(),
      status: 'Active', accountId: 2
    },
    {
      accountName: 'Azure account 3', accountType: 'AzureAccount', lastCollectionTime: new Date().toTimeString(),
      status: 'Active', accountId: 3
    },
    {
      accountName: 'Azure account 4', accountType: 'AzureAccount', lastCollectionTime: new Date().toTimeString(),
      status: 'Active', accountId: 4
    },
  ];
  public createDb() {
    return {};
  }

  // intercept ResponseOptions from default HTTP method handlers
  // add a response header and report interception to console.log
  public responseInterceptor(resOptions: ResponseOptions, reqInfo: RequestInfo) {

    resOptions.headers = resOptions.headers.set('x-test', 'test-header');
    const method = reqInfo.method.toUpperCase();
    const body = JSON.stringify(resOptions);
    console.log(`responseInterceptor: ${method} ${reqInfo.req.url}: \n${body}`);

    return resOptions;
  }

  public get(reqInfo: RequestInfo): Observable<any> {

    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP get override');

      const options: ResponseOptions = {
        body: this.accounts,
        status: STATUS.OK
      };
      return this.finishOptions(options, reqInfo);
    });
  }

  public patch(reqInfo: RequestInfo): Observable<any> {

    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP patch override');

      const id = reqInfo.id;
      const account = reqInfo.utils.getJsonBody(reqInfo.req);
      const accountName = account.accountName;

      const accountNameExist = this.accounts.map(acc => (
        acc.accountName
      )).includes(accountName);

      if (!accountNameExist) {
        this.accounts.find(acc => (
          acc.accountId == id
        )).accountName = accountName;
      }


      const options: ResponseOptions = accountNameExist ?

        {
          body: { error: 'account Name Exist' },
          status: STATUS.CONFLICT
        }
        :
        {
          status: STATUS.OK
        };
      return this.finishOptions(options, reqInfo);
    });
  }

  public delete(reqInfo: RequestInfo): Observable<any> {

    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP delete override');

      this.accounts = this.accounts.filter(a => (
        a.accountId === reqInfo.id
      ));
      const options: ResponseOptions = {
        status: STATUS.OK
      };
      return this.finishOptions(options, reqInfo);
    });
  }

  public post(reqInfo: RequestInfo): Observable<any> {

    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP post override');

      const id = this.genId();
      const account = reqInfo.utils.getJsonBody(reqInfo.req);
      const accountName = account.accountName;

      const accountNameExist = this.accounts.map(acc => (
        acc.accountName
      )).includes(accountName);

      if (!accountNameExist) {
        account.id = id;
        account.status = 'Active';
        this.accounts.push(account);
      }

      const options: ResponseOptions = accountNameExist ?
        {
          body: { error: 'account Name Exist' },
          status: STATUS.CONFLICT
        }
        :
        {
          status: STATUS.OK
        };
      return this.finishOptions(options, reqInfo);
    });
  }

  public genId(): number {
    return this.accounts.length > 0 ? Math.max(...this.accounts.map(acc => acc.accountId)) + 1 : 11;
  }

  private finishOptions(options: ResponseOptions, { headers, url }: RequestInfo) {
    options.statusText = getStatusText(options.status);
    options.headers = headers;
    options.url = url;
    return options;
  }
}
*/
