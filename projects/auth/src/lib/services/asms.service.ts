import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DialogService } from '@shared/services/dialog.service';
import { AsmsApiService } from '@services/api/asms-api.service';
import { NetworkPoliciesTypes } from '@network-policy/models/network-policies-types.enum';

@Injectable({ providedIn: 'root' })
export class AsmsService {

  constructor(
    private asmsApiService: AsmsApiService,
    private dialogService: DialogService
  ) { }

  public getLastConnectionTime(): Observable<Date> {
    return this.asmsApiService.getLastConnectionTime().pipe(
      map(
        lastConnectionTimeStamp => {
          return lastConnectionTimeStamp['lastUpdate'] ? new Date(lastConnectionTimeStamp['lastUpdate']) : null;
        },
        errors => {
          this.handleError(errors);
        }
      )
    );
  }

  public getDownloadCertificate(): Observable<any> {
    return this.asmsApiService.getDownloadCertificate();
  }

  private handleError(errors: any): void {
    console.log(errors);
    this.dialogService.openErrorModal('Operation has failed', errors.message);
  }
}
