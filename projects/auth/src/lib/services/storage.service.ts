import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class StorageService {
  private readonly appPrefix: string = 'ash-';
  private storage: Storage;

  constructor() {
    this.storage = localStorage;
  }

  public getItem(key: string) {
    return this.storage.getItem(`${this.appPrefix}${key}`);
  }

  public setItem(key: string, value: string) {
    this.storage.setItem(`${this.appPrefix}${key}`, value);
  }

  public setObjectToStorageData(object: object) {
    Object.keys(object).forEach(key => this.setItem(key, object[key]));
  }

  public removeItem(key: string) {
    this.storage.removeItem(`${this.appPrefix}${key}`);
  }

  public clearStorage(): void {
    this.storage.clear();
  }
}
