import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DialogService } from '@shared/services/dialog.service';
import { SettingsApiService } from '@services/api/settings-api.service';
import { NetworkPoliciesTypes } from '@network-policy/models/network-policies-types.enum';

@Injectable({ providedIn: 'root' })
export class SettingsService {

  constructor(
    private settingsApiService: SettingsApiService,
    private dialogService: DialogService
  ) { }

  public setUnusedThresholdDays(policyType: NetworkPoliciesTypes, days: number): Observable<void> {
    return this.settingsApiService.setUnusedThresholdDays(policyType, (days * 24).toString());
  }

  public getUnusedThresholdDays(policyType: NetworkPoliciesTypes): Observable<number> {
    return this.settingsApiService.getUnusedThresholdDays(policyType).pipe(
      map(
        thresholdInHours => {
          return Number(thresholdInHours) / 24;
        },
        errors => {
          this.handleError(errors);
        }
      )
    );
  }

  private handleError(errors: any): void {
    console.log(errors);
    this.dialogService.openErrorModal('Operation has failed', errors.message);
  }
}
