import { Injectable } from '@angular/core';
// import Amplify, { Auth } from 'aws-amplify';
import { UserPool, userPoolKeys } from '../models/user-pool';
import { AwsAmplifyAuthConfig } from '../models/aws-amplify-auth-config';
import { StorageService } from './storage.service';
import { UserCredential } from '../models/user-credential';

@Injectable({ providedIn: 'root' })
export class CognitoService {
  // public initialized = false;
  // public temporaryUser: any;

  // private static readonly defaultUserAttribute = { profile: 'admin' };

  // constructor(private storageService: StorageService) {
  //   this.initCognitoParams(this.getUserPollParamsFromStorage());
  // }

  // public login(cognitoUserPollParams: UserPool, userCredential: UserCredential): Promise<any> {
  //   this.storeUserPollParams(cognitoUserPollParams);
  //   this.initCognitoParams(cognitoUserPollParams);
  //   return Auth.signIn(userCredential.username, userCredential.password);
  // }

  // public forgotPassword(cognitoUserPollParams: UserPool, username: string): Promise<any> {
  //   this.storeUserPollParams(cognitoUserPollParams);
  //   this.initCognitoParams(cognitoUserPollParams);
  //   return Auth.forgotPassword(username);
  // }

  // public submitForgotPassword(username: string, password: string, code: string): Promise<void> {
  //   return Auth.forgotPasswordSubmit(username, code, password);
  // }

  // public fistLoginCompleteSignUp(email: string, password: string, emailVerified?: boolean): Promise<void> {
  //   return emailVerified ? this.completeSignUp(password) : this.completeSignUpWithEmailVerification(password, email);
  // }

  // public verifyEmailByCode(verificationCode: string): Promise<void> {
  //   return Auth.verifyUserAttributeSubmit(this.temporaryUser, 'email', verificationCode).then(() => (this.temporaryUser = null));
  // }

  // public confirmSignInWithMfa(userCode: string): Promise<void> {
  //   return Auth.confirmSignIn(this.temporaryUser, userCode, 'SOFTWARE_TOKEN_MFA').then(() => (this.temporaryUser = null));
  // }

  // public logout(): Promise<void> {
  //   this.initialized = false;
  //   this.removeUserPollParamsFromStorage();
  //   return Auth.signOut();
  // }

  // private initCognitoParams(cognitoParams: UserPool): void {
  //   if (CognitoService.isUserPoolParamsExists(cognitoParams)) {
  //     const awsAmplifyAuthConfig: AwsAmplifyAuthConfig = CognitoService.getAmplifyConfig(cognitoParams);
  //     Amplify.configure({ Auth: awsAmplifyAuthConfig });
  //     Auth.configure({ Auth: awsAmplifyAuthConfig });
  //     this.initialized = true;
  //   }
  // }

  // private static getAmplifyConfig(cognitoParams: UserPool): AwsAmplifyAuthConfig {
  //   return {
  //     region: cognitoParams.region,
  //     userPoolId: cognitoParams.userPoolId,
  //     userPoolWebClientId: cognitoParams.clientId
  //   };
  // }

  // private storeUserPollParams(userPoolParams: UserPool): void {
  //   this.storageService.setObjectToStorageData(userPoolParams);
  // }

  // private getUserPollParamsFromStorage(): UserPool {
  //   const userPoolParams = {} as UserPool;
  //   Object.keys(userPoolKeys).forEach(key => {
  //     userPoolParams[key] = this.storageService.getItem(key);
  //   });
  //   return userPoolParams;
  // }

  // private removeUserPollParamsFromStorage(): void {
  //   Object.keys(userPoolKeys).forEach(key => {
  //     this.storageService.removeItem(key);
  //   });
  // }

  // private static isUserPoolParamsExists(userPool: UserPool): boolean {
  //   return !!(
  //     userPool.clientId &&
  //     userPool.region &&
  //     userPool.userPoolId &&
  //     userPool.cognitoUserPoolUrl &&
  //     userPool.redirectSignOut &&
  //     userPool.redirectSignIn
  //   );
  // }

  // private completeSignUpWithEmailVerification(password: string, email: string) {
  //   return Auth.completeNewPassword(this.temporaryUser, password, { ...CognitoService.defaultUserAttribute, email }).then(() => {
  //     return Auth.verifyUserAttribute(this.temporaryUser, 'email');
  //   });
  // }

  // private completeSignUp(password: string) {
  //   return Auth.completeNewPassword(this.temporaryUser, password, { ...CognitoService.defaultUserAttribute });
  // }
}
