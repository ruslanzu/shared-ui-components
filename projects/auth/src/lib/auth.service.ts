import { Injectable } from '@angular/core';
import { Auth } from 'aws-amplify';
import { of } from 'rxjs';
import { AwsAmplifyAuthConfig } from './models/aws-amplify-auth-config';
import { UserPool, userPoolKeys } from './models/user-pool';
import { StorageService } from './services/storage.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private readonly idTokenPrefix: string = 'id_token=';
  private readonly refreshTokenPrefix: string = '&refresh_token=';

  constructor(private storageService: StorageService) {
    Auth.configure(AuthService.getAmplifyConfig(this.getUserPollParamsFromStorage()));
  }

  public async getToken(): Promise<string> {
    const session = await Auth.currentSession();
    return session.getIdToken().getJwtToken();
  }

  public async logout(): Promise<void> {
    try {
      return Auth.signOut();
    } catch (error) {
      return Promise.reject();
    }
  }

  public async getCurrentSession(): Promise<any>{
    try {
      return await Auth.currentSession();
    } catch (error) {
      console.log('No current session');
      return null;
    }
  }

  public async isValidSession(): Promise<boolean> {
    // Original below doesnt work. Using getCurrentSession insted
    // const session = await Auth.userSession(Auth.currentAuthenticatedUser());
    const session = await this.getCurrentSession();
    if (!session) {
      return of (false).toPromise();
    }
    return session.isValid();
  }

  private getUserPollParamsFromStorage(): UserPool {
    const userPoolParams = {} as UserPool;
    Object.keys(userPoolKeys).forEach(key => {
      userPoolParams[key] = this.storageService.getItem(key);
    });
    return userPoolParams;
  }

  private static getAmplifyConfig(cognitoParams: UserPool): AwsAmplifyAuthConfig {
    return {
      region: cognitoParams.region,
      userPoolId: cognitoParams.userPoolId,
      userPoolWebClientId: cognitoParams.clientId
    };
  }

  // private initDefaults() {
  //   this.amplifyService
  //     .auth()
  //     .currentAuthenticatedUser()
  //     .then(user => {
  //       this.updateAuthData(user.signInUserSession.isValid(), AuthService.getAuthenticatedUser(user));
  //     })
  //     .catch(() => {
  //       this.updateAuthData(false, null);
  //     });
  // }

  // private updateAuthData(isAuthenticated: boolean, authenticatedUser: UserParams): void {
  //   this.isUserLoggedIn = isAuthenticated;
  //   this.userDetails = authenticatedUser;
  // }

  // private static isSignInState(authState: string): boolean {
  //   return authState === CognitoAuthState.signedIn || authState === CognitoAuthState.signIn;
  // }

  // private static getAuthenticatedUser(user): UserParams {
  //   return {
  //     username: user.username,
  //     email: AuthService.getUserEmail(user),
  //     emailVerified: AuthService.isUserEmailVerified(user),
  //     mfaEnabled: AuthService.getUserCustomMfaEnabled(user),
  //     // This field represents the current user's MFA configuration in cognito 'NOMFA' | 'SOFTWARE_TOKEN_MFA'
  //     preferredMfa: user.preferredMFA,
  //   };
  // }

  // private static isUserEmailVerified(user): boolean {
  //   const emailVerified: string | boolean =
  //     this.safeValue(() => user.attributes.email_verified, undefined) ||
  //     this.safeValue(() => user.challengeParam.userAttributes.email_verified, undefined);
  //   return emailVerified === 'true' || emailVerified === true;
  // }

  // private static getUserEmail(user): string {
  //   return (
  //     this.safeValue(() => user.attributes.email, undefined) ||
  //     this.safeValue(() => user.challengeParam.userAttributes.email, undefined)
  //   );
  // }

  // private static getUserCustomMfaEnabled(user): boolean {
  //   return this.safeValue(() => user.attributes['custom:mfa_enable'], false);
  // }

  // private subscribeAuthState() {
  //   this.amplifyService.authStateChange$.subscribe((authState: AuthState) => {
  //     this.updateAuthData(AuthService.isSignInState(authState.state), AuthService.getAuthenticatedUser(authState.user || {}));
  //   });
  // }

  // private static safeValue(evaluateFn: () => any, defaultVal?: any): any {
  //   try {
  //     return evaluateFn();
  //   } catch (e) {
  //     return defaultVal;
  //   }
  // }

}
