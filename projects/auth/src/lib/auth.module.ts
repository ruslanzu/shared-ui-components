import { NgModule } from '@angular/core';
import { AuthService } from './auth.service';
import { SessionService } from './services/session.service';

@NgModule({
  providers: [AuthService, SessionService],
  declarations: [
  ],
  imports: [
  ],
  exports: [
  ]
})
export class AuthModule { }