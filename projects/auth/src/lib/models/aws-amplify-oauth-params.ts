export interface AwsAmplifyOauthParams {
  domain: string;
  responseType: string;
  scope: string[];
  redirectSignIn: string;
  redirectSignOut: string;
  options: any;
}
