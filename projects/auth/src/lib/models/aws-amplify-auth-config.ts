import { AwsAmplifyOauthParams } from './aws-amplify-oauth-params';

export interface AwsAmplifyAuthConfig {
  userPoolId: string;
  userPoolWebClientId: string;
  region: string;
  oauth?: AwsAmplifyOauthParams;
}
