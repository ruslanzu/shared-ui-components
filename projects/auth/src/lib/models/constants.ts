/****************************
 * General constants
 ****************************/
export const AUTHORIZATION_HEADER_KEY: string = 'Authorization';
export const TOKEN_PREFIX: string = 'Bearer ';
export const TENANT_ID_KEY: string = 'x-cloudflow-tenant-id';
export const USERNAME_KEY: string = 'username';
export const USER_ACTIVITY_KEY: string = 'userActivity';
export const LAST_VIEWED_BRAND_KEY: string = 'lastViewedBrand';
export const HOUR_IN_MILLI_SEC: number = 1000 * 60 * 60;
export const DAY_IN_MILLI_SEC: number = HOUR_IN_MILLI_SEC * 24;
export const PASSWORD_MIN_LENGTH: number = 8;
export const SESSION_TIMEOUT_MILLI_SEC: number = HOUR_IN_MILLI_SEC;
export const SKIP_TOKEN_INTERCEPTOR: string = 'skip_token_interceptor';
export const HEADERS_BASE_CONFIG = Object.freeze({
  'Content-Type': 'application/json',
  Accept: 'application/json'
});

export const EULA_STATUS_ERROR: number = 4030335;
export const CLOUDFLOW_HELP_CENTER_BASE_LINK: string = 'https://docs.algosec.com/en/cloudflow/content/cloudflow';
export const CLOUDFLOW_HELP_CENTER_LINK: string = 'https://docs.algosec.com/en/cloudflow/index.htm';
export const CLOUDFLOW_EULA_LINK: string = '/assets/files/eula-08-2019.pdf';

export const GRID_HIGHLIGHTED_ROW_CLASS: string = 'highlighted-row';

export const ASMS_INTEGRATION_HELP_CENTER_LINK: string = `${CLOUDFLOW_HELP_CENTER_BASE_LINK}/asms-integration.htm`;
export const ASMS_CONNECTIVITY_HELP_CENTER_LINK: string = `${CLOUDFLOW_HELP_CENTER_BASE_LINK}/connect-asms.htm`;

export const ADD_ACCOUNT_HELP_CENTER_LINK_AWS: string = `${CLOUDFLOW_HELP_CENTER_BASE_LINK}/add-aws.htm`;
export const ADD_ACCOUNT_HELP_CENTER_LINK_AZURE: string = `${CLOUDFLOW_HELP_CENTER_BASE_LINK}/add-azure.htm`;
export const LAST_USED_HELP_CENTER_LINK: string = `${CLOUDFLOW_HELP_CENTER_BASE_LINK}/clean-policies.htm`;
export const MFA_SETUP_HELP_CENTER_LINK: string = `${CLOUDFLOW_HELP_CENTER_BASE_LINK}/manage-access.htm`;
export const GCP_SETUP_HELP_CENTER_LINK: string = `${CLOUDFLOW_HELP_CENTER_BASE_LINK}/add-gcp.htm`;

export const LAST_USED_HEADER_TOOLTIP: string =
  'When flow logging is enabled, indicates last used data for each rule.\nFor details, see the CloudFlow Help Center.';
export const CLOUDFLOW_MOTTO: string = 'Visibility. Risk Remediation. Policy Management.';
export const LOGIN_ERROR_MESSAGE: string = 'Incorrect credentials. Check your details and try again.';
export const VERIFICATION_CODE_ERROR_MESSAGE: string = 'Invalid verification code provided, please try again.';
export const MFA_VALIDATE_CODE_ERROR_MESSAGE: string = 'The authentication code provided is not valid';

/****************************
 * * Internal routing paths
 ****************************/

// HOME ROUTES
export const HOME_ROUTE: string = '';

export const HOME_TITLE: string = 'Home';
export const SETTINGS_TITLE: string = 'Settings';
export const ADD_NEW_ACCOUNT_PAGE_TITLE: string = 'Add New Account';
export const ADD_NEW_AZURE_ACCOUNT_PAGE_TITLE: string = 'Add New Azure Subscription';
export const ADD_NEW_GCP_PROJECT_PAGE_TITLE: string = 'Add New GCP Project';
export const ADD_NEW_AWS_ACCOUNT_PAGE_TITLE: string = 'Add New AWS Account';
export const INVENTORY_DEFAULT_PAGE_TITLE: string = 'All Inventory';
export const RISK_DEFAULT_PAGE_TITLE: string = 'Risk Triggers';
