export enum CognitoAuthState {
  signIn = 'signIn',
  signedIn = 'signedIn',
  cognitoHostedUI = 'cognitoHostedUI',
  requireNewPassword = 'requireNewPassword',
  mfaRequired = 'mfaRequired',
  signIn_failure = 'signIn_failure',
  cognitoHostedUI_failure = 'cognitoHostedUI_failure',
  signedOut = 'signedOut'
}
