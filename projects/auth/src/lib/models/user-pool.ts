export interface UserPool {
  region: string; // us-east-1
  userPoolId: string; // us-east-1_zFzBVI7Ru
  clientId: string; // 6npjb8rp2nro03bnmr955smdvs
  redirectSignIn: string; // https://cloudflow.algosec.com/login/authenticated
  redirectSignOut: string; // https://cloudflow.algosec.com/login
  cognitoUserPoolUrl: string; // https://dortest.auth.us-east-1.amazoncognito.com
}

export const userPoolKeys: UserPool = {
  region: 'region',
  userPoolId: 'userPoolId',
  clientId: 'clientId',
  redirectSignIn: 'redirectSignIn',
  redirectSignOut: 'redirectSignOut',
  cognitoUserPoolUrl: 'cognitoUserPoolUrl'
};
