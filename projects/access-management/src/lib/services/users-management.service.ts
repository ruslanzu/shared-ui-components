import { Injectable } from '@angular/core';
import { ClientUserForm } from '../models/user/clientUserForm';
import { Store } from '@ngrx/store';
import { UserApiService } from './api/user-api.service';
import { AccessManagementServiceModule } from '../access-management-service.module';
import { CreateOrEditUserComponent } from '../users-management/create-or-edit-user/create-or-edit-user.component';
import { UserStatus } from '../models/administration-client/administration-user-client.types';
import { DialogService, actionTypes, ConfirmationModalComponent } from '@algosec/base-components';
import { AccessManagementRootState } from '../store/access-management-root-state';
import { LoadUsersAction } from '../store/users-management-store/users-management.actions';
import { filter, take } from 'rxjs/operators';
import { ADMIN_ROLE_ID } from '../models/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class UsersManagementService {
  constructor(
    private dialogService: DialogService,
    private userApiService: UserApiService,
    private store: Store<AccessManagementRootState>
  ) {}

  public openCreateUserModal(): void {
    const createOrEditUserComponent: CreateOrEditUserComponent = this.getCreateOrEditUserModal();
    createOrEditUserComponent.submitForm$.pipe(filter(formData => !!formData)).subscribe(formData => {
      createOrEditUserComponent.isLoading = true;
      this.userApiService
        .createUser(formData)
        .pipe(take(1))
        .subscribe(() => this.onSubmitFormSuccess(), error => this.onSubmitFormFail(createOrEditUserComponent, error));
    });
  }

  public openEditUserModal(userForm: ClientUserForm): void {
    const createOrEditUserComponent: CreateOrEditUserComponent = this.getCreateOrEditUserModal(userForm);
    createOrEditUserComponent.submitForm$.pipe(filter(formData => !!formData)).subscribe(formData => {
      createOrEditUserComponent.isLoading = true;
      this.userApiService
        .updateUser(formData)
        .pipe(take(1))
        .subscribe(() => this.onSubmitFormSuccess(), error => this.onSubmitFormFail(createOrEditUserComponent, error));
    });
  }

  public openDeleteUserModal(userToDelete: ClientUserForm): void {
    const modalRef = this.dialogService.openDialog(ConfirmationModalComponent, {
      centered: true
    });
    const deleteUserConfirmationModalComponent: ConfirmationModalComponent = modalRef.componentInstance;
    deleteUserConfirmationModalComponent.title = 'Delete user';
    deleteUserConfirmationModalComponent.body = `Are you sure you want to delete the following user?<br>
    Username: <b>${userToDelete.username}</b>`;
    modalRef.result.then(result => {
      if (result === actionTypes.YES) {
        this.userApiService
          .deleteUser(userToDelete.username)
          .pipe(take(1))
          .subscribe(
            () => this.onSubmitFormSuccess(),
            error => {
              this.dialogService.openErrorModal('', UsersManagementService.safeValue(() => error.message, 'Operation has failed'));
            }
          );
      }
    });
  }

  public openResendEmailInvitationModal(user: ClientUserForm): void {
    const modalRef = this.dialogService.openDialog(ConfirmationModalComponent, {
      centered: true
    });
    const deleteUserConfirmationModalComponent: ConfirmationModalComponent = modalRef.componentInstance;
    deleteUserConfirmationModalComponent.title = 'Resend invitation email';
    deleteUserConfirmationModalComponent.body = `CloudFlow will resend an invitation email to the following<br>
     user details:<br><br>
     Username: <b>${user.username}</b><br>
     Email: <b>${user.email}</b>`;
    deleteUserConfirmationModalComponent.approveButtonText = 'OK';
    deleteUserConfirmationModalComponent.declineButtonText = 'Cancel';
    modalRef.result.then(result => {
      if (result === actionTypes.YES) {
        this.userApiService
          .resendEmailInvitation(user.username)
          .pipe(take(1))
          .subscribe(
            () => this.dialogService.closeDialog(),
            error => {
              this.dialogService.openErrorModal('', UsersManagementService.safeValue(() => error.message, 'Operation has failed'));
            }
          );
      }
    });
  }

  public openResetMfaModal(user: ClientUserForm): void {
    const modalRef = this.dialogService.openDialog(ConfirmationModalComponent, {
      centered: true
    });
    const resetMfaConfirmationModalComponent: ConfirmationModalComponent = modalRef.componentInstance;
    resetMfaConfirmationModalComponent.title = 'Reset MFA device';
    resetMfaConfirmationModalComponent.body = `Are you sure you want to reset MFA device for the following user?<br>
    Username: <b>${user.username}</b>`;
    modalRef.result.then(result => {
      if (result === actionTypes.YES) {
        this.userApiService
          .resetMfa(user.username)
          .pipe(take(1))
          .subscribe(
            () => this.onSubmitFormSuccess(),
            error => {
              this.dialogService.openErrorModal('', UsersManagementService.safeValue(() => error.message, 'Operation has failed'));
            }
          );
      }
    });
  }

  private onSubmitFormFail(userModalComponent: CreateOrEditUserComponent, error) {
    userModalComponent.showErrorMessage = true;
    userModalComponent.errorMessage = UsersManagementService.safeValue(() => error.message, 'Oops...something went wrong');
    userModalComponent.isLoading = false;
  }

  private onSubmitFormSuccess() {
    this.dialogService.closeDialog();
    this.store.dispatch(LoadUsersAction.create());
  }

  private getCreateOrEditUserModal(userForm?: ClientUserForm): CreateOrEditUserComponent {
    const isEditMode: boolean = !!userForm;
    userForm = isEditMode ? userForm : UsersManagementService.getEmptyUserForm();
    const modalRef = this.dialogService.openDialog(CreateOrEditUserComponent, {
      centered: true,
      size: 'lg',
      windowClass: 'create-or-edit-user-modal'
    });
    const createOrEditUserComponent: CreateOrEditUserComponent = modalRef.componentInstance;
    createOrEditUserComponent.isEditMode = isEditMode;
    createOrEditUserComponent.userForm = userForm;
    return createOrEditUserComponent;
  }

  private static getEmptyUserForm(): ClientUserForm {
    return {
      status: UserStatus.PENDING_FOR_INITIAL_LOGIN,
      roles: [ADMIN_ROLE_ID],
      mfaEnable: true,
      phoneNumber: ''
    } as ClientUserForm;
  }

  public static safeValue(evaluateFn: () => any, defaultVal?: any): any {
    try {
      return evaluateFn();
    } catch (e) {
      return defaultVal;
    }
  }
}
