import { Injectable } from '@angular/core';
import { AlgoSaaSApiService } from './api.service';
import {
 AUTHZ_SERVICE_PATH, AUTHZ_SERVICE_VERSION
} from './micro-services-paths';
import { Observable } from 'rxjs';
import {StorageService} from '../storage.service';
import { ClientRole } from '../../models/role/clientRole';
import { CLOUDFLOW_PRODUCT_NAME, TENANT_ID_KEY } from '../../models/constants/constants';

@Injectable({ providedIn: 'root' })
export class RoleApiService {

  private static readonly authzBaseApi: string = `${AUTHZ_SERVICE_PATH}/${AUTHZ_SERVICE_VERSION}`;

  constructor(private algoSaaSApiService: AlgoSaaSApiService, private storageService: StorageService) {}

  public getRoles(): Observable<ClientRole[]> {
    const queryParams = `tenant=${this.storageService.getItem(TENANT_ID_KEY)}&product=${CLOUDFLOW_PRODUCT_NAME}`;
    return this.algoSaaSApiService.get<ClientRole[]>(`${RoleApiService.authzBaseApi}/role?${queryParams}`);
  }
}
