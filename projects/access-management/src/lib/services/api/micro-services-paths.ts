const isLocalMode = false;

export const CLUDFLOW_URL_PREF = ''; //should be in the future - '/cloudflow/CS-XXXX'
export const ALGOSAAS_URL_PREF = 'algosaas';
export const ALGOSAAS_API_VERSION = 'v1';

export const AUTH_SERVICE_PATH: string = `/${ALGOSAAS_URL_PREF}/auth/${ALGOSAAS_API_VERSION}`;
export const TENANT_SERVICE_PATH: string =  `/${ALGOSAAS_URL_PREF}/tenant/${ALGOSAAS_API_VERSION}`;

export const SECURITY_SERVICE_PATH: string = isLocalMode ? '55555/api/security' : CLUDFLOW_URL_PREF + '/security';
export const SECURITY_SERVICE_VERSION: string = '/v1';

export const NETWORK_POLICY_SERVICE_PATH: string = isLocalMode ? '3001/api/policy' : CLUDFLOW_URL_PREF + '/policy';
export const NETWORK_POLICY_SERVICE_VERSION: string = '/v1';

export const CEDA_SERVICE_PATH: string = isLocalMode ? '3000/api/ceda' : CLUDFLOW_URL_PREF + '/ceda';
export const CEDA_SERVICE_VERSION: string = '/v1';

export const ADMIN_SERVICE_PATH: string = isLocalMode ? '3010/api/admin' : CLUDFLOW_URL_PREF + '/admin';
export const ADMIN_SERVICE_VERSION: string = '/v1';

export const USERS_ADMIN_SERVICE_PATH: string = isLocalMode ? '3010/api/admin' : `/${ALGOSAAS_URL_PREF}/users/${ALGOSAAS_API_VERSION}`;
export const USERS_ADMIN_SERVICE_VERSION: string = '/v1';

export const CLOUD_ASSET_COLLECTOR_SERVICE_PATH: string = isLocalMode ? '8080/api/cac' : '/cac';
export const CLOUD_ASSET_COLLECTOR_SERVICE_VERSION: string = '/v1';

export const NOTIFY_SERVICE_PATH: string = isLocalMode ? '3020/api/notify' : '/notify';
export const NOTIFY_SERVICE_VERSION: string = '/v1';

export const RISK_SERVICE_PATH: string = isLocalMode ? '3040/api/risk' : CLUDFLOW_URL_PREF + '/risk';
export const RISK_SERVICE_VERSION: string = '/v1';

export const ASMS_SERVICE_PATH: string = isLocalMode ? '3050/api/asms' : CLUDFLOW_URL_PREF + '/asms';
export const ASMS_SERVICE_VERSION: string = '/v1';

export const AUTHZ_SERVICE_PATH: string = isLocalMode ? `3001/api/${ALGOSAAS_URL_PREF}/authorization` : `/${ALGOSAAS_URL_PREF}/authorization`;
export const AUTHZ_SERVICE_VERSION: string = 'v1';
