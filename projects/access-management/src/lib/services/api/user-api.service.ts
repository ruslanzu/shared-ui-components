import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HEADERS_BASE_CONFIG, SKIP_TOKEN_INTERCEPTOR, TENANT_ID_KEY } from '../../models/constants/constants';
import { ClientUserForm } from '../../models/user/clientUserForm';
import { AlgoSaaSApiService, ApiService } from './api.service';
import {
 AUTH_SERVICE_PATH, TENANT_SERVICE_PATH, USERS_ADMIN_SERVICE_PATH
} from './micro-services-paths';
import { User } from '../../models/administration-client/administration-user-client.types';
import { Observable } from 'rxjs';
import { Environment } from '../../models/environment';

@Injectable({ providedIn: 'root' })
export class UserApiService {
  private static readonly usersBaseApi: string = `${USERS_ADMIN_SERVICE_PATH}`;

  public env: Environment;

  constructor(private algoSaaSApiService: AlgoSaaSApiService) {}

  public getUsers(): Observable<ClientUserForm[]> {
    this.algoSaaSApiService.environment = this.env;
    return this.algoSaaSApiService.get<ClientUserForm[]>(`${UserApiService.usersBaseApi}/user`);
  }

  public createUser(user: User): Observable<void> {
    this.algoSaaSApiService.environment = this.env;
    return this.algoSaaSApiService.post<void>(`${UserApiService.usersBaseApi}/user`, { user });
  }

  public updateUser(user: User): Observable<void> {
    this.algoSaaSApiService.environment = this.env;
    return this.algoSaaSApiService.patch<void>(`${UserApiService.usersBaseApi}/user`, { user });
  }

  public deleteUser(usernameToDelete: string): Observable<void> {
    this.algoSaaSApiService.environment = this.env;
    return this.algoSaaSApiService.delete<void>(`${UserApiService.usersBaseApi}/user/${encodeURIComponent(usernameToDelete)}`);
  }

  public resendEmailInvitation(username: string): Observable<void> {
    this.algoSaaSApiService.environment = this.env;
    return this.algoSaaSApiService.post<void>(`${UserApiService.usersBaseApi}/user/email-invite`, { username });
  }

  public resetMfa(username: string): Observable<void> {
    this.algoSaaSApiService.environment = this.env;
    return this.algoSaaSApiService.delete<void>(`${UserApiService.usersBaseApi}/user/${encodeURIComponent(username)}/mfa`);
  }
}
