import { ReplaySubject } from 'rxjs';

/**
 * This class will use us ti unsubscribe on ngDestroy and takeUntil for the observables.
 * Please read the below
 * https://medium.com/@benlesh/rxjs-dont-unsubscribe-6753ed4fda87
 * https://medium.com/angular-in-depth/rxjs-avoiding-takeuntil-leaks-fb5182d047ef
 *
 * It's better to use ReplaySubject instead of Subject for that to avoid unsubscribe problems
 * https://stackoverflow.com/questions/42490265/rxjs-takeuntil-angular-components-ngondestroy
 * */
export class UnsubscribeSubject extends ReplaySubject<boolean> {
  constructor() {
    super(1);
  }

  public cleanup() {
    this.next(true);
    this.complete();
  }
}
