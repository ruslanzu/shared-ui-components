import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AccessManagementTabs } from '@models/access-management-tabs.enum';
import { AccessManagementRootState } from '@store/settings-store/access-management-store/access-management-root-state';
import { SelectedTabChangeAccessManagementAction } from '@store/settings-store/access-management-store/access-management.actions';

@Component({
  selector: 'ash-roles-table-container',
  templateUrl: './roles-table-container.component.html',
  styleUrls: ['./roles-table-container.component.scss']
})
export class RolesTableContainerComponent {
  constructor(private store: Store<AccessManagementRootState>) {
    this.store.dispatch(SelectedTabChangeAccessManagementAction.create(AccessManagementTabs.ROLES));
  }
}
