import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseComponentsModule, AngularMaterialModule } from '@algosec/base-components';
import { AccessManagementComponent } from './access-management.component';
import { NgSelectModule } from '@ng-select/ng-select';
// import { RolesTableContainerComponent } from './roles-management/roles-table-container/roles-table-container.component';
import { UsersTableContainerComponent } from './users-management/users-table-container/users-table-container.component';
import { AccessManagementStoreModule } from './store/access-management-store.module';
import { CreateOrEditUserComponent } from './users-management/create-or-edit-user/create-or-edit-user.component';

@NgModule({
  declarations: [AccessManagementComponent, UsersTableContainerComponent, CreateOrEditUserComponent],
  imports: [BaseComponentsModule, AccessManagementStoreModule, NgSelectModule, AngularMaterialModule],
  entryComponents: [CreateOrEditUserComponent]
})
export class AccessManagementModule { }
