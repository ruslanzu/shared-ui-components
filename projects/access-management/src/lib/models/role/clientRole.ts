export interface ClientRole {
  id: number;
  name: string;
  description: string;
  tenant: string;
  product: string;
  isOOB: boolean;
  allowedOperations: string[];
}
