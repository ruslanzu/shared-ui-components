import {httpNoBodyJSon, httpWithBodyJSon} from "../-micro-service-clients-utils";
import {administrationClientConfigPath} from "./administration-client";
import {JsonObject} from "../../utils/json.types";
import {User} from "./administration-user-client.types";

export function createUser(createUserReqBody: User): Promise<User> {
    return httpWithBodyJSon(
        'post', administrationClientConfigPath,
        '/v1/user',
        [],
        {user: <JsonObject><unknown>createUserReqBody});
}

export function editUser(editUserReqBody: User): Promise<void> {
    return httpWithBodyJSon(
        'patch', administrationClientConfigPath,
        '/v1/user',
        [],
        {user: <JsonObject><unknown>editUserReqBody});
}

export function listUsers(): Promise<User[]> {
    return httpNoBodyJSon(
        'get', administrationClientConfigPath,
        '/v1/user',
        []);
}

export function deleteUser(username: string): Promise<void> {
    return httpNoBodyJSon(
        'delete', administrationClientConfigPath,
        '/v1/user/' + username,
        []
        );
}

// MOST IMPORTS SHOULD BE BELOW THIS LINE (see VERIFY-IMPORTS-ON-TOP.js)
