export function newAccount(accountData: BaseAddAccountReqBody): Promise<ExtendedAccountDunoDoc> {
    return httpWithBodyJSon(
            'post', administrationClientConfigPath,
            '/v1/accounts',
            [],
            <JsonObject><unknown>accountData);
}

export function deleteAccount(accountCfid: Cfid): Promise<void> {
    return httpNoBodyJSon(
            'delete', administrationClientConfigPath,
            '/v1/accounts/' + accountCfid,
            []);
}

export function getAccount(accountCfid: Cfid): Promise<ExtendedAccountDunoDoc> {
    return httpNoBodyJSon(
        'get', administrationClientConfigPath,
        '/v1/accounts/' + accountCfid,
        []
    );
}

export function getAllAccounts(): Promise<ExtendedAccountDunoDoc[]> {
    return httpNoBodyJSon(
            'get', administrationClientConfigPath,
            '/v1/accounts',
            []);
}

export function pathAccount(updatedProperties: UpdateAccountReqBody): Promise<void> {
    return httpWithBodyJSon(
            'patch', administrationClientConfigPath,
            '/v1/accounts',
            [],
            <JsonObject><unknown>updatedProperties);
}


// MOST IMPORTS SHOULD BE BELOW THIS LINE (see VERIFY-IMPORTS-ON-TOP.js)
import {administrationClientConfigPath} from './administration-client';
import {BaseAddAccountReqBody, UpdateAccountReqBody} from './administration-accounts-client.types';
import {Cfid} from '../../duno-types/duno-type-name.types';
import {httpNoBodyJSon, httpWithBodyJSon} from '../-micro-service-clients-utils';
import {JsonObject} from '../../utils/json.types';
import {ExtendedAccountDunoDoc} from '../ceda-client/central-duno-docs.types';
