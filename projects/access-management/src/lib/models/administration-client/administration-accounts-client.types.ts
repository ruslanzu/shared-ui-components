// MOST IMPORTS SHOULD BE BELOW THIS LINE (see VERIFY-IMPORTS-ON-TOP.js)
import {Cfid, DunoTypeName_Account} from '../../duno-types/duno-type-name.types';


export interface BaseAddAccountReqBody {
    accountName: string;
    accountType: DunoTypeName_Account;
}

export interface AzureAddAccountReqBody extends BaseAddAccountReqBody{
    subscriptionId: string;
    tenantId: string;
    applicationId: string;
    applicationSecret: string;
}

export interface AwsAddAccountReqBody extends BaseAddAccountReqBody {
    roleArn: string;
    externalId: string;
}

export interface AwsOnboardingDetails {
    cloudFlowAccountId: string;
    externalId: string;
}

export interface GcpAddAccountReqBody extends BaseAddAccountReqBody, GcpFileFields{}

export interface GcpFileFields{
    type: string;
    project_id: string;
    private_key_id: string;
    private_key: string;
    client_email: string;
    client_id: string;
    auth_uri: string;
    token_uri: string;
    auth_provider_x509_cert_url: string;
    client_x509_cert_url: string;
}

export type UpdateAccountReqBody = {accountCfid: Cfid} & Partial<BaseAddAccountReqBody> & Partial<AzureAddAccountReqBody> & Partial<AwsAddAccountReqBody> & Partial<GcpAddAccountReqBody>;
