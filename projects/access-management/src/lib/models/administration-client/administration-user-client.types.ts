// MOST IMPORTS SHOULD BE BELOW THIS LINE (see VERIFY-IMPORTS-ON-TOP.js)

export interface InviteUserRequest {
    username: string;
}

export interface CreateUserRequest {
    user: User;
}

export interface EditUserRequest {
    user: User;
}

export interface User {
    username: string;
    email: string;
    roles?: string[];
    firstName?: string;
    lastName?: string;
    phoneNumber?: string;
    mfaEnable?: boolean;
    status: UserStatus;
}

export enum UserStatus {
    ENABLED = "ENABLED",
    DISABLED = "DISABLED",
    PENDING_FOR_INITIAL_LOGIN = "PENDING_FOR_INITIAL_LOGIN",
    UNKNOWN = "UNKNOWN"
}

export interface GetQRCodeRequest {
    accessToken: string;
}

export interface ValidateMFARequest {
    accessToken: string;
    userCode: string;
}
