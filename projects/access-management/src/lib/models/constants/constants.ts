/****************************
 * General constants
 ****************************/
export const CLOUDFLOW_PRODUCT_NAME: string = 'CloudFlow';
export const AUTHORIZATION_HEADER_KEY: string = 'Authorization';
export const TOKEN_PREFIX: string = 'Bearer ';
export const TENANT_ID_KEY: string = 'x-cloudflow-tenant-id';
export const USERNAME_KEY: string = 'username';
export const USER_ACTIVITY_KEY: string = 'userActivity';
export const USER_OPS_KEY: string = 'userOps';
export const LAST_VIEWED_BRAND_KEY: string = 'lastViewedBrand';
export const HOUR_IN_MILLI_SEC: number = 1000 * 60 * 60;
export const DAY_IN_MILLI_SEC: number = HOUR_IN_MILLI_SEC * 24;
export const PASSWORD_MIN_LENGTH: number = 8;
export const SKIP_TOKEN_INTERCEPTOR: string = 'skip_token_interceptor';
export const HEADERS_BASE_CONFIG = Object.freeze({
  'Content-Type': 'application/json',
  Accept: 'application/json'
});
export const ADMIN_ROLE_ID = '1';