export const IP_CIDR_PATTERN: RegExp = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))$/;
export const DIGIT_PATTERN: RegExp = /^\d+$/;

// (?=.*[a-z])        // use positive look ahead to see if at least one lower case letter exists
// (?=.*[A-Z])        // use positive look ahead to see if at least one upper case letter exists
// (?=.*\d)           // use positive look ahead to see if at least one digit exists
// (?=.*\W])        // use positive look ahead to see if at least one non-word character exists
export const PASSWORD_PATTERN: RegExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/;

export const USERNAME_PATTERN: RegExp = /^[\w\.]{3,100}$/;
