export enum UserOperation {
    OP_ADMIN_SETTINGS = 'AdminSettings',
    OP_SETUP_CLOUD_ACCOUNTS = 'SetupCloudAccounts',
    OP_MANAGE_RESOURCES = 'ManageResources',
    OP_VIEW_RESOURCES = 'ViewResources'
}
