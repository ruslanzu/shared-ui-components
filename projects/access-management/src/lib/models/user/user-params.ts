export interface UserParams {
  username: string;
  email?: string;
  emailVerified?: boolean;
  mfaEnabled?: boolean;
  preferredMfa?: string;
}

export const userParamsKeys: UserParams = {
  username: 'cognito:username',
  email: 'email'
};
