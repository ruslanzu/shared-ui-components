import { User } from '../administration-client/administration-user-client.types';

export interface ClientUserForm extends User {
  email: string;
  roles: string[];
  firstName: string;
  lastName: string;
  phoneNumber: string;
  mfaEnable: boolean;
}
