import {
  initialUsersManagementState,
  UsersManagementState,
  userStateAdapter
} from './users-management-state';
import { UsersManagementActions, UsersManagementActionTypes } from './users-management.actions';

export function reducer(state = initialUsersManagementState, action: UsersManagementActions): UsersManagementState {
  switch (action.type) {
    case UsersManagementActionTypes.LOAD_USERS:
      return {
        ...state,
        isLoading: true
      };

    case UsersManagementActionTypes.LOAD_USERS_SUCCESS:
      return userStateAdapter.setAll(action.payload.users, {
        ...state,
        isLoading: false
      });

    case UsersManagementActionTypes.LOAD_USERS_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error
      };

    default:
      return state;
  }
}
