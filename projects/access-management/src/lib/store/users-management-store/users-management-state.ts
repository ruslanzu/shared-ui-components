import { ClientUserForm } from '../../models/user/clientUserForm';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const userStateAdapter: EntityAdapter<ClientUserForm> = createEntityAdapter<ClientUserForm>({
  selectId: model => model.username
});

export interface UsersManagementState extends EntityState<ClientUserForm> {
  isLoading: boolean;
  selectedUserId?: string;
  error?: any;
}

export const initialUsersManagementState: UsersManagementState = userStateAdapter.getInitialState({
  isLoading: false,
  selectedUserId: null,
  error: null
});
