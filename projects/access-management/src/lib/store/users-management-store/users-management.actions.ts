import { ClientUserForm } from '../../models/user/clientUserForm';
import { Action } from '@ngrx/store';

export enum UsersManagementActionTypes {
  LOAD_USERS = '[UsersManagement] Start to load users',
  LOAD_USERS_SUCCESS = '[UsersManagement] Load users success',
  LOAD_USERS_FAIL = '[UsersManagement] Load users fail',
  EDIT_USER_CLICKED = '[UsersManagement] Edit user clicked',
  DELETE_USER_CLICKED = '[UsersManagement] Delete user clicked',
  SEND_EMAIL_INVITATION_CLICKED = '[UsersManagement] Send email invitation user clicked',
  RESET_MFA_DEVICE_CLICKED = '[UsersManagement] Reset MFA device clicked',
}

export class LoadUsersAction implements Action {
  public readonly type = UsersManagementActionTypes.LOAD_USERS;
  public static create(): LoadUsersAction {
    return new LoadUsersAction();
  }
}

export class LoadUsersSuccessAction implements Action {
  public readonly type = UsersManagementActionTypes.LOAD_USERS_SUCCESS;

  constructor(public payload: { users: ClientUserForm[] }) {}

  public static create(users: ClientUserForm[]): LoadUsersSuccessAction {
    return new LoadUsersSuccessAction({ users });
  }
}

export class LoadUsersFailAction implements Action {
  public readonly type = UsersManagementActionTypes.LOAD_USERS_FAIL;

  constructor(public payload: { error: any }) {}

  public static create(error: any): LoadUsersFailAction {
    return new LoadUsersFailAction({ error });
  }
}

export class EditUserClickedAction implements Action {
  public readonly type = UsersManagementActionTypes.EDIT_USER_CLICKED;

  constructor(public payload: { user: ClientUserForm }) {}

  public static create(user: ClientUserForm): EditUserClickedAction {
    return new EditUserClickedAction({ user });
  }
}

export class DeleteUserClickedAction implements Action {
  public readonly type = UsersManagementActionTypes.DELETE_USER_CLICKED;

  constructor(public payload: { user: ClientUserForm }) {}

  public static create(user: ClientUserForm): DeleteUserClickedAction {
    return new DeleteUserClickedAction({ user });
  }
}

export class SendEmailInvitationClickedAction implements Action {
  public readonly type = UsersManagementActionTypes.SEND_EMAIL_INVITATION_CLICKED;

  constructor(public payload: { user: ClientUserForm }) {}

  public static create(user: ClientUserForm): SendEmailInvitationClickedAction {
    return new SendEmailInvitationClickedAction({ user });
  }
}

export class ResetMfaDeviceClickedAction implements Action {
  public readonly type = UsersManagementActionTypes.RESET_MFA_DEVICE_CLICKED;

  constructor(public payload: { user: ClientUserForm }) {}

  public static create(user: ClientUserForm): ResetMfaDeviceClickedAction {
    return new ResetMfaDeviceClickedAction({ user });
  }
}

export type UsersManagementActions =
  | LoadUsersAction
  | LoadUsersSuccessAction
  | LoadUsersFailAction
  | EditUserClickedAction
  | DeleteUserClickedAction
  | SendEmailInvitationClickedAction
  | ResetMfaDeviceClickedAction;
