import { Injectable } from '@angular/core';
import { ClientUserForm } from '../../models/user/clientUserForm';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { UserApiService } from '../../services/api/user-api.service';
import { UsersManagementService } from '../../services/users-management.service';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import {
  UsersManagementActionTypes,
  UsersManagementActions,
  LoadUsersSuccessAction,
  LoadUsersFailAction,
  EditUserClickedAction,
  DeleteUserClickedAction,
  SendEmailInvitationClickedAction,
  ResetMfaDeviceClickedAction
} from './users-management.actions';

@Injectable()
export class UsersManagementEffects {
  @Effect()
  public loadUsersAction$ = this.actions$.pipe(
    ofType(UsersManagementActionTypes.LOAD_USERS),
    mergeMap(() =>
      this.usersApiService.getUsers().pipe(
        map((users: ClientUserForm[]) => LoadUsersSuccessAction.create(users)),
        catchError(error => of(LoadUsersFailAction.create(error)))
      )
    )
  );

  @Effect({ dispatch: false })
  public editUserClickedAction$ = this.actions$.pipe(
    ofType(UsersManagementActionTypes.EDIT_USER_CLICKED),
    map((action: EditUserClickedAction) => action.payload.user),
    tap((userForm: ClientUserForm) => this.usersManagementService.openEditUserModal(userForm))
  );

  @Effect({ dispatch: false })
  public deleteUserClickedAction$ = this.actions$.pipe(
    ofType(UsersManagementActionTypes.DELETE_USER_CLICKED),
    map((action: DeleteUserClickedAction) => action.payload.user),
    tap((userToDelete: ClientUserForm) => this.usersManagementService.openDeleteUserModal(userToDelete))
  );

  @Effect({ dispatch: false })
  public sendEmailInvitationClickedAction$ = this.actions$.pipe(
    ofType(UsersManagementActionTypes.SEND_EMAIL_INVITATION_CLICKED),
    map((action: SendEmailInvitationClickedAction) => action.payload.user),
    tap((user: ClientUserForm) => this.usersManagementService.openResendEmailInvitationModal(user))
  );

  @Effect({ dispatch: false })
  public resetMfaDeviceClickedAction$ = this.actions$.pipe(
    ofType(UsersManagementActionTypes.RESET_MFA_DEVICE_CLICKED),
    map((action: ResetMfaDeviceClickedAction) => action.payload.user),
    tap((user: ClientUserForm) => this.usersManagementService.openResetMfaModal(user))
  );

  constructor(
    private actions$: Actions<UsersManagementActions>,
    private usersApiService: UserApiService,
    private usersManagementService: UsersManagementService
  ) {}
}
