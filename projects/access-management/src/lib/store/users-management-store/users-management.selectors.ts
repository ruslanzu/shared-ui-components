import { ClientUserForm } from '../../models/user/clientUserForm';
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import {
  UsersManagementState,
  userStateAdapter
} from '../users-management-store/users-management-state';

const { selectAll } = userStateAdapter.getSelectors();

export const selectUsersManagementState: MemoizedSelector<object, UsersManagementState> = createFeatureSelector<UsersManagementState>(
  'usersManagement'
);

export const selectIsLoadingUserManagement: MemoizedSelector<object, boolean> = createSelector(
  selectUsersManagementState,
  (state: UsersManagementState) => state.isLoading
);

export const selectUserManagementErrors: MemoizedSelector<object, any> = createSelector(
  selectUsersManagementState,
  (state: UsersManagementState) => state.error
);

export const selectUsersList: MemoizedSelector<object, ClientUserForm[]> = createSelector(
  selectUsersManagementState,
  selectAll
);
