import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AccessManagementTabs } from '../models/access-management-tabs.enum';
import { UsersManagementService } from '../services/users-management.service';
import { tap } from 'rxjs/operators';
import { AccessManagementActionTypes, AccessManagementActions, AddItemClickedAccessManagementAction } from './access-management.actions';

@Injectable()
export class AccessManagementEffects {
  @Effect({ dispatch: false })
  public addItemClickedAction$ = this.actions$.pipe(
    ofType(AccessManagementActionTypes.ADD_ITEM_CLICKED),
    tap((action: AddItemClickedAccessManagementAction) => {
      switch (action.payload.selectedTab) {
        case AccessManagementTabs.USERS:
          this.usersManagementService.openCreateUserModal();
      }
    })
  );

  constructor(private actions$: Actions<AccessManagementActions>, private usersManagementService: UsersManagementService) {}
}
