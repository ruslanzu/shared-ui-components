import { Action } from '@ngrx/store';
import { AccessManagementTabs } from '../models/access-management-tabs.enum';

export enum AccessManagementActionTypes {
  SEARCH_KEY_CHANGE = '[AccessManagement] Search key has been updated',
  SELECTED_TAB_CHANGE = '[AccessManagement] Selected tab has been updated',
  NUMBER_OF_ITEMS_CHANGE = '[AccessManagement] Number of items has been updated',
  ADD_ITEM_CLICKED = '[AccessManagement] User click on add item button'
}

export class SearchKeyChangeAccessManagementAction implements Action {
  public readonly type = AccessManagementActionTypes.SEARCH_KEY_CHANGE;

  constructor(public payload: { searchKey: string }) {}

  public static create(searchKey: string): SearchKeyChangeAccessManagementAction {
    return new SearchKeyChangeAccessManagementAction({ searchKey });
  }
}

export class SelectedTabChangeAccessManagementAction implements Action {
  public readonly type = AccessManagementActionTypes.SELECTED_TAB_CHANGE;

  constructor(public payload: { selectedTab: AccessManagementTabs }) {}

  public static create(selectedTab: AccessManagementTabs): SelectedTabChangeAccessManagementAction {
    return new SelectedTabChangeAccessManagementAction({ selectedTab });
  }
}

export class NumberOfItemsChangeAccessManagementAction implements Action {
  public readonly type = AccessManagementActionTypes.NUMBER_OF_ITEMS_CHANGE;

  constructor(public payload: { numberOfItems: number }) {}

  public static create(numberOfItems: number): NumberOfItemsChangeAccessManagementAction {
    return new NumberOfItemsChangeAccessManagementAction({ numberOfItems });
  }
}

export class AddItemClickedAccessManagementAction implements Action {
  public readonly type = AccessManagementActionTypes.ADD_ITEM_CLICKED;

  constructor(public payload: { selectedTab: AccessManagementTabs }) {}

  public static create(selectedTab: AccessManagementTabs): AddItemClickedAccessManagementAction {
    return new AddItemClickedAccessManagementAction({ selectedTab });
  }
}

export type AccessManagementActions =
  | SearchKeyChangeAccessManagementAction
  | SelectedTabChangeAccessManagementAction
  | AddItemClickedAccessManagementAction
  | NumberOfItemsChangeAccessManagementAction;
