import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { AccessManagementTabs } from '../models/access-management-tabs.enum';
import { AccessManagementState } from './access-management-state';

export const selectAccessManagementState: MemoizedSelector<object, AccessManagementState> = createFeatureSelector<AccessManagementState>(
  'accessManagement'
);

export const selectAccessManagementSearchKey: MemoizedSelector<object, string> = createSelector(
  selectAccessManagementState,
  (state: AccessManagementState) => state.searchKey
);

export const selectAccessManagementSelectedTab: MemoizedSelector<object, AccessManagementTabs> = createSelector(
  selectAccessManagementState,
  (state: AccessManagementState) => state.selectedTab
);

export const selectAccessManagementNumberOfItems: MemoizedSelector<object, number> = createSelector(
  selectAccessManagementState,
  (state: AccessManagementState) => state.numberOfItems
);
