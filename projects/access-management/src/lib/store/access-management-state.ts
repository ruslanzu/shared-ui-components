import { AccessManagementTabs } from '../models/access-management-tabs.enum';

export interface AccessManagementState {
  selectedTab: AccessManagementTabs;
  searchKey: string;
  numberOfItems: number;
}

export const initialAccessManagementState: AccessManagementState = {
  selectedTab: AccessManagementTabs.USERS,
  searchKey: '',
  numberOfItems: 0
};
