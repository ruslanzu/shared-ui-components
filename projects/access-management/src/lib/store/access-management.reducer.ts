import { AccessManagementState, initialAccessManagementState } from './access-management-state';
import { AccessManagementActions, AccessManagementActionTypes } from './access-management.actions';

export function reducer(state = initialAccessManagementState, action: AccessManagementActions): AccessManagementState {
  switch (action.type) {
    case AccessManagementActionTypes.SEARCH_KEY_CHANGE:
      return {
        ...state,
        searchKey: action.payload.searchKey
      };

    case AccessManagementActionTypes.SELECTED_TAB_CHANGE:
      return {
        ...state,
        selectedTab: action.payload.selectedTab
      };

    case AccessManagementActionTypes.NUMBER_OF_ITEMS_CHANGE:
      return {
        ...state,
        numberOfItems: action.payload.numberOfItems
      };

    default:
      return state;
  }
}
