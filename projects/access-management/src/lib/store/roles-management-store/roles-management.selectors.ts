import { ClientRole } from '../../models/role/clientRole';
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import {
  RolesManagementState,
  roleStateAdapter
} from './roles-management-state';

const { selectAll } = roleStateAdapter.getSelectors();

export const selectRolesManagementState: MemoizedSelector<object, RolesManagementState> = createFeatureSelector<RolesManagementState>(
  'rolesManagement'
);

export const selectIsLoadingRoleManagement: MemoizedSelector<object, boolean> = createSelector(
  selectRolesManagementState,
  (state: RolesManagementState) => state.isLoading
);

export const selectRolesList: MemoizedSelector<object, ClientRole[]> = createSelector(
  selectRolesManagementState,
  selectAll
);
