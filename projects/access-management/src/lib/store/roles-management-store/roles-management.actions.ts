import { ClientRole} from '../../models/role/clientRole';
import { Action } from '@ngrx/store';

export enum RolesManagementActionTypes {
  LOAD_ROLES = '[RolesManagement] Start to load roles',
  LOAD_ROLES_SUCCESS = '[RolesManagement] Load roles success',
  LOAD_ROLES_FAIL = '[RolesManagement] Load roles fail'
}

export class LoadRolesAction implements Action {
  public readonly type = RolesManagementActionTypes.LOAD_ROLES;
  public static create(): LoadRolesAction {
    return new LoadRolesAction();
  }
}

export class LoadRolesSuccessAction implements Action {
  public readonly type = RolesManagementActionTypes.LOAD_ROLES_SUCCESS;

  constructor(public payload: { roles: ClientRole[] }) {}

  public static create(roles: ClientRole[]): LoadRolesSuccessAction {
    return new LoadRolesSuccessAction({ roles });
  }
}

export class LoadRolesFailAction implements Action {
  public readonly type = RolesManagementActionTypes.LOAD_ROLES_FAIL;

  constructor(public payload: { error: any }) {}

  public static create(error: any): LoadRolesFailAction {
    return new LoadRolesFailAction({ error });
  }
}

export type RolesManagementActions =
  | LoadRolesAction
  | LoadRolesSuccessAction
  | LoadRolesFailAction;
