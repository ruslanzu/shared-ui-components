import { ClientRole } from '../../models/role/clientRole';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const roleStateAdapter: EntityAdapter<ClientRole> = createEntityAdapter<ClientRole>({
  selectId: model => model.id
});

export interface RolesManagementState extends EntityState<ClientRole> {
  isLoading: boolean;
  selectedRoleId?: string;
  error?: any;
}

export const initialRolesManagementState: RolesManagementState = roleStateAdapter.getInitialState({
  isLoading: false,
  selectedRoleId: null,
  error: null
});
