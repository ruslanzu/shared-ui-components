import {
  roleStateAdapter,
  RolesManagementState,
  initialRolesManagementState
} from '../../store/roles-management-store/roles-management-state';
import { RolesManagementActions, RolesManagementActionTypes } from './roles-management.actions';

export function reducer(state = initialRolesManagementState, action: RolesManagementActions): RolesManagementState {
  switch (action.type) {
    case RolesManagementActionTypes.LOAD_ROLES:
      return {
        ...state,
        isLoading: true
      };

    case RolesManagementActionTypes.LOAD_ROLES_SUCCESS:
      return roleStateAdapter.setAll(action.payload.roles, {
        ...state,
        isLoading: false
      });

    case RolesManagementActionTypes.LOAD_ROLES_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error
      };

    default:
      return state;
  }
}
