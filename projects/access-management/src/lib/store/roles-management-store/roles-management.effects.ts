import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  RolesManagementActionTypes,
  RolesManagementActions,
  LoadRolesSuccessAction,
  LoadRolesFailAction
} from './roles-management.actions';
import { RoleApiService } from '../../services/api/role-api.service';
import { ClientRole } from '../../models/role/clientRole';
import { of } from 'rxjs';

@Injectable()
export class RolesManagementEffects {
  @Effect()
  public loadRolesAction$ = this.actions$.pipe(
    ofType(RolesManagementActionTypes.LOAD_ROLES),
    mergeMap(() =>
      this.roleApiService.getRoles().pipe(
        map((roles: ClientRole[]) => LoadRolesSuccessAction.create(roles)),
        catchError(err => of(LoadRolesFailAction.create(err)))
      )
    )
  );

  constructor(
    private actions$: Actions<RolesManagementActions>,
    private roleApiService: RoleApiService
  ) {}
}
