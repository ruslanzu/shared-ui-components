import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import * as fromAccessManagement from './access-management.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AccessManagementEffects } from './access-management.effects';
import * as fromUsersManagement from './users-management-store/users-management.reducer';
import { UsersManagementEffects } from './users-management-store/users-management.effects';
import * as fromRolesManagement from './roles-management-store/roles-management.reducer';
import { RolesManagementEffects } from './roles-management-store/roles-management.effects';

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature('accessManagement', fromAccessManagement.reducer),
    StoreModule.forFeature('usersManagement', fromUsersManagement.reducer),
    StoreModule.forFeature('rolesManagement', fromRolesManagement.reducer),
    EffectsModule.forFeature([AccessManagementEffects, UsersManagementEffects, RolesManagementEffects])
  ]
})
export class AccessManagementStoreModule {}
