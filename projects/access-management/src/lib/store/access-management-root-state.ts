import { AccessManagementState } from './access-management-state';
import { RolesManagementState } from './roles-management-store/roles-management-state';
import { UsersManagementState } from './users-management-store/users-management-state';

export interface AccessManagementRootState {
  accessManagementState: AccessManagementState;
  usersManagementState: UsersManagementState;
  rolesManagementState: RolesManagementState;
}
