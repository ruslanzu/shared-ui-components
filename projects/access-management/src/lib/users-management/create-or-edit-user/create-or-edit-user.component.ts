import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { USERNAME_PATTERN } from '../../models/constants/general-regex';
import { ClientUserForm } from '../../models/user/clientUserForm';
import { NgOption } from '@ng-select/ng-select';
import { AbstractFormComponent, FormGroupTyped } from '@algosec/base-components';
import { UnsubscribeSubject } from '../../utils/unsubscribe-subject';
import { BehaviorSubject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { AccessManagementRootState } from '../../store/access-management-root-state';
import { selectRolesList } from '../../store/roles-management-store/roles-management.selectors';
import { ClientRole } from '../../models/role/clientRole';
import { SessionService } from '@algosec/auth';

@Component({
  selector: 'ash-create-or-edit-user',
  templateUrl: './create-or-edit-user.component.html',
  styleUrls: ['./create-or-edit-user.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateOrEditUserComponent extends AbstractFormComponent<ClientUserForm> implements OnInit, OnDestroy {
  public userForm: ClientUserForm;
  public submitForm$: Observable<ClientUserForm>;
  public rolesOptions: NgOption[];
  public showErrorMessage: boolean;
  public errorMessage: string;
  public isLoading: boolean = false;
  public isEditMode: boolean = false;
  public showRolesError: boolean = false;

  private unsubscribe$: UnsubscribeSubject = new UnsubscribeSubject();
  private _submitForm$: BehaviorSubject<ClientUserForm>;
  private rolesMapById;

  constructor(private store: Store<AccessManagementRootState>, private sessionService: SessionService) {
    super();
    this._submitForm$ = new BehaviorSubject<ClientUserForm>(null);
    this.submitForm$ = this._submitForm$.asObservable();
  }

  public ngOnInit() {
    this.form = new FormGroupTyped<ClientUserForm>({
      username: new FormControl({ value: this.userForm.username, disabled: this.isEditMode }, [
        Validators.required,
        Validators.pattern(USERNAME_PATTERN)
      ]),
      firstName: new FormControl(this.userForm.firstName, Validators.required),
      lastName: new FormControl(this.userForm.lastName, Validators.required),
      email: new FormControl(this.userForm.email, [Validators.required, Validators.email]),
      roles: new FormControl({value:this.userForm.roles, disabled: this.shouldDisableRoles()}, this.rolesValidator),
      phoneNumber: new FormControl(this.userForm.phoneNumber),
      mfaEnable: new FormControl(this.userForm.mfaEnable),
      status: new FormControl(this.userForm.status)
    });
    this.subscribeFormValueChanges();
    this.subscribeRolesList();
  }

  public ngOnDestroy() {
    this.unsubscribe$.cleanup();
    this._submitForm$.complete();
  }

  public onSubmit() {
    super.onSubmit();
  }

  get modalTitleText(): string {
    return this.isEditMode ? 'Edit user' : 'Add user';
  }

  get modalButtonText(): string {
    return this.isEditMode ? 'Save' : 'Add';
  }

  get modalInfoText(): string {
    return this.isEditMode ? '' : 'An invitation will be sent to the specified email.';
  }

  protected submit(): void {
    this.showErrorMessage = false;
    this._submitForm$.next(this.form.getRawValue());
  }

  private subscribeFormValueChanges(): void {
    this.form.valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.showErrorMessage = false;
      this.showRolesError = this.shouldShowRolesError();
    });
  }

  private shouldShowRolesError(): boolean {
    return !!(this.form.controls.roles.errors);
  }

  private prepareRolesOptions(roles: ClientRole[]): NgOption[] {
    // OOB roles on top, then alphabetically
    return roles.map(role => ({...role, id: String(role.id)})).sort((a,b) => {
      if (!a.isOOB && !b.isOOB) {
        return a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1;
      }
      if (a.isOOB && b.isOOB) {
        return 0;
      }
      return +b.isOOB - +a.isOOB;
    });
  }

  private buildRolesMap(roles: ClientRole[]) {
    const rolesMap = {};
    roles.forEach((r) => {
      rolesMap[String(r.id)] = r;
    });
    return rolesMap;
  }

  private subscribeRolesList(): void {
    this.store
      .pipe(
        select(selectRolesList),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((roles: ClientRole[]) => {
        this.rolesMapById = this.buildRolesMap(roles);
        this.rolesOptions = this.prepareRolesOptions(roles);
      });
  }

  private rolesValidator = (rolesControl: FormControl) => {
    if (!this.rolesMapById) {
      return null;
    }
    const allSelectedRoles = rolesControl.value;
    const relevantSelectedRoles = allSelectedRoles.filter((roleId) => !!this.rolesMapById[roleId]);
    return relevantSelectedRoles.length > 0 ? null : { rolesRequired: true };
  }

  private shouldDisableRoles(): boolean {
  	return false;
    // return this.userForm.username && this.userForm.username === this.sessionService.getUsername();
  }
}
