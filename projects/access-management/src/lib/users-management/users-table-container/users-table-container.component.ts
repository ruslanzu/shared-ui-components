import { ColDef, GridOptions } from '@ag-grid-community/core';
import { CellActionDropdownRendererComponent } from '@algosec/base-components';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { map, takeUntil, withLatestFrom } from 'rxjs/operators';
import { AccessManagementTabs } from '../../models/access-management-tabs.enum';
import { UserStatus } from '../../models/administration-client/administration-user-client.types';
import { ClientRole } from '../../models/role/clientRole';
import { ClientUserForm } from '../../models/user/clientUserForm';
import { AccessManagementRootState } from '../../store/access-management-root-state';
import {
  NumberOfItemsChangeAccessManagementAction,
  SelectedTabChangeAccessManagementAction
} from '../../store/access-management.actions';
import { selectAccessManagementSearchKey } from '../../store/access-management.selectors';
import { LoadRolesAction } from '../../store/roles-management-store/roles-management.actions';
import {
  selectIsLoadingRoleManagement,
  selectRolesList
} from '../../store/roles-management-store/roles-management.selectors';
import {
  DeleteUserClickedAction,
  EditUserClickedAction,
  LoadUsersAction,
  ResetMfaDeviceClickedAction,
  SendEmailInvitationClickedAction
} from '../../store/users-management-store/users-management.actions';
import {
  selectIsLoadingUserManagement, selectUsersManagementState
} from '../../store/users-management-store/users-management.selectors';
import { UnsubscribeSubject } from '../../utils/unsubscribe-subject';

@Component({
  selector: 'ash-users-table-container',
  templateUrl: './users-table-container.component.html',
  styleUrls: ['./users-table-container.component.scss']
})
export class UsersTableContainerComponent implements OnInit, OnDestroy {
  public gridOptions: GridOptions;
  public columnDefs: ColDef[];
  public unsubscribe$: UnsubscribeSubject = new UnsubscribeSubject();
  public userListToShow: ClientUserForm[] = [];

  private users: ClientUserForm[];
  private rolesMap: {[id: string]: ClientRole};
  private gridParams: any;
  
  constructor(private store: Store<AccessManagementRootState>) {
    this.initStore();
  }

  private static isUsernameIncludeKey(user: ClientUserForm, searchKey: string): boolean {
    return user.username && user.username.toLocaleLowerCase().includes(searchKey);
  }

  private static isUserEmailIncludeKey(user: ClientUserForm, searchKey: string): boolean {
    return user.email && user.email.toLocaleLowerCase().includes(searchKey);
  }

  private static isUserRoleIncludeKey(user: ClientUserForm, searchKey: string, rolesMap: {[id: string]: ClientRole}): boolean {
    return !!(user.roles && user.roles.filter(role => {
      return rolesMap[role] && rolesMap[role].name.toLocaleLowerCase().includes(searchKey);
    }).length);
  }

  private getGridOptions(): GridOptions {
    return {
      rowHeight: 40,
      animateRows: true,
      frameworkComponents: {
        cellActionDropdownRender: CellActionDropdownRendererComponent
      },
      onGridReady: (params) => {
        this.gridParams = params;
      },
    };
  }


  public ngOnInit() {
    this.initGridParams();
    this.subscribeUsersList();
    this.subscribeSearchEvents();
    this.subscribeRolesList();
    this.resizeListener();
  }

  public ngOnDestroy() {
    this.unsubscribe$.cleanup();
  }

  get isLoading$(): Observable<boolean> {
    return combineLatest([
      this.store.select(selectIsLoadingUserManagement),
      this.store.select(selectIsLoadingRoleManagement)
    ]).pipe(
      map(([usersLoading, rolesLoading]) => usersLoading || rolesLoading),
      takeUntil(this.unsubscribe$)
    );
  }

  private initStore() {
    this.store.dispatch(LoadUsersAction.create());
    this.store.dispatch(LoadRolesAction.create());
    this.store.dispatch(SelectedTabChangeAccessManagementAction.create(AccessManagementTabs.USERS));
  }

  private subscribeUsersList(): void {
    this.store
    .pipe(
      select(selectUsersManagementState),
      withLatestFrom(this.store.select(selectAccessManagementSearchKey)),
      takeUntil(this.unsubscribe$)
    )
      .subscribe(([state, searchKey]: [any, string]) => {
      this.users = Object.values(state.entities);
      this.userListToShow = !!searchKey ? this.getFilteredUserListToShow(searchKey) : this.users;
      this.notifyStoreOnNumberOfItemsInTable();
    });
}

  private subscribeRolesList(): void {
    this.store
      .pipe(
        select(selectRolesList),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(roles => {
        this.rolesMap = this.buildRolesMap(roles);
      });
  }

  private subscribeSearchEvents(): void {
    this.store
      .pipe(
        select(selectAccessManagementSearchKey),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((searchKey: string) => {
        this.userListToShow = this.getFilteredUserListToShow(searchKey.toLocaleLowerCase());
        this.notifyStoreOnNumberOfItemsInTable();
      });
  }

  private buildRolesMap(roles: ClientRole[]): {[id: string]: ClientRole} {
    return roles.reduce((rolesMap, role) => {
      rolesMap[role.id] = role;
      return rolesMap;
    }, {});
  }

  private getFilteredUserListToShow(searchKey: string): ClientUserForm[] {
    return this.users.filter(
      user =>
        UsersTableContainerComponent.isUsernameIncludeKey(user, searchKey) ||
        UsersTableContainerComponent.isUserEmailIncludeKey(user, searchKey) ||
        UsersTableContainerComponent.isUserRoleIncludeKey(user, searchKey, this.rolesMap)
    );
  }

  private initGridParams(): void {
    this.gridOptions = this.getGridOptions();
    this.columnDefs = this.getColumnDefs();
  }

  private notifyStoreOnNumberOfItemsInTable() {
    this.store.dispatch(NumberOfItemsChangeAccessManagementAction.create(this.userListToShow.length));
  }

  private formatUserRoles(roles: string[]): string[] {
    const rolesForSort = [...roles];
    const userRoles = [];
    rolesForSort
      // Sort roles by id
      .sort((a, b) => Number(a) - Number(b))
      .forEach((roleId) => {
        if (this.rolesMap[roleId]) {
          userRoles.push(this.rolesMap[roleId].name);
        }
    });
    return userRoles;
  }

  private getColumnDefs(): ColDef[] {
    const cellStyle = { paddingTop: '7px' };
    return [
      {
        headerName: 'Username',
        field: 'username',
        minWidth: 150,
        sortable: true,
        resizable: true,
        cellStyle
      },
      {
        headerName: 'Email',
        field: 'email',
        minWidth: 150,
        sortable: true,
        resizable: true,
        cellStyle
      },
      {
        headerName: 'Roles',
        colId: 'roles',
        valueGetter: (params => this.formatUserRoles(params.data.roles)),
        minWidth: 150,
        sortable: true,
        resizable: true,
        cellStyle
      },
      {
        headerName: 'MFA',
        colId: 'mfa',
        valueGetter: (params) => params.data.mfaEnable ? 'Yes' : 'No',
        minWidth: 150,
        sortable: true,
        resizable: true,
        cellStyle
      },
      {
        headerName: 'Actions',
        colId: 'actions',
        minWidth: 85,
        maxWidth: 85,
        sortable: true,
        resizable: true,
        cellRenderer: 'cellActionDropdownRender',
        cellRendererParams: {
          cellActionDropdownItem: [
            {
              actionText: 'Edit',
              actionFunction: (user: ClientUserForm) => this.store.dispatch(EditUserClickedAction.create(user)),
              showItem: () => true,
              itemClass: 'accessmgmt-edit-user-action'
            },
            {
              actionText: 'Delete',
              actionFunction: (user: ClientUserForm) => this.store.dispatch(DeleteUserClickedAction.create(user)),
              showItem: () => true,
              itemClass: 'accessmgmt-delete-user-action'
            },
            {
              actionText: 'Reset MFA device',
              actionFunction: (user: ClientUserForm) => this.store.dispatch(ResetMfaDeviceClickedAction.create(user)),
              showItem: (user: ClientUserForm) => user.mfaEnable,
              itemClass: 'accessmgmt-reset-mfa-device'
            },
            {
              actionText: 'Resend invitation',
              actionFunction: (user: ClientUserForm) => this.store.dispatch(SendEmailInvitationClickedAction.create(user)),
              showItem: (user: ClientUserForm) => user.status === UserStatus.PENDING_FOR_INITIAL_LOGIN
            }
          ]
        }
      }
    ];
  }

  private resizeListener(): void {
    fromEvent(window, 'resize').pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      if (this.gridParams && this.gridParams.api) {
        this.gridParams.api.sizeColumnsToFit();
      }
    });
  }
}
