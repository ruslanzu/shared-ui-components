import { Component, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ROLE_MANAGEMENT_ROUTE_PATH, USER_MANAGEMENT_ROUTE_PATH } from './models/constants/settings-routes';
import { PLUS_ICON } from './models/constants/svg-icon-constants';
import { select, Store } from '@ngrx/store';
import { AccessManagementTabs } from './models/access-management-tabs.enum';
import { AccessManagementRootState } from './store/access-management-root-state';
import {
  AddItemClickedAccessManagementAction,
  SearchKeyChangeAccessManagementAction
} from './store/access-management.actions';
import {
  selectAccessManagementNumberOfItems,
  selectAccessManagementSelectedTab
} from './store/access-management.selectors';
import { UnsubscribeSubject } from './utils/unsubscribe-subject';
import { Environment, EnvironmentPreset, environments } from './models/environment';
import { takeUntil } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { UserApiService } from './services/api/user-api.service';
import { UsersManagementService } from './services/users-management.service';


@Component({
  selector: 'ash-access-management',
  templateUrl: './access-management.component.html',
  styleUrls: ['./access-management.component.scss']
})
export class AccessManagementComponent implements OnDestroy {
  public navLinks: any[] = [
    { path: `/${USER_MANAGEMENT_ROUTE_PATH}`, label: 'USERS', disabled: false }
  ];
  public tabTextToTextMap: Map<AccessManagementTabs, string> = new Map();
  public selectedTab: AccessManagementTabs = AccessManagementTabs.USERS;
  public numberOfItems: number;
  public readonly plusIcon = PLUS_ICON;

  public env: Environment;

  private unsubscribe$: UnsubscribeSubject = new UnsubscribeSubject();
  
  private subscription: Subscription;

  constructor(
    private store: Store<AccessManagementRootState>,
    private route: ActivatedRoute,
    private usersApiService: UserApiService
) {
    this.subscription = this.route.data.subscribe(data => {
      this.usersApiService.env = environments[data.envPreset];
    });

    this.tabTextToTextMap.set(AccessManagementTabs.USERS, 'user');
    this.tabTextToTextMap.set(AccessManagementTabs.ROLES, 'role');
    this.subscribeSelectedTab();
    this.subscribeNumberOfItems();
  }

  public ngOnDestroy(): void {
    this.store.dispatch(SearchKeyChangeAccessManagementAction.create(''));
    this.unsubscribe$.cleanup();
  }

  public addButtonClicked(): void {
    this.store.dispatch(AddItemClickedAccessManagementAction.create(this.selectedTab));
  }

  public handleSearch(searchKey: string): void {
    this.store.dispatch(SearchKeyChangeAccessManagementAction.create(searchKey));
  }

  private subscribeSelectedTab(): void {
    this.store
      .pipe(
        select(selectAccessManagementSelectedTab),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((selectedTab: AccessManagementTabs) => {
        this.selectedTab = selectedTab;
      });
  }

  private subscribeNumberOfItems(): void {
    this.store
      .pipe(
        select(selectAccessManagementNumberOfItems),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((numberOfItems: number) => {
        this.numberOfItems = numberOfItems;
      });
  }
}
