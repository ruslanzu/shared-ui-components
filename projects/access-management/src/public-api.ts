/*
 * Public API Surface of access-management
 */

export * from './lib/access-management.service';
export * from './lib/access-management.component';
export * from './lib/access-management.module';
