var HttpSimpleProxy = require('http-simple-proxy');
var httpSimpleProxy = new HttpSimpleProxy();
httpSimpleProxy.init({
 ports: {
  4200: {
    router: {
      "*": 4201,
      "/algosaas/*": 4202,
    },
  },
 }
}, function(err) {
  if(err)
    console.error('Proxy error:', err)
 // listening
 console.info('http-simple-proxy started')
});
