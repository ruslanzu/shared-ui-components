pipeline {
  agent {label "spot-ec2"}
  options {
    timestamps()
    timeout(time: 1, unit: 'HOURS')
    buildDiscarder logRotator(daysToKeepStr: '30', numToKeepStr: '50')
  }
  stages {
    stage('prepare env') {
      steps{
        script{
          sh 'printenv'
          withNPM(npmrcConfig:'npmc') {
            sh'''#!/bin/bash
            echo "install chrome"
            wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
            sudo yum -y install ./google-chrome-stable_current_x86_64.rpm
            sudo ln -s /usr/bin/google-chrome-stable /usr/bin/chromium
            echo "install node"
            curl -sL https://rpm.nodesource.com/setup_14.x | sudo bash -
            sudo yum install nodejs -y
            node --version
            npm --version
            '''
          }
        }
      }
    }
    stage('build') {
      steps {
        script {
          if (env.CHANGE_TARGET){
            echo " ***** Pull Request ***** "
            currentBuild.displayName = "${BUILD_NUMBER} - ${env.CHANGE_BRANCH}"
            withNPM(npmrcConfig:'npmc') {
              sh '''#!/bin/bash
              set -x
              npm --registry http://172.21.3.228:8081/repository/npm-group install
              for component in auth base-components shell asms-integration access-management authorization; do
                node_modules/.bin/ng build ${component}
                node_modules/.bin/ng test ${component} --watch=false --browsers=ChromeHeadless
                newVer=0.${CHANGE_ID}.${BUILD_NUMBER}-beta.0
                cd dist/${component}
                npm -no-git-tag-version version ${newVer}
                cd ../../
                echo " Locally install built ${component} "
                npm install ./dist/${component} 
                npm publish --ignore-scripts ./dist/${component} --registry http://172.21.3.228:8081/repository/npm-private --prod
              done
              '''
            }
          }
          if (! env.CHANGE_TARGET) {
            currentBuild.displayName = "${BUILD_NUMBER} - ${env.BRANCH_NAME}"
            withNPM(npmrcConfig:'npmc') {
              sh '''#!/bin/bash
              set -x
              if [ "$GIT_BRANCH" == "develop" ]; then
                print " ***** Merge to Develop ***** "
                npm --registry http://172.21.3.228:8081/repository/npm-group install
                regex="([0-9]+).([0-9]+).([0-9]+)"
                for component in auth base-components shell asms-integration access-management authorization; do
                  pVER=$(jq -r ".version" projects/${component}/package.json)
                  if [[ $pVER =~ $regex ]];then
                    major="${BASH_REMATCH[1]}"
                    minor="${BASH_REMATCH[2]}"
                  fi
                  newVer=${major}.${minor}.${BUILD_NUMBER}
                  node_modules/.bin/ng build ${component} --prod
                  node_modules/.bin/ng test ${component} --watch=false --browsers=ChromeHeadless
                  cd dist/${component}
                  npm -no-git-tag-version version ${newVer}
                  cd ../../
                  # TODO: remove --ignore-scripts from npm publish  (bug: CS-4442 )
                  echo " Locally install built ${component} "
                  npm install ./dist/${component} 
                  npm publish --ignore-scripts ./dist/${component} --registry http://172.21.3.228:8081/repository/npm-private --prod
                  cd ${WORKSPACE}
                done
                npm install
                for component in auth base-components shell asms-integration access-management authorization; do
                  npm update @algosec/${component}
                done
                npm list --depth=0 > dependencies.txt
                # npm run build lib-tester
                temp_role=$(aws sts assume-role --role-arn arn:aws:iam::403658236501:role/remote_jenkins --role-session-name jenkins --output json )
                export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq .Credentials.AccessKeyId | xargs)
                export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq .Credentials.SecretAccessKey | xargs)
                export AWS_SESSION_TOKEN=$(echo $temp_role | jq .Credentials.SessionToken | xargs)
                aws s3 cp dist/lib-tester/ s3://algosec-app-dev/lib-tester --recursive --exclude index.html --exclude index.dev.html --exclude index.stage.html --exclude index.prod.html
                aws s3 cp dist/lib-tester/index.html s3://algosec-app-dev/lib-tester/index.html --cache-control no-store
                aws s3 cp dependencies.txt s3://algosec-app-dev/lib-tester/dependencies.txt 
              else
                echo "Building Branch: $GIT_BRANCH"
                npm --registry http://172.21.3.228:8081/repository/npm-group install
                for component in auth base-components shell asms-integration access-management authorization; do
                  node_modules/.bin/ng build ${component}
                  node_modules/.bin/ng test ${component} --watch=false --browsers=ChromeHeadless
                  echo " Locally install built ${component} "
                  npm install ./dist/${component} 
                done
              fi
              '''
            }
          }
        }
      }
    }
  }
}
