# SharedComponents

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.14.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Setting up a local environment for test program
1. Install dependencies: npm install. Also see 'Add the desired @algosec packages to package.json ' section below to insure you are working with the desired library versions)

2. The auth library must be aware of the product type being used. Declare it with: sessionService.setProductType(<product-type>);current product-type options are: 'cloud_flow' and 'object_flow'. If using lib tester, this item is already done.

3. Start the lib tester test program locally consistent with proxy server settings above,
and the proxy server with:
npm run lib-tester-with-login
This starts the proxy server as well with:
ng serve --port 4201 --base-href \"/\" lib-tester

4. Install the login application locally (available from login-app repository)

5. It is recommended that the login app work with the stage environment for authenticating users. To do  this, edit the env_mode line in src/index.html to:
	<p hidden id="env_mode">stage</p>

6. Based on what is set in the previous step, in lib-tester/src/environments/environment.ts set
either dev, stage, or production to true, and the others false.
	
7. Start the login application locally:
	npm run start-stage-dev
	This runs:
	ng serve -c=stage --port 4202 --base-href /algosaas/
	which is consistent with the proxy server setting above
8. Open browser to:
   localhost:4200

## Setting up a local environment for library development
1. Build required algosec libraries. In console, run:
npm run build-libs . It will build the libraries:
base-components
shell 
asms-integration 
access-management 
build auth 
authorization
   
2. The auth library must be aware of the product type being used. Declare it with: sessionService.setProductType(<product-type>);current product-type options are: 'cloud_flow' and 'object_flow'
If using lib tester, this item is already done.
 
3. Start the lib tester test program locally consistent with proxy server settings above,
and the proxy server with:
npm run lib-tester-with-login
This starts the proxy server as well with:
ng serve --port 4201 --base-href \"/\" lib-tester

4. Install the login application locally (available from login-app repository)

5. It is recommended that the login app work with the stage environment for authenticating users. To do  this, edit the env_mode line in src/index.html to:
	<p hidden id="env_mode">stage</p>

6. Based on what is set in the previous step, in lib-tester/src/environments/environment.ts set
either dev, stage, or production to true, and the others false.

7. Start the login application locally:
	npm run start-stage-dev
	This runs:
	ng serve -c=stage --port 4202 --base-href /algosaas/
	which is consistent with the proxy server setting above
8. Open browser to:
   localhost:4200

## Using the shared libraries in an application:
1. Add the desired packages to package.json  (the one in the root directory)
(a) Adjust package.json with needed libs:
Either:
I. installation + updating
"dependencies": {
    "@algosec/base-components": "^0.0.0",
    "@algosec/shell": "^0.0.0",
	...
	
Important: If the mechanism above is used, you must run npm update <package-names> to insure the latest versions get installed. 
Example: npm update @algosec/base-components etc

or 
II. Specific versions, example:
	"dependencies": {
    "@algosec/base-components": "^0.0.9",
    "@algosec/shell": "^0.0.9",
	...
	
	Version lists can be seen at:
	algosaas-nexus.algosec.com:8081/#browse/browse:npm-private

2. Run 'npm install', note any peer depedencies that are required by the packages you used, for example the base-components (at time of writing) needs
    "rxjs": "^6.5.3",
    "@ngrx/effects": "^12.4.0",
    "@ngrx/entity": "^12.4.0",
    "@ngrx/router-store": "^12.4.0",
    "@ngrx/store": "^12.4.0"
So make sure all peer packages are fulfilled.

3. When you need a newer version of a package, please update all used packages together, to make sure they match
npm update @algosec/base-components @algosec/shell @algosec/auth @algosec/access-management

4. Functional packages (e.g. asms-integration or access-management) needs env settings so they'll access the correct backend environment. This is archived via data object that is given during the routing:
{
    path: 'settings/asms-integration',
    component: AsmsIntegrationComponent,
    data: { envPreset: EnvironmentPreset.Stage },
    canActivate: [AuthGuard]
}

5. Make sure you have NGRX store initialized in your app, even if you dont need one. base-components depends on it
@NgModule({
  declarations: [ ... ],
  imports: [ ..., StoreModule.forRoot({}, {}), EffectsModule.forRoot([]) ],
  providers: [ ... ],
  bootstrap: [ ... ]
})
export class AppModule { }

6. Please see the lib-tester application for examples of the items above.

## Using the auth library in an application:
1. add the @algosec/auth and aws-amplify to package.json
"dependencies": {
    "@algosec/auth": "^0.0.0",
	"aws-amplify": "^4.0.0",
	 (To insure proper @algosec version see  'Add the desired @algosec packages' above)
2. Setup the interceptors  
import relevant interceptors in the app module:
import { AuthErrorInterceptor, TokenInterceptor } from '@algosec/auth';

and declare the interceptors as providers:
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthErrorInterceptor, multi: true }
];

At this time, any other interceptors must be supplied and managed by the application developer

3. Declare the application's product type. Example:
sessionService.setProductType('cloud_flow');
current options are: 'cloud_flow' and 'object_flow'

4. To extend session timeout period on user activity, keystroke and mouse click  HostListeners can be setup as follow as follows:
  @HostListener('document:keyup', ['$event'])
  @HostListener('document:click', ['$event'])
  public updateUserActivity() {
   this.sessionService.updateUserActivity(false);
  }

5. If needed, login and logout can be programmtically driven by the application with:
sessionService.userLogin() and sessionService.userLogout()

6. Add to polyfills.ts (missing on ObjectFlow. Test program works without it)
// aws-sdk requires global to exist
(window as any).global = window;

7. In angular.json, in projects.$name.architect.build.options section, add: 
"preserveSymlinks": true

(preserveSymlinks missing on ObjectFlow. Test program works without it)

8. Please see the lib-tester application for examples of the items above.

## Using the authorization library in an application:
1. The intent of this library is to provide authorization features for a logged in user with regard to enabling/disabling UI components of interest.
2. Follow steps in 'Using the auth library in an application' above (auth library is for authentication, not authorization)
3. Minimally, make sure you have:
"dependencies": {
    "@algosec/auth": "^0.0.0",
    "@algosec/authorization": "^0.0.0",

    you most likely will also want:
     "@algosec/base-components": "^0.0.0",
     	 (To insure proper version see  'Add the desired @algosec packages' above)
4. Please see 'Component Examples' panel and SampleComponent in the lib tester for examples of use.
     



	


